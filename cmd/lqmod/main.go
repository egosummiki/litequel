package main

import (
	"io/ioutil"
	"log"

	"gitlab.com/egosummiki/litequel/lexer"
	"gitlab.com/egosummiki/litequel/modules"
)

func main() {
	buff, err := ioutil.ReadFile("project.mod")
	if err != nil {
		log.Fatal(err)
	}

	tokens, err := lexer.ReadTokens(buff)
	if err != nil {
		log.Fatal(err)
	}

	p := modules.Parser{}
	project, err := p.Parse(tokens)
	if err != nil {
		log.Fatal(err)
	}

	log.Print(project)
}
