package main

import (
	"fmt"
	"io/ioutil"
	"os"

	"gitlab.com/egosummiki/litequel"
)

func main() {
	if len(os.Args) < 2 {
		_, _ = fmt.Fprint(os.Stderr, "No argument specified")
		os.Exit(1)
	}

	buffer, err := ioutil.ReadFile(os.Args[1])
	if err != nil {
		_, _ = fmt.Fprint(os.Stderr, err.Error())
		os.Exit(2)
	}

	engine := litequel.NewEngine()
	rtErr := engine.Evaluate(buffer)
	if rtErr != nil {
		_, _ = fmt.Fprint(os.Stderr, rtErr.Error())
		os.Exit(3)
	}
}
