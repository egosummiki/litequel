package main

import (
	"io/ioutil"
	"log"
	"os"

	"gitlab.com/egosummiki/litequel"
	"gitlab.com/egosummiki/litequel/generation"
)

func main() {
	if len(os.Args) < 2 {
		log.Fatal("No argument specified")
	}

	buffer, err := ioutil.ReadFile(os.Args[1])
	if err != nil {
		log.Fatal(err)
	}

	engine := litequel.NewEngine()
	tokens, err := engine.GetTokens(buffer)
	if err != nil {
		log.Fatal(err)
	}

	ast, err := engine.GetAST(tokens)
	if err != nil {
		log.Fatal(err)
	}

	generation.PrintAST(ast, 0)
}
