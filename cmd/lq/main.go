package main

import (
	"bufio"
	"fmt"
	"os"

	"gitlab.com/egosummiki/litequel"
	"gitlab.com/egosummiki/litequel/runtime/execution"
	"gitlab.com/egosummiki/litequel/tokens"
)

const Version = "0.1"

func printIndent(indent int) {
	fmt.Print("  ")
	for i := 0; i < indent; i++ {
		fmt.Print("   ")
	}
}

func getTokens(engine *litequel.Engine, reader *bufio.Reader, indent int) (tokens.TokenReader, error) {
	if indent == 0 {
		fmt.Print("> ")
	} else {
		printIndent(indent)
	}

	cmd, err := reader.ReadBytes('\n')
	if err != nil {
		return nil, err
	}

	tkns, err := engine.GetTokens(cmd)
	if err != nil {
		return nil, err
	}

	if tkns.EndsWithSemi() {
		return tkns, nil
	}

	for {
		more, err := getTokens(engine, reader, indent+1)
		if err != nil {
			return nil, err
		}
		tkns.Join(more)
		if more.EndsScope() {
			fmt.Print("\033[1A\r")
			printIndent(indent)
			for _, token := range tokens.ReadAll(more) {
				if token.Kind != ';' {
					fmt.Print(token.Content)
				}
			}
			fmt.Println("                ")
			return tkns, nil
		}
	}
}

func main() {
	var exec *execution.Execution
	reader := bufio.NewReader(os.Stdin)
	engine := litequel.NewEngine()

	fmt.Println("Litequel Command Line -- Version", Version)
	for {
		tokens, err := getTokens(engine, reader, 0)
		if err != nil {
			fmt.Println(err)
			continue
		}
		ast, err := engine.GetAST(tokens)
		if err != nil {
			fmt.Println(err)
			continue
		}
		exec, err = engine.GetInstructions(exec, ast)
		if err != nil {
			fmt.Println(err)
			continue
		}
		engine.Represent(exec)

		rtErr := engine.Execute(exec)
		if rtErr != nil {
			fmt.Println(rtErr)
		}
	}
}
