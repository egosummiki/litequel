package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"os"

	"gitlab.com/egosummiki/litequel"
)

func main() {
	if len(os.Args) < 2 {
		log.Fatal("No argument specified")
	}

	buffer, err := ioutil.ReadFile(os.Args[1])
	if err != nil {
		log.Fatal(err)
	}

	engine := litequel.NewEngine()
	tokens, err := engine.GetTokens(buffer)
	if err != nil {
		log.Fatal(err)
	}

	ast, err := engine.GetAST(tokens)
	if err != nil {
		log.Fatal(err)
	}

	instr, err := engine.GetInstructions(nil, ast)
	if err != nil {
		log.Fatal(err)
	}

	for b := 0; b < instr.NumBuckets(); b++ {
		bucket := instr.GetBucket(b)
		fmt.Printf("%s:\n", bucket.Name)
		for i, ins := range instr.GetBucket(b).Instructions {
			fmt.Printf("  %04d  %s\n", i, ins.Mnemonic(nil))
		}
	}
}
