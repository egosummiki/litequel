package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"os"

	"gitlab.com/egosummiki/litequel"
	"gitlab.com/egosummiki/litequel/tokens"
)

func main() {
	if len(os.Args) < 2 {
		log.Fatal("No argument specified")
	}

	buffer, err := ioutil.ReadFile(os.Args[1])
	if err != nil {
		log.Fatal(err)
	}

	engine := litequel.NewEngine()
	tokenReader, err := engine.GetTokens(buffer)
	if err != nil {
		log.Fatal(err)
	}

	tokenList := tokens.ReadAll(tokenReader)
	for _, token := range tokenList {
		fmt.Printf("%03d:%02d    %s\n", token.Line, token.Column, token.String())
	}
}
