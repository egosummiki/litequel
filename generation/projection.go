package generation

import (
	"github.com/google/uuid"
	"gitlab.com/egosummiki/litequel/ast"
	"gitlab.com/egosummiki/litequel/model"
	"gitlab.com/egosummiki/litequel/model/function"
	"gitlab.com/egosummiki/litequel/runtime/execution"
	"gitlab.com/egosummiki/litequel/runtime/instruction"
	"gitlab.com/egosummiki/litequel/symbols"
	"gitlab.com/egosummiki/litequel/symbols/scope"
	"gitlab.com/egosummiki/litequel/types"
)

func createProjectionFunction(exec *execution.Execution, table symbols.Table, scope scope.Scope, line, col int, objType, projType types.ID, props []ast.ProjectionProperty) (function.Function, error) {
	var instructions []InstructionLoc

	details, err := table.GetTypeDetails(objType)
	if err != nil {
		return function.Function{}, err
	}

	collection, ok := details.(types.Collection)
	if !ok {
		return function.Function{}, genError(line, col, "projection can be used only on collections")
	}

	resultTypeDetails, err := table.GetTypeDetails(collection.ElementType())
	if err != nil {
		return function.Function{}, err
	}
	if resultTypeDetails.TypeVariant() != types.VariantStruct {
		return function.Function{}, genError(line, col, "filtered type must be a struct")
	}
	fields := resultTypeDetails.(types.TypeStruct).Fields

	funcName := uuid.New().String()
	funcObj := function.Function{
		NumArgs: len(fields) + 1,
	}

	newScope := scope.Sub(funcName)
	funcObj.Locals = 0

	err = table.RegisterLocal("self", newScope, collection.ElementType())
	if err != nil {
		return funcObj, err
	}
	for _, field := range fields {
		err = table.RegisterLocal(field.Name, newScope, field.TypeID)
	}

	if len(props) == 1 && len(props[0].Arg) == 0 {
		expr, err := TranslateExpressionCheckBool(exec, newScope, props[0].Value, table)
		if err != nil {
			return function.Function{}, err
		}
		instructions = append(instructions, expr...)
	} else {
		for i, prop := range props {
			expr, err := TranslateExpressionCheckBool(exec, newScope, prop.Value, table)
			if err != nil {
				return function.Function{}, err
			}
			instructions = append(instructions, expr...)
			instructions = append(instructions, loc(line, col, instruction.PushInst{Arg: model.Int32Object(i)}))
		}
		instructions = append(instructions, loc(line, col, instruction.ConstructInst{
			Count:      len(props),
			Type:       projType,
			TotalCount: len(props),
			Overload:   nil,
		}))
	}
	instructions = append(instructions, loc(line, col, instruction.ReturnInst{}))

	pureInst, locations := SeperateInfo(instructions)
	funcObj.BucketID = exec.AddBucket(newScope, 0, pureInst, locations)

	return funcObj, nil
}
