package generation

import (
	"gitlab.com/egosummiki/litequel/runtime/instruction"
)

var OperatorInstruction = map[string]instruction.Instruction{
	"+":  instruction.AddInst{},
	"-":  instruction.SubInst{},
	"*":  instruction.MultInst{},
	"/":  instruction.DivInst{},
	"%":  instruction.ModInst{},
	">":  instruction.GtInst{},
	">=": instruction.GtEqInst{},
	"<":  instruction.LtInst{},
	"<=": instruction.LtEqInst{},
	"==": instruction.EqInst{},
	"!=": instruction.NeqInst{},
	"..": instruction.RangeInst{},
}

