package generation

import (
	"fmt"
)

func genError(line, col int, message string) error {
	return fmt.Errorf("compiler error [%d:%d]: %s", line, col, message)
}

func genErrorf(line, col int, message string, args ...interface{}) error {
	return fmt.Errorf("compiler error [%d:%d]: %s", line, col, fmt.Sprintf(message, args...))
}
