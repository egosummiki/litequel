package generation

import (
	"gitlab.com/egosummiki/litequel/runtime/execution"
	"gitlab.com/egosummiki/litequel/runtime/instruction"
)

type InstructionSet struct {
	instructions []InstructionLoc
}

func (is *InstructionSet) add(line, col int, inst instruction.Instruction) {
	is.instructions = append(is.instructions, loc(line, col, inst))
}

func (is *InstructionSet) addLoc(inst ...InstructionLoc) {
	is.instructions = append(is.instructions, inst...)
}

type InstructionLoc struct {
	Instruction instruction.Instruction
	Line        int
	Col         int
}

func loc(line, col int, instr instruction.Instruction) InstructionLoc {
	return InstructionLoc{
		Instruction: instr,
		Line:        line,
		Col:         col,
	}
}

func SeperateInfo(instructions []InstructionLoc) ([]instruction.Instruction, []execution.Location) {
	var resultInstr []instruction.Instruction
	var locs []execution.Location

	for _, in := range instructions {
		resultInstr = append(resultInstr, in.Instruction)
		locs = append(locs, execution.Location{Line: in.Line, Col: in.Col})
	}
	return resultInstr, locs
}
