package generation

import (
	"fmt"

	"gitlab.com/egosummiki/litequel/ast"
	"gitlab.com/egosummiki/litequel/model"
	"gitlab.com/egosummiki/litequel/runtime"
	"gitlab.com/egosummiki/litequel/runtime/execution"
	"gitlab.com/egosummiki/litequel/runtime/instruction"
	"gitlab.com/egosummiki/litequel/semantics"
	"gitlab.com/egosummiki/litequel/symbols"
	"gitlab.com/egosummiki/litequel/symbols/scope"
	"gitlab.com/egosummiki/litequel/types"
)

func TranslateLiteral(table symbols.Table, scope scope.Scope, literal ast.Literal) (InstructionLoc, types.ID, error) {
	var obj runtime.Object
	switch literal.Kind {
	case ast.LiteralFloat:
		obj = model.NewFloat32Object(literal.Value)
	case ast.LiteralInt:
		obj = model.NewInt32Object(literal.Value)
	case ast.LiteralString:
		obj = model.StringObject(literal.Value)
	default:
		return InstructionLoc{}, types.TypeUnknown, genErrorf(literal.Line, literal.Col, "invalid literal %s", literal.Value)
	}
	return loc(literal.Line, literal.Col, instruction.PushInst{Arg: obj}), obj.Type(), nil
}

func TranslateIdentifier(table symbols.Table, scope scope.Scope, identifier ast.Identifier) (InstructionLoc, types.ID, error) {
	symbol, err := table.Find(identifier.Name, scope)
	if err != nil {
		return InstructionLoc{}, types.TypeUnknown, genErrorf(identifier.Line, identifier.Col, "unrecognised symbol '%s' in scope '%s'", identifier.Name, scope)
	}

	switch symbol.Variant {
	case symbols.Global:
		return loc(identifier.Line, identifier.Col, instruction.LoadGlobal{Arg: symbol.Reference}), symbol.Type, nil
	case symbols.Enclosed:
		return loc(identifier.Line, identifier.Col, instruction.LoadEnclosed{
			Frame:   symbol.Frame,
			Address: symbol.Reference,
		}), symbol.Type, nil
	case symbols.Local:
		return loc(identifier.Line, identifier.Col, instruction.Load{Arg: symbol.Reference}), symbol.Type, nil
	}

	return InstructionLoc{}, types.TypeUnknown, genError(identifier.Line, identifier.Col, "unknown symbol type")
}

func TranslateOperator(table symbols.Table, scope scope.Scope, operator ast.Operator) (InstructionLoc, types.ID, error) {
	instr := OperatorInstruction[operator.Kind]
	if instr == nil {
		return InstructionLoc{}, types.TypeUnknown, genErrorf(operator.Line, operator.Col, "unrecognised symbol '%s' in scope '%s'", operator.Kind, scope)
	}
	resultType := types.TypeUnknown
	if operator.Kind == ".." {
		resultType = types.TypeRange
	}
	return loc(operator.Line, operator.Col, instr), resultType, nil
}

func TranslateTemplateApplication(exec *execution.Execution, table symbols.Table, scp scope.Scope, temApp ast.TemplateApplication) ([]InstructionLoc, types.ID, error) {
	result := &InstructionSet{}
	var resultType types.ID

	templ, err := table.FindTemplate(temApp.Identifier.Name)
	if err != nil {
		return nil, 0, genErrorf(temApp.Line, temApp.Col, "template error: %s", err)
	}
	switch templ.(type) {
	case ast.FunctionDefinition:
		funcTempl := templ.(ast.FunctionDefinition)
		applied, err := semantics.ApplyFunctionDecl(temApp.Args, funcTempl)
		if err != nil {
			return nil, 0, genErrorf(temApp.Line, temApp.Col, err.Error())
		}

		symbol, err := table.Find(applied.Name, scp)
		if err != nil {
			a := semantics.Analysis{
				Table: table,
			}
			if err := a.CheckFunctionDeclStmt(scp, applied, 0); err != nil {
				return nil, 0, genErrorf(temApp.Line, temApp.Col, "error while applying function template: %s", err)
			}
			symbol, err = table.Find(applied.Name, scp)
			if err != nil {
				return nil, 0, genErrorf(temApp.Line, temApp.Col, err.Error())
			}
		}
		resultType = symbol.Type

		translated, err := TranslateFuncDecl(exec, table, scp, applied, 0)
		if err != nil {
			return nil, 0, err
		}
		result.addLoc(translated...)

		if symbol.Variant == symbols.Global {
			result.add(funcTempl.Line, funcTempl.Col, instruction.LoadGlobal{
				Arg: symbol.Reference,
			})
		} else if symbol.Variant == symbols.Enclosed {
			return nil, 0, fmt.Errorf("cannot apply enclosed symbol")
		}
	default:
		return nil, 0, genErrorf(temApp.Line, temApp.Col, "invalid template")
	}

	return result.instructions, resultType, nil
}

func TranslateExpression(exec *execution.Execution, scope scope.Scope, exp ast.Expression, table symbols.Table) ([]InstructionLoc, error) {
	var lastType types.ID
	result := &InstructionSet{}

	for i, element := range exp.Elements {
		switch element.(type) {
		case ast.FunctionCall:
			var err error
			lastType, err = handleFuncCall(result, exec, table, scope, element, lastType)
			if err != nil {
				return nil, err
			}
		case ast.Conversion:
			convert := element.(ast.Conversion)
			expr, err := TranslateExpressionCheckBool(exec, scope, convert.Value, table)
			if err != nil {
				return nil, err
			}
			result.addLoc(expr...)
			targetType, err := semantics.NodeToTypeID(table, scope, convert.TargetType)
			if err != nil {
				return nil, genErrorf(convert.TargetType.Line, convert.TargetType.Col, "type error: %s", err)
			}
			result.add(convert.Line, convert.Col, instruction.ConvertInst{TargetType: targetType})
		case ast.ArrayLiteral:
			array := element.(ast.ArrayLiteral)
			for _, expr := range array.Elements {
				instr, err := TranslateExpressionCheckBool(exec, scope, expr, table)
				if err != nil {
					return nil, err
				}
				result.addLoc(instr...)
			}
			lastType = types.TypeArray
			if len(array.Elements) > 0 {
				first := array.Elements[0]
				subType, err := semantics.DeduceType(table, scope, first)
				if err != nil {
					return nil, genErrorf(first.Line, first.Col, "error deducing type: %s", err)
				}
				if subType != types.TypeUnknown {
					var err error
					lastType, err = table.RegisterType(types.ListOf(subType))
					if err != nil {
						return nil, err
					}
				}
			}
			result.add(array.Line, array.Col, instruction.MakeListInst{Num: len(array.Elements)})
		case ast.FormatString:
			fStr := element.(ast.FormatString)
			stringFmt := model.StringFormatObject{
				Content: fStr.Content,
			}
			for _, pos := range fStr.Format {
				stringFmt.Format = append(stringFmt.Format, pos.Position)
			}
			for i := len(fStr.Format) - 1; i >= 0; i-- {
				value, err := TranslateExpressionCheckBool(exec, scope, fStr.Format[i].Value, table)
				if err != nil {
					return nil, err
				}
				result.addLoc(value...)
			}
			result.add(fStr.Line, fStr.Col, instruction.FormatStrInst{Value: stringFmt})
		case ast.Literal:
			instr, typeID, err := TranslateLiteral(table, scope, element.(ast.Literal))
			if err != nil {
				return nil, err
			}
			lastType = typeID
			result.addLoc(instr)
		case ast.Identifier:
			instr, typeID, err := TranslateIdentifier(table, scope, element.(ast.Identifier))
			if err != nil {
				return nil, err
			}
			lastType = typeID
			result.addLoc(instr)
		case ast.Operator:
			instr, typeID, err := TranslateOperator(table, scope, element.(ast.Operator))
			if err != nil {
				return nil, err
			}
			lastType = typeID
			result.addLoc(instr)
		case ast.IndexOperator:
			indexOp := element.(ast.IndexOperator)
			index, err := TranslateExpressionCheckBool(exec, scope, indexOp.Index, table)

			if err != nil {
				return nil, err
			}
			result.addLoc(index...)
			result.add(indexOp.Line, indexOp.Col, instruction.IndexInst{})

			typeDetails, err := table.GetTypeDetails(lastType)
			if err != nil {
				return nil, genError(indexOp.Line, indexOp.Col, err.Error())
			}

			collection, ok := typeDetails.(types.Collection)
			if !ok {
				return nil, genErrorf(indexOp.Line, indexOp.Col, "%s is not a collection type", typeDetails.TypeName())
			}

			lastType = collection.ElementType()
		case ast.ObjectProperty:
			objProp := element.(ast.ObjectProperty)
			symbol, err := table.FindProperty(lastType, objProp.Name)
			if err != nil {
				return nil, genErrorf(objProp.Line, objProp.Col, "unknown symbol %s.%s", lastType.String(), objProp.Name)
			}

			lastType = symbol.Type
			lastTypeDetails, err := table.GetTypeDetails(symbol.Type)
			if err != nil {
				return nil, genError(objProp.Line, objProp.Col, err.Error())
			}

			if lastTypeDetails.TypeVariant() == types.VariantFunction { // if method assure we are calling it
				if len(exp.Elements) <= i+1 {
					return nil, genErrorf(objProp.Line, objProp.Col, "attempt to access method %s.%s as an object", lastType.String(), objProp.Name)
				}
				if _, ok := exp.Elements[i+1].(ast.FunctionCall); !ok {
					return nil, genErrorf(objProp.Line, objProp.Col, "attempt to access method %s.%s as an object", lastType.String(), objProp.Name)
				}
				result.add(objProp.Line, objProp.Col, instruction.LoadGlobal{Arg: symbol.Reference})
			} else {
				result.add(objProp.Line, objProp.Col, instruction.GetAttribInst{Index: symbol.Reference})
			}
		case ast.BooleanOp:
			op := element.(ast.BooleanOp)
			kind := 2
			if op.JumpToElse {
				kind += 2
			}
			if op.JumpOnFalse {
				kind++
			}
			result.add(op.Line, op.Col, instruction.TmpInst{Kind: kind, Level: op.Level})
		case ast.Constructor:
			var err error
			lastType, err = HandleConstructor(result, table, exec, scope, exp, element, lastType)
			if err != nil {
				return nil, err
			}
		case ast.Type:
			loneType := element.(ast.Type)
			t, err := semantics.NodeToTypeID(table, scope, loneType)
			if err != nil {
				return nil, genError(loneType.Line, loneType.Col, err.Error())
			}
			result.add(loneType.Line, loneType.Col, instruction.PushInst{Arg: model.Int32Object(t)})
		case ast.SortStmt:
			sort := element.(ast.SortStmt)

			details, err := table.GetTypeDetails(lastType)
			if err != nil {
				return nil, genError(sort.Line, sort.Col, err.Error())
			}

			collection, ok := details.(types.Collection)
			if !ok {
				return nil, genError(sort.Line, sort.Col, "expected a collection")
			}

			symbol, err := table.FindProperty(collection.ElementType(), sort.Field)
			if err != nil {
				return nil, genErrorf(sort.Line, sort.Col, "unknown symbol %s.%s", collection.ElementType().String(), sort.Field)
			}

			result.add(sort.Line, sort.Col, instruction.SortInst{
				Prop: symbol.Reference,
				Desc: sort.Descending,
			})
		case ast.MapConstruct:
			mapConst := element.(ast.MapConstruct)

			for _, value := range mapConst.Values {
				key, err := TranslateExpression(exec, scope, value.Key, table)
				if err != nil {
					return nil, err
				}
				result.addLoc(key...)
				value, err := TranslateExpressionCheckBool(exec, scope, value.Value, table)
				if err != nil {
					return nil, err
				}
				result.addLoc(value...)
			}

			result.add(mapConst.Line, mapConst.Col, instruction.MakeMapInst{Size: len(mapConst.Values)})
		case ast.Reference:
			ref := element.(ast.Reference)
			result.add(ref.Line, ref.Col, instruction.RefInst{})
		case ast.TemplateApplication:
			temApp := element.(ast.TemplateApplication)
			templ, resultType, err := TranslateTemplateApplication(exec, table, scope, temApp)
			if err != nil {
				return nil, err
			}
			result.addLoc(templ...)
			lastType = resultType
		default:
			return nil, genError(exp.Line, exp.Col, "invalid expression")
		}
	}
	return result.instructions, nil
}

func HandleConstructor(result *InstructionSet, table symbols.Table, exec *execution.Execution, sc scope.Scope, exp ast.Expression, element interface{}, lastType types.ID) (types.ID, error) {
	con := element.(ast.Constructor)
	typeID, err := semantics.NodeToTypeID(table, sc, con.TypeSpec)
	if err != nil {
		return 0, err
	}
	if typeID == types.TypeUnknown {
		return 0, genError(con.Line, exp.Col, "constructing undeclared type")
	}

	if len(con.TypeSpec.TemplateArgs) != 0 {
		template, err := table.FindTemplate(con.TypeSpec.Name)
		if err == nil {
			structTem, ok := template.(ast.StructDefinition)
			if ok {
				for _, met := range structTem.MethodTemplates {
					var err error
					tmpName := met.Name
					met, err = semantics.ApplyFunctionDecl(con.TypeSpec.TemplateArgs, met)
					if err != nil {
						return 0, err
					}
					met.Name = tmpName
					decl, err := TranslateFuncDecl(exec, table, sc, met, typeID)
					if err != nil {
						return 0, err
					}
					result.addLoc(decl...)
				}
			}
		}
	}

	for _, arg := range con.Values {
		ref, err := table.FindProperty(typeID, arg.Arg)
		if err != nil {
			return 0, genErrorf(arg.Value.Line, arg.Value.Col, "Type %s doesn't contain property %s.", con.TypeSpec, arg.Arg)
		}
		instr, err := TranslateExpressionCheckBool(exec, sc, arg.Value, table)
		if err != nil {
			return 0, err
		}
		result.addLoc(instr...)
		result.add(arg.Value.Line, arg.Value.Col, instruction.PushInst{Arg: model.Int32Object(ref.Reference)})
	}
	resultInst := instruction.ConstructInst{
		Count:      len(con.Values),
		Type:       typeID,
		TotalCount: table.ScopeReferences(scope.Property.TypeScope(typeID)),
	}
	addMethod, err := table.FindProperty(typeID, "add")
	if err == nil {
		resultInst.Overload = &struct {
			AddMethod int
		}{AddMethod: addMethod.Reference}
	}
	lastType = typeID
	result.add(con.Line, con.Col, resultInst)
	return lastType, nil
}

func getInterfaceMapping(table symbols.Table, interfaceType types.Interface, structType types.ID, structDet types.TypeStruct) (map[int]int, error) {
	mapping := make(map[int]int)
	for i, ifaceMet := range interfaceType.Contract {
		hasMet := false
		for _, structMet := range structDet.Methods {
			if types.MethodsEqual(ifaceMet, structMet) {
				sym, err := table.Find(symbols.FormatMethod(structType, structMet.Name), scope.Global)
				if err != nil {
					return nil, err
				}
				mapping[i] = sym.Reference
				hasMet = true
				break
			}
		}
		if !hasMet {
			return nil, fmt.Errorf("struct does not implement interface missing %s method", ifaceMet.Name)
		}
	}
	return mapping, nil
}

func handleFuncCall(result *InstructionSet, exec *execution.Execution, table symbols.Table, scope scope.Scope, element interface{}, lastType types.ID) (types.ID, error) {
	call := element.(ast.FunctionCall)

	details, err := table.GetTypeDetails(lastType)
	if err != nil {
		return 0, err
	}

	if details.TypeVariant() != types.VariantFunction {
		return 0, genErrorf(call.Line, call.Col, "attempted to call a non function")
	}
	funcType := details.(types.Function)

	if funcType.IsVariadic {
		if len(call.Arguments) < len(funcType.Arguments) {
			return 0, genErrorf(call.Line, call.Col, "invalid number of call arguments expected %d, got %d", len(funcType.Arguments), len(call.Arguments))
		}
	} else {
		if len(call.Arguments) != len(funcType.Arguments) {
			return 0, genErrorf(call.Line, call.Col, "invalid number of call arguments expected %d, got %d", len(funcType.Arguments), len(call.Arguments))
		}
	}

	for i, arg := range call.Arguments {
		t, err := semantics.DeduceType(table, scope, arg)
		if err != nil {
			return 0, genErrorf(call.Line, call.Col, "error deducing type: %s", err)
		}
		if i < len(funcType.Arguments) {
			if funcType.Arguments[i] != types.TypeUnknown && funcType.Arguments[i] != t {
				funcArgDetails, err := table.GetTypeDetails(funcType.Arguments[i])
				if err != nil {
					return 0, genErrorf(call.Line, call.Col, "type error: %s", err)
				}
				if funcArgDetails.TypeVariant() == types.VariantInterface {
					details, err := table.GetTypeDetails(t)
					if err != nil {
						return 0, genErrorf(call.Line, call.Col, "type error: %s", err)
					}
					if details.TypeVariant() != types.VariantStruct {
						return 0, genErrorf(call.Line, call.Col, "interface type requires a struct or an interface")
					}

					mapping, err := getInterfaceMapping(table, funcArgDetails.(types.Interface), t, details.(types.TypeStruct))
					if err != nil {
						return 0, genErrorf(call.Line, call.Col, "interface error: %s", err)
					}

					expr, err := TranslateExpressionCheckBool(exec, scope, call.Arguments[i], table)
					if err != nil {
						return 0, err
					}

					result.addLoc(expr...)
					result.add(call.Line, call.Col, instruction.Wrap{
						IFaceType:      funcType.Arguments[i],
						UnderlyingType: t,
						Mapping:        mapping,
					})
					continue
				}

				return 0, genErrorf(call.Line, call.Col, "invalid type of argument %d expected %s got %s", i, funcType.Arguments[i].String(), t.String())
			}
		}

		expr, err := TranslateExpressionCheckBool(exec, scope, call.Arguments[i], table)
		if err != nil {
			return 0, err
		}

		result.addLoc(expr...)
	}
	if call.Or != nil {
		info, err := table.Find(call.Or.ErrName, scope)
		if err != nil {
			return 0, genErrorf(call.Or.Line, call.Or.Col, "unknown error symbol %s", call.Or.ErrName)
		}
		result.add(call.Line, call.Col, instruction.CallCatchInst{NumArgs: len(call.Arguments), Address: info.Reference})
		return funcType.ReturnType, nil
	}
	result.add(call.Line, call.Col, instruction.CallInst{NumArgs: len(call.Arguments)})

	return funcType.ReturnType, nil
}
