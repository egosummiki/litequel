package generation

import (
	"gitlab.com/egosummiki/litequel/ast"
	"gitlab.com/egosummiki/litequel/model"
	"gitlab.com/egosummiki/litequel/model/function"
	"gitlab.com/egosummiki/litequel/runtime"
	"gitlab.com/egosummiki/litequel/runtime/execution"
	"gitlab.com/egosummiki/litequel/runtime/instruction"
	"gitlab.com/egosummiki/litequel/semantics"
	"gitlab.com/egosummiki/litequel/symbols"
	"gitlab.com/egosummiki/litequel/symbols/scope"
	"gitlab.com/egosummiki/litequel/types"
)

func toTmp(ins InstructionLoc) *instruction.TmpInst {
	switch ins.Instruction.(type) {
	case instruction.TmpInst:
		tmp := ins.Instruction.(instruction.TmpInst)
		return &tmp
	}
	return nil
}

func addJumpInst(result *[]InstructionLoc, index int, value int, whenZero bool) {
	instr := (*result)[index]
	if whenZero {
		(*result)[index] = loc(instr.Line, instr.Col, instruction.JumpZeroInst{Address: value})
	} else {
		(*result)[index] = loc(instr.Line, instr.Col, instruction.JumpNotZeroInst{Address: value})
	}
}

func FillLazyEvaluation(result *[]InstructionLoc, blockSize int) {
nextAndOrToken:
	for i, ins := range *result {
		tmp := toTmp(ins)
		if tmp == nil {
			continue
		}

		jumpToElse := (tmp.Kind - 2) / 2
		jumpOnFalse := (tmp.Kind - 2) % 2

		for j, ins2 := range (*result)[i:] {
			other := toTmp(ins2)
			if other == nil {
				continue
			}
			jumpToElse2 := (other.Kind - 2) / 2

			if jumpToElse != jumpToElse2 && tmp.Level >= other.Level {
				addJumpInst(result, i, j, jumpOnFalse != 0)
				continue nextAndOrToken
			}
		}

		if jumpToElse != 0 {
			addJumpInst(result, i, len(*result)-i+blockSize-1, jumpOnFalse != 0)
		} else {
			addJumpInst(result, i, len(*result)-i-1, jumpOnFalse != 0)
		}
	}
}

func TranslateExpressionCheckBool(exec *execution.Execution, scope scope.Scope, exp ast.Expression, table symbols.Table) ([]InstructionLoc, error) {
	result, err := TranslateExpression(exec, scope, exp, table)
	if err != nil {
		return result, err
	}
	if exp.Boolean {
		FillLazyEvaluation(&result, 2)
		result = append(result,
			loc(exp.Line, exp.Col, instruction.PushInst{Arg: model.Int32Object(1)}),
			loc(exp.Line, exp.Col, instruction.JumpForwardInst{Address: 1}),
			loc(exp.Line, exp.Col, instruction.PushInst{Arg: model.Int32Object(0)}),
		)
	}
	return result, nil
}

func TranslateConstDecl(exec *execution.Execution, table symbols.Table, sc scope.Scope, decl ast.ConstDecl) ([]InstructionLoc, error) {
	var result []InstructionLoc

	symbol, err := table.Find(decl.Name, scope.Global)
	if err != nil {
		return nil, genErrorf(decl.Line, decl.Col, "Unrecognied symbol '%s' in global scope: %s", decl.Name, err)
	}

	value, err := TranslateExpressionCheckBool(exec, sc, decl.Value, table)
	if err != nil {
		return nil, err
	}

	result = append(result, value...)
	result = append(result, loc(decl.Line, decl.Col, instruction.StoreGlobal{Address: symbol.Reference}))

	return result, nil
}

func TranslateFailStmt(exec *execution.Execution, table symbols.Table, scope scope.Scope, stmt ast.FailStmt) ([]InstructionLoc, error) {
	var result []InstructionLoc
	deduced, err := semantics.DeduceType(table, scope, stmt.Message)
	if err != nil {
		return nil, genErrorf(stmt.Message.Line, stmt.Message.Col, "error deducing type: %s", err)
	}
	if deduced != types.TypeString && deduced != types.TypeError {
		return nil, genError(stmt.Line, stmt.Col, "expected a string type")
	}
	expr, err := TranslateExpressionCheckBool(exec, scope, stmt.Message, table)
	if err != nil {
		return nil, err
	}
	result = append(result, expr...)
	result = append(result, loc(stmt.Line, stmt.Col, instruction.FailInst{}))
	return result, nil
}

func TranslateFuncDecl(exec *execution.Execution, table symbols.Table, scp scope.Scope, decl ast.FunctionDefinition, receiverType types.ID) ([]InstructionLoc, error) {
	if len(decl.TemplateParams) != 0 {
		return nil, nil
	}

	funcObj := function.Function{
		NumArgs: len(decl.Params),
	}

	var symbol symbols.Details
	if receiverType != 0 || len(decl.Receiver.Name) != 0 {
		var err error
		if receiverType == 0 {
			receiverType, err = semantics.NodeToTypeID(table, scp, decl.Receiver)
			if err != nil {
				return nil, err
			}
		}

		symbol, err = table.Find(symbols.FormatMethod(receiverType, decl.Name), scope.Global)
		if err != nil {
			return nil, genErrorf(decl.Line, decl.Col, "symbol error: %s", err)
		}
	} else {
		var err error
		symbol, err = table.Find(decl.Name, scp)
		if err != nil {
			return nil, genErrorf(decl.Line, decl.Col, "symbol error: %s", err)
		}
	}

	details, err := table.GetTypeDetails(symbol.Type)
	if err != nil {
		return nil, genErrorf(decl.Line, decl.Col, "symbol error: %s", err)
	}
	if details.TypeVariant() != types.VariantFunction {
		return nil, genErrorf(decl.Line, decl.Col, "invalid declaration type")
	}
	funcDetails := details.(types.Function)

	var localScope scope.Scope
	if funcDetails.Receiver != types.TypeUnknown {
		localScope = scp.Sub(symbols.FormatMethod(funcDetails.Receiver, decl.Name))
		funcObj.Locals = table.ScopeReferences(localScope) - len(decl.Params) - 2
	} else {
		localScope = scp.Sub(decl.Name)
		funcObj.Locals = table.ScopeReferences(localScope) - len(decl.Params)
	}

	var slide []InstructionLoc
	if funcObj.Locals > 0 {
		slide = append(slide, loc(decl.Line, decl.Col, instruction.Slide{Amount: funcObj.Locals}))
	}

	body, err := TranslateBlock(exec, table, localScope, decl.Body)
	if err != nil {
		return nil, err
	}

	if len(body) > 0 {
		switch body[len(body)-1].Instruction.(type) {
		case instruction.ReturnInst:
		default:
			body = append(body, loc(decl.Line, decl.Col, instruction.PushInst{Arg: model.None}))
		}
	} else {
		body = append(body, loc(decl.Line, decl.Col, instruction.PushInst{Arg: model.None}))
	}

	pureInst, locs := SeperateInfo(append(slide, body...))
	funcObj.BucketID = exec.AddBucket(localScope, 0, pureInst, locs)

	var pushObj runtime.Object
	if funcDetails.Receiver != types.TypeUnknown {
		symbol, err = table.Find(symbols.FormatMethod(funcDetails.Receiver, decl.Name), scp)
		pushObj = function.Method(funcObj)
	} else if decl.Variadic {
		symbol, err = table.Find(decl.Name, scp)
		pushObj = function.VariadicFunc(funcObj)
	} else {
		symbol, err = table.Find(decl.Name, scp)
		pushObj = funcObj
	}
	if err != nil {
		return nil, genErrorf(decl.Line, decl.Col, "unrecognised symbol '%s' in scope '%s'", decl.Name, scp)
	}
	if symbol.Variant == symbols.Global {
		return []InstructionLoc{
			loc(decl.Line, decl.Col, instruction.PushInst{Arg: pushObj}),
			loc(decl.Line, decl.Col, instruction.StoreGlobal{Address: symbol.Reference}),
		}, nil
	} else {
		return []InstructionLoc{
			loc(decl.Line, decl.Col, instruction.PushInst{Arg: pushObj}),
		}, nil
	}
}

func TranslateReturnStmt(exec *execution.Execution, table symbols.Table, scope scope.Scope, stmt ast.ReturnStmt) ([]InstructionLoc, error) {
	var result []InstructionLoc
	var err error
	result, err = TranslateExpressionCheckBool(exec, scope, stmt.Value, table)
	if err != nil {
		return nil, err
	}
	if len(result) == 0 {
		result = append(result, loc(stmt.Line, stmt.Col, instruction.PushInst{Arg: model.None}))
	}
	result = append(result, loc(stmt.Line, stmt.Col, instruction.ReturnInst{}))
	return result, nil
}

func TranslateVariableDecl(exec *execution.Execution, table symbols.Table, scp scope.Scope, decl ast.VariableDecl) ([]InstructionLoc, error) {
	var result []InstructionLoc

	symbol, err := table.Find(decl.Name, scp)
	if err != nil {
		return nil, genErrorf(decl.Line, decl.Col, "symbol error: %s", err)
	}

	valueType, err := semantics.DeduceType(table, scp, decl.Value)
	if err != nil {
		return nil, genErrorf(decl.Value.Line, decl.Value.Col, "type error: %s", err)
	}

	value, err := TranslateExpressionCheckBool(exec, scp, decl.Value, table)
	if err != nil {
		return nil, err
	}
	if len(value) == 0 {
		result = append(result, loc(decl.Line, decl.Col, instruction.PushInst{Arg: model.None}))
	} else {
		result = append(result, value...)
	}

	if symbol.Type != valueType {
		result = append(result, loc(decl.Line, decl.Col, instruction.ConvertInst{TargetType: symbol.Type}))
	}

	result = append(result, loc(decl.Line, decl.Col, instruction.Store{Address: symbol.Reference}))
	return result, nil
}

func TranslateAssignStmt(exec *execution.Execution, table symbols.Table, scope scope.Scope, stmt ast.AssignStmt) ([]InstructionLoc, error) {
	var result []InstructionLoc

	targetType, err := semantics.DeduceType(table, scope, stmt.Target)
	if err != nil {
		return nil, genErrorf(stmt.Target.Line, stmt.Target.Col, "type error: %s", err)
	}
	if stmt.Kind == ast.AssignAppend {
		det, err := table.GetTypeDetails(targetType)
		if err != nil {
			return nil, err
		}
		collection, ok := det.(types.Collection)
		if !ok {
			return nil, genErrorf(stmt.Value.Line, stmt.Value.Col, "expected collection type")
		}
		targetType = collection.ElementType()
	}
	valueType, err := semantics.DeduceType(table, scope, stmt.Value)
	if err != nil {
		return nil, genErrorf(stmt.Value.Line, stmt.Value.Col, "type error: %s", err)
	}

	if stmt.Kind == ast.AssignMinusMinus || stmt.Kind == ast.AssignPlusPlus {
		result = append(result, loc(stmt.Line, stmt.Col, instruction.PushInst{Arg: model.Int32Object(1)}))
	} else {
		value, err := TranslateExpressionCheckBool(exec, scope, stmt.Value, table)
		if err != nil {
			return nil, err
		}
		if len(value) == 0 {
			result = append(result, loc(stmt.Line, stmt.Col, instruction.PushInst{Arg: model.None}))
		} else {
			result = append(result, value...)
		}

		if targetType != valueType {
			result = append(result, loc(stmt.Line, stmt.Col, instruction.ConvertInst{
				TargetType: targetType,
			}))
		}
	}

	if len(stmt.Target.Elements) == 1 {
		value, ok := stmt.Target.Elements[0].(ast.Identifier)
		if !ok {
			return nil, genErrorf(stmt.Line, stmt.Col, "not a valid assignation")
		}
		symbol, err := table.Find(value.Name, scope)
		if err != nil {
			return nil, genErrorf(stmt.Line, stmt.Col, "unrecognised symbol '%s' in scope '%s'", value.Name, scope)
		}
		if symbol.Variant == symbols.Global && stmt.Kind != ast.AssignAppend {
			return nil, genErrorf(stmt.Line, stmt.Col, "globals are not mutable")
		}
		if symbol.Variant == symbols.Enclosed {
			if stmt.Kind != ast.AssignEq {
				return nil, genError(stmt.Line, stmt.Col, "enclosed variables can only be assigned with an equal sign")
			}
			result = append(result, loc(stmt.Line, stmt.Col, instruction.StoreEnclosed{Frame: symbol.Frame, Address: symbol.Reference}))
			return result, nil
		}
		switch stmt.Kind {
		case ast.AssignPlus:
			fallthrough
		case ast.AssignPlusPlus:
			result = append(result, loc(stmt.Line, stmt.Col, instruction.StoreAdd{Address: symbol.Reference}))
		case ast.AssignMinus:
			fallthrough
		case ast.AssignMinusMinus:
			result = append(result, loc(stmt.Line, stmt.Col, instruction.StoreSubtract{Address: symbol.Reference}))
		case ast.AssignMult:
			result = append(result, loc(stmt.Line, stmt.Col, instruction.StoreMult{Address: symbol.Reference}))
		case ast.AssignDiv:
			result = append(result, loc(stmt.Line, stmt.Col, instruction.StoreDiv{Address: symbol.Reference}))
		case ast.AssignAppend:
			if symbol.Variant == symbols.Global {
				result = append(result, loc(stmt.Line, stmt.Col, instruction.AppendGlobal{Address: symbol.Reference}))
			} else {
				result = append(result, loc(stmt.Line, stmt.Col, instruction.Append{Address: symbol.Reference}))
			}
		case ast.AssignEq:
			result = append(result, loc(stmt.Line, stmt.Col, instruction.Store{Address: symbol.Reference}))
		}
	} else {
		if stmt.Target.Boolean {
			return nil, genErrorf(stmt.Line, stmt.Col, "Cannot assign to boolean value.")
		}
		last := stmt.Target.Elements[len(stmt.Target.Elements)-1]
		switch last.(type) {
		case ast.IndexOperator:
			if stmt.Kind != ast.AssignEq {
				return nil, genErrorf(stmt.Line, stmt.Col, "This version supports only '=' assignation to lists.")
			}
			indexedExpr := ast.Expression{
				Elements: stmt.Target.Elements[:len(stmt.Target.Elements)-1],
				Boolean:  false,
				Line:     stmt.Line,
				Col:      stmt.Col,
			}
			indexed, err := TranslateExpression(exec, scope, indexedExpr, table)
			if err != nil {
				return nil, err
			}
			result = append(result, indexed...)
			indexValue, err := TranslateExpression(exec, scope, last.(ast.IndexOperator).Index, table)
			if err != nil {
				return nil, err
			}
			result = append(result, indexValue...)
			result = append(result, loc(stmt.Line, stmt.Col, instruction.SetIndexInst{}))
		case ast.ObjectProperty:
			if stmt.Kind != ast.AssignEq {
				return nil, genErrorf(stmt.Line, stmt.Col, "This version supports only '=' assignation to objects.")
			}
			objectExpr := ast.Expression{
				Elements: stmt.Target.Elements[:len(stmt.Target.Elements)-1],
				Boolean:  false,
				Line:     stmt.Line,
				Col:      stmt.Col,
			}
			object, err := TranslateExpression(exec, scope, objectExpr, table)
			if err != nil {
				return nil, err
			}
			result = append(result, object...)
			objectType, err := semantics.DeduceType(table, scope, objectExpr)
			if err != nil {
				return nil, genErrorf(objectExpr.Line, objectExpr.Col, "error deducing type: %s", err)
			}
			symbol, err := table.FindProperty(objectType, last.(ast.ObjectProperty).Name)
			if err != nil {
				return nil, genErrorf(stmt.Line, stmt.Col, "Unknown symbol %s.%s", objectType.String(), last.(ast.ObjectProperty).Name)
			}
			result = append(result, loc(stmt.Line, stmt.Col, instruction.SetPropertyInst{Index: symbol.Reference}))
		default:
			return nil, genErrorf(stmt.Line, stmt.Col, "Not a valid assignation.")
		}
	}

	return result, nil
}

func TranslateIfStmt(exec *execution.Execution, table symbols.Table, scope scope.Scope, stmt ast.IfStmt) ([]InstructionLoc, error) {
	var result []InstructionLoc

	instr, err := TranslateExpression(exec, scope, stmt.Condition, table)
	if err != nil {
		return nil, err
	}

	thenInstr, err := TranslateBlock(exec, table, scope, stmt.Then)
	if err != nil {
		return nil, err
	}

	blockSize := len(thenInstr)
	if len(stmt.Else) > 0 {
		blockSize++
	}

	FillLazyEvaluation(&instr, blockSize)
	result = append(result, instr...)
	result = append(result, thenInstr...)

	if len(stmt.Else) > 0 {
		elseInstr, err := TranslateBlock(exec, table, scope, stmt.Else)
		if err != nil {
			return nil, err
		}
		result = append(result, loc(stmt.Line, stmt.Col, instruction.JumpForwardInst{Address: len(elseInstr)}))
		result = append(result, elseInstr...)
	}
	return result, nil
}

func TranslateForStmt(exec *execution.Execution, table symbols.Table, scope scope.Scope, stmt ast.ForStmt) ([]InstructionLoc, error) {
	var result []InstructionLoc

	symbol, err := table.Find(stmt.What, scope)
	if err != nil {
		return nil, genErrorf(stmt.Line, stmt.Col, "unrecognised symbol '%s' in scope '%s'", stmt.What, scope)
	}

	if stmt.Over.Boolean {
		return nil, genErrorf(stmt.Line, stmt.Col, "boolean expression is not iterable")
	}

	instr, err := TranslateExpression(exec, scope, stmt.Over, table)
	if err != nil {
		return nil, err
	}

	result = append(result, instr...)
	result = append(result, loc(stmt.Line, stmt.Col, instruction.IterInst{}))

	body, err := TranslateBlock(exec, table, scope, stmt.Body)
	if err != nil {
		return nil, err
	}
	result = append(result, loc(stmt.Line, stmt.Col, instruction.LoopInstr{Skip: len(body) + 2}), loc(stmt.Line, stmt.Col, instruction.Store{Address: symbol.Reference}))
	result = append(result, body...)
	result = append(result, loc(stmt.Line, stmt.Col, instruction.JumpForwardInst{Address: -len(body) - 3}))

	// Replace continue and break
	for i, ins := range result {
		switch ins.Instruction.(type) {
		case instruction.TmpInst:
			switch ins.Instruction.(instruction.TmpInst).Kind {
			case instruction.TmpContinue:
				result[i].Instruction = instruction.JumpForwardInst{Address: -i + 1}
			case instruction.TmpBreak:
				result[i].Instruction = instruction.BreakInst{Address: len(result) - i - 1}
			}
		}
	}

	return result, nil
}

func TranslateWhileStmt(exec *execution.Execution, table symbols.Table, scope scope.Scope, stmt ast.WhileStmt) ([]InstructionLoc, error) {
	var result []InstructionLoc

	body, err := TranslateBlock(exec, table, scope, stmt.Body)
	if err != nil {
		return nil, err
	}

	result = append(result, loc(stmt.Line, stmt.Col, instruction.JumpForwardInst{Address: len(body)}))
	result = append(result, body...)

	for i, elem := range stmt.Condition.Elements {
		switch elem.(type) {
		case ast.BooleanOp:
			prev := elem.(ast.BooleanOp)
			var jumpOnFalse, jumpToElse bool
			if i == len(stmt.Condition.Elements)-1 {
				jumpOnFalse = !prev.JumpOnFalse
				jumpToElse = prev.JumpToElse
			} else {
				jumpOnFalse = prev.JumpOnFalse
				jumpToElse = !prev.JumpToElse
			}

			stmt.Condition.Elements[i] = ast.BooleanOp{
				Level:       prev.Level,
				JumpOnFalse: jumpOnFalse,
				JumpToElse:  jumpToElse,
				Line:        prev.Line,
				Col:         prev.Col,
			}
		}
	}

	condition, err := TranslateExpression(exec, scope, stmt.Condition, table)
	if err != nil {
		return nil, err
	}
	FillLazyEvaluation(&condition, -len(body)-len(condition))
	result = append(result, condition...)

	// Replace continue and break
	for i, ins := range result {
		switch ins.Instruction.(type) {
		case instruction.TmpInst:
			switch ins.Instruction.(instruction.TmpInst).Kind {
			case instruction.TmpContinue:
				result[i].Instruction = instruction.JumpForwardInst{Address: -i - 1}
			case instruction.TmpBreak:
				result[i].Instruction = instruction.JumpForwardInst{Address: len(result) - i - 1}
			}
		}
	}

	return result, nil
}

func TranslateInterfaceDecl(table symbols.Table, decl ast.InterfaceDecl) ([]InstructionLoc, error) {
	id, err := table.GetTypeID(decl.Name)
	if err != nil {
		return nil, genErrorf(decl.Line, decl.Col, "type error: %s", err)
	}

	details, err := table.GetTypeDetails(id)
	if err != nil {
		return nil, genErrorf(decl.Line, decl.Col, "type error: %s", err)
	}
	if details.TypeVariant() != types.VariantInterface {
		return nil, genErrorf(decl.Line, decl.Col, "non interface type passed as an interface in declaration")
	}

	var result InstructionSet
	ifaceDetails := details.(types.Interface)

	for i, met := range ifaceDetails.Contract {
		symbol, err := table.Find(symbols.FormatMethod(id, met.Name), scope.Global)
		if err != nil {
			return nil, genErrorf(decl.Line, decl.Col, "symbol error: %s", err)
		}

		result.add(decl.Line, decl.Col, instruction.PushInst{
			Arg: model.InterfaceMethod{
				MethodId: i,
			},
		})
		result.add(decl.Line, decl.Col, instruction.StoreGlobal{
			Address: symbol.Reference,
		})
	}

	return result.instructions, nil
}

func TranslateBlock(exec *execution.Execution, table symbols.Table, sc scope.Scope, elements []interface{}) ([]InstructionLoc, error) {
	var result []InstructionLoc
	for _, element := range elements {
		var instr []InstructionLoc
		var err error

		switch element.(type) {
		case ast.IfStmt:
			instr, err = TranslateIfStmt(exec, table, sc, element.(ast.IfStmt))
		case ast.ForStmt:
			instr, err = TranslateForStmt(exec, table, sc, element.(ast.ForStmt))
		case ast.WhileStmt:
			instr, err = TranslateWhileStmt(exec, table, sc, element.(ast.WhileStmt))
		case ast.AssignStmt:
			instr, err = TranslateAssignStmt(exec, table, sc, element.(ast.AssignStmt))
		case ast.VariableDecl:
			instr, err = TranslateVariableDecl(exec, table, sc, element.(ast.VariableDecl))
		case ast.ConstDecl:
			instr, err = TranslateConstDecl(exec, table, sc, element.(ast.ConstDecl))
		case ast.ReturnStmt:
			instr, err = TranslateReturnStmt(exec, table, sc, element.(ast.ReturnStmt))
		case ast.FailStmt:
			instr, err = TranslateFailStmt(exec, table, sc, element.(ast.FailStmt))
		case ast.FunctionDefinition:
			funcDef := element.(ast.FunctionDefinition)
			if sc != scope.Base {
				return nil, genErrorf(funcDef.Line, funcDef.Col, "cannot define a function within another function")
			}
			instr, err = TranslateFuncDecl(exec, table, sc, funcDef, 0)
			if err != nil {
				return nil, err
			}
			// We define functions first!
			result = append(instr, result...)
			continue
		case ast.ContinueStmt:
			cont := element.(ast.ContinueStmt)
			result = append(
				result,
				loc(cont.Line, cont.Col, instruction.TmpInst{Kind: 0}),
			)
			continue
		case ast.BreakStmt:
			bre := element.(ast.BreakStmt)
			result = append(
				result,
				loc(bre.Line, bre.Col, instruction.TmpInst{Kind: 1}),
			)
			continue
		case ast.Enum:
			enum := element.(ast.Enum)
			for i, s := range enum.Values {
				typeID, err := table.Find(s, scope.Global)
				if err != nil {
					return nil, err
				}
				result = append(result, loc(enum.Line, enum.Col, instruction.PushInst{
					Arg: model.Int32Object(i),
				}))
				result = append(result, loc(enum.Line, enum.Col, instruction.StoreGlobal{
					Address: typeID.Reference,
				}))
			}
		case ast.InterfaceDecl:
			ifaceDecl := element.(ast.InterfaceDecl)
			decl, err := TranslateInterfaceDecl(table, ifaceDecl)
			if err != nil {
				return nil, err
			}
			result = append(result, decl...)
		case ast.Expression:
			expr := element.(ast.Expression)
			instr, err = TranslateExpression(exec, sc, expr, table)
			if err != nil {
				return nil, err
			}
			if expr.Boolean {
				// Calculate the result but we don't want to store it anywhere
				// Also the last instruction is useless, replace with pop
				FillLazyEvaluation(&instr, 0)
				instr[len(instr)-1].Instruction = instruction.PopInst{Amount: 1}
				result = append(result, instr...)
				continue
			}
			if len(instr) > 0 {
				result = append(result, instr...)
				result = append(result, loc(expr.Line, expr.Col, instruction.PopInst{Amount: 1}))
			}
			continue
		}
		if err != nil {
			return nil, err
		}
		result = append(result, instr...)
	}

	return result, nil
}

func GenerateInstructions(exec *execution.Execution, table symbols.Table, scope scope.Scope, elements []interface{}) (*execution.Execution, error) {
	var result []InstructionLoc
	if exec == nil {
		exec = execution.NewExecution()
	}

	localRefs := table.ScopeReferences(scope)
	if localRefs > 0 {
		result = append(result, loc(0, 0, instruction.Slide{Amount: localRefs}))
	}
	block, err := TranslateBlock(exec, table, scope, elements)
	if err != nil {
		return nil, err
	}
	pureInst, locs := SeperateInfo(append(result, block...))
	exec.SetMainBucket("main", 0, pureInst, locs)
	return exec, nil
}
