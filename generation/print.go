package generation

import (
	"fmt"

	"gitlab.com/egosummiki/litequel/ast"
)

func printIdent(indent int) {
	for i := 0; i < indent; i++ {
		fmt.Print("   ")
	}
}

func PrintExpression(elements []interface{}, indent int) {
	for _, el := range elements {
		printIdent(indent)
		switch el.(type) {
		case ast.FunctionCall:
			fmt.Printf("call:\n")
			for _, arg := range el.(ast.FunctionCall).Arguments {
				PrintExpression(arg.Elements, indent+1)
			}
		case ast.ObjectProperty:
			fmt.Printf("property %s\n", el.(ast.ObjectProperty).Name)
		case ast.ArrayLiteral:
			fmt.Print("list:\n")
			for _, e := range el.(ast.ArrayLiteral).Elements {
				PrintExpression(e.Elements, indent+1)
			}
		default:
			fmt.Println("Unimplemented")
		}
	}
}

func PrintAST(elements []interface{}, indent int) {
	for _, el := range elements {
		switch el.(type) {
		case ast.IfStmt:
			printIdent(indent)
			fmt.Print("if:\n")
			PrintExpression(el.(ast.IfStmt).Condition.Elements, indent+1)
			printIdent(indent)
			fmt.Print("then:\n")
			PrintAST(el.(ast.IfStmt).Then, indent+1)
			printIdent(indent)
			fmt.Print("else:\n")
			PrintAST(el.(ast.IfStmt).Else, indent+1)
		case ast.ForStmt:
			printIdent(indent)
			fmt.Printf("for %s in:\n", el.(ast.ForStmt).What)
			PrintExpression(el.(ast.ForStmt).Over.Elements, indent+1)
			printIdent(indent)
			fmt.Print("loop:\n")
			PrintAST(el.(ast.ForStmt).Body, indent+1)
		case ast.WhileStmt:
			printIdent(indent)
			fmt.Println("while:")
			PrintExpression(el.(ast.WhileStmt).Condition.Elements, indent+1)
			printIdent(indent)
			fmt.Print("loop:\n")
			PrintAST(el.(ast.WhileStmt).Body, indent+1)
		case ast.AssignStmt:
			printIdent(indent)
			fmt.Printf("assign to: \n")
			PrintExpression(el.(ast.AssignStmt).Target.Elements, indent+1)
			printIdent(indent)
			fmt.Printf("value: \n")
			PrintExpression(el.(ast.AssignStmt).Value.Elements, indent+1)
		case ast.FunctionDefinition:
			printIdent(indent)
			fmt.Printf("function %s (%v):\n", el.(ast.FunctionDefinition).Name, el.(ast.FunctionDefinition).Params)
			PrintAST(el.(ast.FunctionDefinition).Body, indent+1)
		case ast.Expression:
			printIdent(indent)
			fmt.Printf("expression boolean=%v: \n", el.(ast.Expression).Boolean)
			PrintExpression(el.(ast.Expression).Elements, indent+1)
		default:
			printIdent(indent)
			fmt.Println("Unimplemented")
		}
	}
}
