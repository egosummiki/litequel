package generation

import (
	"github.com/google/uuid"
	"gitlab.com/egosummiki/litequel/ast"
	"gitlab.com/egosummiki/litequel/runtime/execution"
	"gitlab.com/egosummiki/litequel/runtime/instruction"
	"gitlab.com/egosummiki/litequel/symbols/scope"
	"gitlab.com/egosummiki/litequel/types"

	"gitlab.com/egosummiki/litequel/model/function"
	"gitlab.com/egosummiki/litequel/symbols"
)

func createFilterFunction(exec *execution.Execution, table symbols.Table, sc scope.Scope, objType types.ID, body ast.Expression) (function.Function, error) {
	details, err := table.GetTypeDetails(objType)
	if err != nil {
		return function.Function{}, err
	}

	collection, ok := details.(types.Collection)
	if !ok {
		return function.Function{}, genError(body.Line, body.Col, "filtration can be used only on collections")
	}

	resultType := collection.ElementType()
	funcName := uuid.New().String()

	resultTypeDetails, err := table.GetTypeDetails(resultType)
	if err != nil {
		return function.Function{}, err
	}
	if resultTypeDetails.TypeVariant() != types.VariantStruct {
		return function.Function{}, genError(body.Line, body.Col, "filtered type must be a struct")
	}
	fields := resultTypeDetails.(types.TypeStruct).Fields

	funcObj := function.Function{
		NumArgs: len(fields) + 1,
	}

	newScope := sc.Sub(funcName)
	funcObj.Locals = 0

	err = table.RegisterLocal("self", newScope, resultType)
	if err != nil {
		return funcObj, err
	}
	for _, field := range fields {
		err = table.RegisterLocal(field.Name, newScope, field.TypeID)
	}

	filterBody, err := TranslateExpressionCheckBool(exec, newScope, body, table)
	if err != nil {
		return funcObj, err
	}
	if len(filterBody) == 0 {
		return funcObj, genError(body.Line, body.Col, "filter statement's body is empty")
	}
	filterBody = append(filterBody, loc(body.Line, body.Col, instruction.ReturnInst{}))

	instructions, locations := SeperateInfo(filterBody)
	funcObj.BucketID = exec.AddBucket(newScope, 0, instructions, locations)

	return funcObj, nil
}
