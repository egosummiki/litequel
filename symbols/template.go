package symbols

import (
	"gitlab.com/egosummiki/litequel/ast"
)

type Template interface {
	TemName() string
	TemParams() []ast.TemplateParam
}
