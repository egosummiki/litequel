package symbols

import (
	"gitlab.com/egosummiki/litequel/symbols/scope"
	"gitlab.com/egosummiki/litequel/types"
)

type Details struct {
	Variant   Variant
	Name      string
	Reference int
	Frame     int      // Just for enclosed variable
	Type      types.ID // Return type if function
}

type Variant byte

const (
	NoScope Variant = iota
	Local
	Global
	Property
	Enclosed
)

type Table interface {
	RegisterLocal(name string, scp scope.Scope, typeID types.ID) error
	MustRegisterLocal(name string, scope scope.Scope, typeID types.ID)
	RegisterGlobal(name string, typeID types.ID) error
	MustRegisterGlobal(name string, typeID types.ID)
	RegisterProperty(receiver types.ID, name string, typeID types.ID) error
	MustRegisterProperty(receiver types.ID, name string, typeID types.ID)
	RegisterType(details types.Details) (types.ID, error)
	MustRegisterType(details types.Details) types.ID
	RegisterMethod(typeID types.ID, method types.Method) (types.ID, error)

	GetTypeDetails(typeID types.ID) (types.Details, error)
	GetTypeID(name string) (types.ID, error)
	Find(name string, scp scope.Scope) (Details, error)
	MustFind(name string, scp scope.Scope) Details
	FindProperty(typeID types.ID, name string) (Details, error)
	MustFindProperty(typeID types.ID, name string) Details
	FindType(details types.Details) types.ID

	ScopeReferences(scope scope.Scope) int

	GenerateReflector() *SymbolReflector

	RegisterTemplate(tem Template) error
	FindTemplate(name string) (Template, error)
}
