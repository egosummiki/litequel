package symbols

import (
	"fmt"

	"gitlab.com/egosummiki/litequel/ast"
	"gitlab.com/egosummiki/litequel/types"
)

func FormatMethod(typeID types.ID, name string) string {
	return fmt.Sprintf("%04X.%s", typeID, name)
}

func FormatNamespace(namespace string, identifier string) string {
	return fmt.Sprintf("%s::%s", namespace, identifier)
}

func FormatTemplateName(name string, args []ast.TypeOrLiteral) string {
	return fmt.Sprintf("%s%v", name, args)
}
