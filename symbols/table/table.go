package table

import (
	"errors"

	"gitlab.com/egosummiki/litequel/ast"
	"gitlab.com/egosummiki/litequel/symbols"
	"gitlab.com/egosummiki/litequel/symbols/scope"
	"gitlab.com/egosummiki/litequel/types"
)

type SymbolStore interface {
	StoreTypeAs(id types.ID, details types.Details) error
	StoreType(details types.Details) (types.ID, bool, error)
	GetTypeID(name string) (types.ID, error)
	GetTypeDetails(id types.ID) (types.Details, error)
	StoreSymbol(path scope.Path, details symbols.Details) error
	GetSymbol(name scope.Path) (symbols.Details, error)
	TypeDetails() []types.Details
	TypeIDs() map[string]types.ID
	StoreTemplate(template symbols.Template, allowRedef bool) error
	GetTemplate(name string) (symbols.Template, error)
}

type SymbolCounter interface {
	Increase(s scope.Scope) int
	References(s scope.Scope) int
}

type Table struct {
	store   SymbolStore
	counter SymbolCounter
}

func New(store SymbolStore, counter SymbolCounter) *Table {
	return &Table{store: store, counter: counter}
}

func (t Table) RegisterLocal(name string, scp scope.Scope, typeID types.ID) error {
	return t.store.StoreSymbol(scp.Path(name), symbols.Details{
		Variant:   symbols.Local,
		Name:      name,
		Reference: t.counter.Increase(scp),
		Type:      typeID,
	})
}

func (t Table) MustRegisterLocal(name string, scope scope.Scope, typeID types.ID) {
	if err := t.RegisterLocal(name, scope, typeID); err != nil {
		panic(err)
	}
}

func (t Table) RegisterGlobal(name string, typeID types.ID) error {
	return t.store.StoreSymbol(scope.Global.Path(name), symbols.Details{
		Variant:   symbols.Global,
		Name:      name,
		Reference: t.counter.Increase(scope.Global),
		Type:      typeID,
	})
}

func (t *Table) MustRegisterGlobal(name string, typeID types.ID) {
	if err := t.RegisterGlobal(name, typeID); err != nil {
		panic(err)
	}
}

func (t Table) RegisterProperty(receiver types.ID, name string, typeID types.ID) error {
	details, err := t.GetTypeDetails(typeID)
	if err != nil {
		return err
	}

	var scp scope.Scope
	var path scope.Path

	if details.TypeVariant() == types.VariantFunction {
		scp = scope.Global
		path = scp.TypeScope(receiver).Path(name)
	} else {
		scp = scope.Property.TypeScope(receiver)
		path = scp.Path(name)
	}

	return t.store.StoreSymbol(path, symbols.Details{
		Variant:   symbols.Property,
		Name:      name,
		Reference: t.counter.Increase(scp),
		Type:      typeID,
	})
}

func (t *Table) MustRegisterProperty(receiver types.ID, name string, typeID types.ID) {
	if err := t.RegisterProperty(receiver, name, typeID); err != nil {
		panic(err)
	}
}

func (t Table) FindType(details types.Details) types.ID {
	id, err := t.store.GetTypeID(details.TypeName())
	if err != nil {
		return types.TypeUnknown
	}
	return id
}

func (t Table) RegisterType(details types.Details) (types.ID, error) {
	id, alreadyExists, err := t.store.StoreType(details)
	if err != nil {
		return types.TypeUnknown, err
	}

	if alreadyExists {
		return id, nil
	}

	if details.TypeVariant() == types.VariantStruct {
		fields := details.(types.TypeStruct).Fields
		for _, f := range fields {
			err := t.RegisterProperty(id, f.Name, f.TypeID)
			if err != nil {
				return types.TypeUnknown, err
			}
		}
	}

	return id, nil
}

func (t Table) MustRegisterType(details types.Details) types.ID {
	typeID, err := t.RegisterType(details)
	if err != nil {
		panic(err)
	}
	return typeID
}

func (t Table) RegisterMethod(typeID types.ID, method types.Method) (types.ID, error) {
	details, err := t.GetTypeDetails(typeID)
	if err != nil {
		return 0, err
	}

	if details.TypeVariant() != types.VariantStruct {
		return 0, errors.New("method can be registered only on a struct")
	}

	structDet := details.(types.TypeStruct)
	structDet.Methods = append(structDet.Methods, method)

	if err := t.store.StoreTypeAs(typeID, structDet); err != nil {
		return 0, err
	}

	methodType, err := t.RegisterType(types.NewMethod(method.ReturnType, method.Args, typeID, method.Variadic))
	if err != nil {
		return 0, err
	}

	err = t.RegisterGlobal(symbols.FormatMethod(typeID, method.Name), methodType)
	if err != nil {
		return 0, err
	}

	return methodType, nil
}

func (t Table) GetTypeDetails(typeID types.ID) (types.Details, error) {
	return t.store.GetTypeDetails(typeID)
}

func (t Table) GetTypeID(name string) (types.ID, error) {
	return t.store.GetTypeID(name)
}

func (t Table) Find(name string, scp scope.Scope) (symbols.Details, error) {
	details, err := t.store.GetSymbol(scp.Path(name))
	if err == nil {
		return details, nil
	}

	// TODO: Bring back closures

	details, err = t.store.GetSymbol(scope.Global.Path(name))
	if err == nil {
		return details, nil
	}

	return symbols.Details{}, symbols.Errorf("could not find symbol %s", name)
}

func (t Table) MustFind(name string, scp scope.Scope) symbols.Details {
	details, err := t.Find(name, scp)
	if err != nil {
		panic(err)
	}
	return details
}

func (t Table) FindProperty(typeID types.ID, name string) (symbols.Details, error) {
	symbol, err := t.store.GetSymbol(scope.Property.TypeScope(typeID).Path(name))
	if err == nil {
		return symbol, nil
	}

	symbol, err = t.store.GetSymbol(scope.Global.TypeScope(typeID).Path(name))
	if err == nil {
		return symbol, nil
	}

	details, err := t.store.GetTypeDetails(typeID)
	if err != nil {
		return symbols.Details{}, symbols.Errorf("unknown type of the property")
	}

	if details.TypeVariant() == types.VariantGeneric {
		return t.FindProperty(details.(types.TypeGeneric).Type, name)
	}

	return symbols.Details{}, symbols.Errorf("property %s of type %s not found", name, details.TypeName())
}

func (t Table) MustFindProperty(typeID types.ID, name string) symbols.Details {
	details, err := t.FindProperty(typeID, name)
	if err != nil {
		panic(err)
	}
	return details
}

func (t *Table) ScopeReferences(scope scope.Scope) int {
	return t.counter.References(scope)
}

func (t Table) GenerateReflector() *symbols.SymbolReflector {
	r := symbols.SymbolReflector{
		Types:      map[types.ID]string{},
		TypeFields: map[types.ID][]types.TypeField{},
		Details:    map[types.ID]types.Details{},
	}

	for i, details := range t.store.TypeDetails() {
		r.Details[types.ID(i)] = details
		if details.TypeVariant() == types.VariantStruct {
			structDetails := details.(types.TypeStruct)
			r.TypeFields[types.ID(i)] = structDetails.Fields
		}
	}

	for k, v := range t.store.TypeIDs() {
		r.Types[v] = k
	}

	return &r
}

func (t Table) RegisterTemplate(tem symbols.Template) error {
	funcTem, ok := tem.(ast.FunctionDefinition)
	if ok && len(funcTem.Receiver.Name) != 0{
		receiverTem, err := t.store.GetTemplate(funcTem.Receiver.Name)
		if err == nil {
			structTem, ok := receiverTem.(ast.StructDefinition)
			if ok {
				structTem.MethodTemplates = append(structTem.MethodTemplates, funcTem)
				_ = t.store.StoreTemplate(structTem, true)
				return nil
			}
		}
	}

	return t.store.StoreTemplate(tem, false)
}

func (t Table) FindTemplate(name string) (symbols.Template, error) {
	return t.store.GetTemplate(name)
}
