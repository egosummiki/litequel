package store

import (
	"errors"
	"fmt"

	"gitlab.com/egosummiki/litequel/ast"
	"gitlab.com/egosummiki/litequel/symbols"
	"gitlab.com/egosummiki/litequel/symbols/scope"
	"gitlab.com/egosummiki/litequel/types"
)

type SymbolStore struct {
	symbolDetails map[scope.Path]symbols.Details
	typeNames     map[string]types.ID
	typeDetails   []types.Details
	templates     map[string]symbols.Template
}

func New() *SymbolStore {
	return &SymbolStore{
		symbolDetails: map[scope.Path]symbols.Details{},
		typeDetails:   []types.Details{},
		typeNames:     map[string]types.ID{},
		templates:     map[string]symbols.Template{},
	}
}

func (s *SymbolStore) StoreTypeAs(id types.ID, details types.Details) error {
	if int(id) >= len(s.typeDetails) {
		return errors.New("invalid type id")
	}
	s.typeDetails[id] = details
	s.typeNames[details.TypeName()] = id
	return nil
}

func (s *SymbolStore) StoreType(details types.Details) (types.ID, bool, error) {
	id := s.typeNames[details.TypeName()]
	if id != types.TypeUnknown {
		if types.DetailsEqual(s.typeDetails[id], details) {
			return id, true, nil
		}
		return types.TypeUnknown, false, symbols.Errorf("redefined type %s", details.TypeName())
	}

	id = types.ID(len(s.typeDetails))
	s.typeDetails = append(s.typeDetails, details)
	s.typeNames[details.TypeName()] = id
	return id, false, nil
}

func (s *SymbolStore) GetTypeID(name string) (types.ID, error) {
	id, ok := s.typeNames[name]
	if !ok {
		return 0, symbols.Errorf("unknown type %s", name)
	}
	return id, nil
}

func (s *SymbolStore) GetTypeDetails(id types.ID) (types.Details, error) {
	if int(id) >= len(s.typeDetails) {
		return nil, symbols.Errorf("invalid type id %d", id)
	}
	return s.typeDetails[id], nil
}

func (s *SymbolStore) StoreSymbol(path scope.Path, details symbols.Details) error {
	_, ok := s.symbolDetails[path]
	if ok {
		return symbols.Errorf("redefined symbol %s", details.Name)
	}
	s.symbolDetails[path] = details
	return nil
}

func (s *SymbolStore) GetSymbol(path scope.Path) (symbols.Details, error) {
	details, ok := s.symbolDetails[path]
	if !ok {
		return symbols.Details{}, symbols.Errorf("cannot find symbol %s", path)
	}
	return details, nil
}

func (s *SymbolStore) TypeDetails() []types.Details {
	return s.typeDetails
}

func (s *SymbolStore) TypeIDs() map[string]types.ID {
	return s.typeNames
}

func (s *SymbolStore) StoreTemplate(template symbols.Template, allowRedefinition bool) error {
	if !allowRedefinition {
		_, ok := s.templates[template.TemName()]
		if ok {
			return errors.New("redefined template")
		}
	}

	s.templates[template.TemName()] = template
	return nil
}

func (s *SymbolStore) GetTemplate(name string) (symbols.Template, error) {
	template, ok := s.templates[name]
	if !ok {
		return ast.StructDefinition{}, fmt.Errorf("invalid template name %s", name)
	}

	return template, nil
}
