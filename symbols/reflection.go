package symbols

import (
	"gitlab.com/egosummiki/litequel/runtime"
	"gitlab.com/egosummiki/litequel/types"
)

type SymbolReflector struct {
	Types      map[types.ID]string
	TypeFields map[types.ID][]types.TypeField
	Generics   map[types.ID]runtime.GenericType
	Constants  map[string]int
	Details    map[types.ID]types.Details
}

func (r *SymbolReflector) GetTypeName(typeID types.ID) string {
	return r.Types[typeID]
}

func (r *SymbolReflector) GetTypeAttribs(typeID types.ID) []types.TypeField {
	return r.TypeFields[typeID]
}

func (r *SymbolReflector) GetGeneric(typeID types.ID) runtime.GenericType {
	return r.Generics[typeID]
}

func (r *SymbolReflector) GetConstant(name string) int {
	return r.Constants[name]
}

func (r *SymbolReflector) GetDetails(typeID types.ID) types.Details {
	return r.Details[typeID]
}