package symbols

import "fmt"

func ErrorWrap(err error) error {
	return fmt.Errorf("%w", err)
}

func Errorf(template string, a ...interface{}) error {
	return ErrorWrap(fmt.Errorf(template, a...))
}
