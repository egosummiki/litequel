package scope

import (
	"fmt"
	"strings"

	"gitlab.com/egosummiki/litequel/types"
)

type Scope string

const Base Scope = ""
const Global Scope = "%global%"
const Property Scope = "%property%"

func (s Scope) Sub(name string) Scope {
	if s == Base {
		return Scope(name)
	}
	return s + "." + Scope(name)
}

func (s Scope) Path(name string) Path {
	return Path(s) + "." + Path(name)
}

func (s Scope) TypeScope(id types.ID) Scope {
	return Scope(fmt.Sprintf("%s.%04X", s, id))
}

func (s Scope) Parent() Scope {
	last := strings.LastIndexByte(string(s), '.')
	if last == -1 {
		return Base
	}
	return s[:last]
}

func (s Scope) String() string {
	return string(s)
}

type Path string

type SymbolCounter struct{
	references map[Scope]int
}

func NewSymbolCounter() *SymbolCounter {
	return &SymbolCounter{references: map[Scope]int{}}
}


func (sc *SymbolCounter) Increase(s Scope) int {
	count, ok := sc.references[s]
	if !ok {
		sc.references[s] = 1
		return 0
	}

	sc.references[s]++
	return count
}

func (sc *SymbolCounter) References(s Scope) int {
	return sc.references[s]
}


