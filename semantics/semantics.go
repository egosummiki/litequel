package semantics

import (
	"fmt"

	"gitlab.com/egosummiki/litequel/ast"
	"gitlab.com/egosummiki/litequel/symbols"
	"gitlab.com/egosummiki/litequel/symbols/scope"
	"gitlab.com/egosummiki/litequel/types"
)

type Table interface {
	RegisterLocal(name string, scp scope.Scope, typeID types.ID) error
	RegisterGlobal(name string, typeID types.ID) error
	RegisterProperty(receiver types.ID, name string, typeID types.ID) error
	RegisterType(details types.Details) (types.ID, error)
	RegisterMethod(typeID types.ID, method types.Method) (types.ID, error)

	GetTypeDetails(typeID types.ID) (types.Details, error)
	GetTypeID(name string) (types.ID, error)
	Find(name string, scp scope.Scope) (symbols.Details, error)
	FindProperty(typeID types.ID, name string) (symbols.Details, error)
	FindType(details types.Details) types.ID

	RegisterTemplate(tem symbols.Template) error
	FindTemplate(name string) (symbols.Template, error)
}

type Analysis struct {
	Table Table
	Ast   []interface{}
}

func (a *Analysis) PopulateSymbols(scp scope.Scope) error {
	return a.checkBlock(scp, a.Ast)
}

func (a *Analysis) checkBlock(scp scope.Scope, node []interface{}) error {
	for _, element := range node {
		switch element.(type) {
		case ast.ConstDecl:
			if err := a.checkConstDeclStmt(scp, element.(ast.ConstDecl)); err != nil {
				return err
			}
		case ast.IfStmt:
			if err := a.checkIfStmt(scp, element.(ast.IfStmt)); err != nil {
				return err
			}
		case ast.ForStmt:
			if err := a.checkForStmt(scp, element.(ast.ForStmt)); err != nil {
				return err
			}
		case ast.WhileStmt:
			if err := a.checkWhileStmt(scp, element.(ast.WhileStmt)); err != nil {
				return err
			}
		case ast.FunctionDefinition:
			if err := a.CheckFunctionDeclStmt(scp, element.(ast.FunctionDefinition), 0); err != nil {
				return err
			}
		case ast.VariableDecl:
			if err := a.checkVariableDeclStmt(scp, element.(ast.VariableDecl)); err != nil {
				return err
			}
		case ast.StructDefinition:
			if err := a.checkStructDefinitionStmt(scp, element.(ast.StructDefinition)); err != nil {
				return err
			}
		case ast.OrStmt:
			if err := a.checkOrStmt(scp, element.(ast.OrStmt)); err != nil {
				return err
			}
		case ast.Enum:
			if err := a.checkEnumDecl(scp, element.(ast.Enum)); err != nil {
				return err
			}
		case ast.InterfaceDecl:
			if err := a.checkInterfaceDecl(scp, element.(ast.InterfaceDecl)); err != nil {
				return err
			}
		default:
			continue
		}
	}
	return nil
}

func (a *Analysis) checkConstDeclStmt(scp scope.Scope, stmt ast.ConstDecl) error {
	var typeID types.ID

	if len(stmt.TypeSpec.Name) != 0 {
		var err error
		typeID, err = NodeToTypeID(a.Table, scp, stmt.TypeSpec)
		if err != nil {
			return err
		}
	} else {
		var err error
		typeID, err = DeduceType(a.Table, scp, stmt.Value)
		if err != nil {
			return err
		}
	}

	if err := a.Table.RegisterGlobal(stmt.Name, typeID); err != nil {
		return semErrorf(stmt.Line, stmt.Col, "type error: %s", err)
	}
	return nil
}

func (a *Analysis) checkIfStmt(scp scope.Scope, stmt ast.IfStmt) error {
	if err := a.checkBlock(scp, stmt.Then); err != nil {
		return err
	}
	if err := a.checkBlock(scp, stmt.Else); err != nil {
		return err
	}
	return nil
}

func (a *Analysis) checkForStmt(scp scope.Scope, stmt ast.ForStmt) error {
	overType, err := DeduceType(a.Table, scp, stmt.Over)
	if err != nil {
		return semErrorf(stmt.Over.Line, stmt.Over.Col, "error deducing type: %s", err)
	}

	details, err := a.Table.GetTypeDetails(overType)
	if err != nil {
		return semErrorf(stmt.Over.Line, stmt.Over.Col, "type error: %s", err)
	}

	var subType types.ID

	collection, isCollection := details.(types.Collection)

	switch {
	case isCollection:
		subType = collection.ElementType()
	case overType == types.TypeString:
		subType = types.TypeString
	case overType == types.TypeInt32:
		subType = types.TypeInt32
	case overType == types.TypeRange:
		subType = types.TypeInt32
	default:
		return semErrorf(stmt.Line, stmt.Col, "invalid loop type")
	}

	if err := a.Table.RegisterLocal(stmt.What, scp, subType); err != nil {
		return semErrorf(stmt.Over.Line, stmt.Over.Col, "symbol error: %s", err)
	}

	if err := a.checkBlock(scp, stmt.Body); err != nil {
		return err
	}

	return nil
}

func (a *Analysis) checkWhileStmt(scp scope.Scope, stmt ast.WhileStmt) error {
	conditionType, err := DeduceType(a.Table, scp, stmt.Condition)
	if err != nil {
		return semErrorf(stmt.Condition.Line, stmt.Condition.Col, "type error: %s", err)
	}
	if conditionType != types.TypeBool {
		return semErrorf(stmt.Condition.Line, stmt.Condition.Col, "while condition must be a boolean")
	}

	if err := a.checkBlock(scp, stmt.Body); err != nil {
		return err
	}
	return nil
}

const thisArgument = "this"
const funcArgument = "func"

func (a *Analysis) CheckFunctionDeclStmt(scp scope.Scope, stmt ast.FunctionDefinition, receiverType types.ID) error {
	if len(stmt.TemplateParams) != 0 {
		if err := a.Table.RegisterTemplate(stmt); err != nil {
			return semErrorf(stmt.Line, stmt.Col, "template error: %s", err)
		}
		return nil
	}

	var localScope scope.Scope

	if receiverType != 0 || len(stmt.Receiver.Name) != 0 {
		var err error
		if receiverType == 0  {
			receiverType, err = NodeToTypeID(a.Table, scp, stmt.Receiver)
			if err != nil {
				return err
			}
		}

		localScope = scp.Sub(symbols.FormatMethod(receiverType, stmt.Name))

		err = a.Table.RegisterLocal(thisArgument, localScope, receiverType)
		if err != nil {
			return semErrorf(stmt.Receiver.Line, stmt.Receiver.Col, "type error: %s", err)
		}
		err = a.Table.RegisterLocal(funcArgument, localScope, types.TypeUnknown)
		if err != nil {
			return semErrorf(stmt.Receiver.Line, stmt.Receiver.Col, "type error: %s", err)
		}
	} else {
		localScope = scp.Sub(stmt.Name)
	}

	argTypes := make([]types.ID, len(stmt.Params))
	for i, arg := range stmt.Params {
		typeID, err := NodeToTypeID(a.Table, localScope, arg.TypeSpec)
		if err != nil {
			return semErrorf(arg.TypeSpec.Line, arg.TypeSpec.Col, "type error: %s", err)
		}
		argTypes[i] = typeID

		err = a.Table.RegisterLocal(arg.Name, localScope, typeID)
		if err != nil {
			return semErrorf(stmt.Receiver.Line, stmt.Receiver.Col, "type error: %s", err)
		}
	}

	if err := a.checkBlock(localScope, stmt.Body); err != nil {
		return err
	}

	returnType, err := NodeToTypeID(a.Table, scp, stmt.Return)
	if err != nil {
		return semErrorf(stmt.Return.Line, stmt.Return.Col, "type error: %s", err)
	}

	if receiverType != types.TypeUnknown {
		_, err := a.Table.RegisterMethod(receiverType, types.Method{
			Name:       stmt.Name,
			Args:       argTypes,
			ReturnType: returnType,
			Variadic:   stmt.Variadic,
		})
		if err != nil {
			return semErrorf(stmt.Return.Line, stmt.Return.Col, "symbol error: %s", err)
		}
		return nil
	}

	funcType, err := a.Table.RegisterType(types.NewFunction(returnType, argTypes, stmt.Variadic))
	if err != nil {
		return semErrorf(stmt.Line, stmt.Col, "type error: %s", err)
	}

	if scp == scope.Base {
		if err := a.Table.RegisterGlobal(stmt.Name, funcType); err != nil {
			return semErrorf(stmt.Line, stmt.Col, "symbol error: %s", err)
		}
	} else {
		if err := a.Table.RegisterLocal(stmt.Name, scp, funcType); err != nil {
			return semErrorf(stmt.Line, stmt.Col, "symbol error: %s", err)
		}
	}
	return nil
}

func (a *Analysis) checkVariableDeclStmt(scp scope.Scope, stmt ast.VariableDecl) error {
	var varType types.ID
	if len(stmt.TypeSpec.Name) != 0 {
		var err error
		varType, err = NodeToTypeID(a.Table, scp, stmt.TypeSpec)
		if err != nil {
			return err
		}
	} else {
		var err error
		varType, err = DeduceType(a.Table, scp, stmt.Value)
		if err != nil {
			return semErrorf(stmt.Value.Line, stmt.Value.Col, "type error: %s", err)
		}
	}

	if err := a.Table.RegisterLocal(stmt.Name, scp, varType); err != nil {
		return semErrorf(stmt.Line, stmt.Col, "symbol error: %s", err)
	}
	return nil
}

func (a *Analysis) checkStructDefinitionStmt(scp scope.Scope, stmt ast.StructDefinition) error {
	if len(stmt.TemplateParams) != 0 {
		if err := a.Table.RegisterTemplate(stmt); err != nil {
			return semErrorf(stmt.Line, stmt.Col, "template error: %s", err)
		}
		return nil
	}

	properties := make([]types.TypeField, len(stmt.Properties))
	for i, prop := range stmt.Properties {
		typeID, err := NodeToTypeID(a.Table, scp, prop.PropType)
		if err != nil {
			return err
		}

		var annots []types.Annotation
		if len(prop.Annotations) != 0 {
			annots = make([]types.Annotation, len(prop.Annotations))
			for j, annot := range prop.Annotations {
				annots[j] = types.Annotation(annot)
			}
		}

		properties[i] = types.TypeField{
			TypeID: typeID,
			Annots: annots,
			Name:   prop.Name,
		}
	}

	if _, err := a.Table.RegisterType(types.NewTypeStruct(stmt.Name, properties)); err != nil {
		return semErrorf(stmt.Line, stmt.Col, "type error: %s", err)
	}

	return nil
}

func (a *Analysis) checkOrStmt(scp scope.Scope, stmt ast.OrStmt) error {
	if _, err := a.Table.Find(stmt.ErrName, scp); err != nil {
		err := a.Table.RegisterLocal(stmt.ErrName, scp, types.TypeError)
		if err != nil {
			return semErrorf(stmt.Line, stmt.Col, "symbol error: %s", err)
		}
	}
	return nil
}

func (a *Analysis) checkEnumDecl(scp scope.Scope, stmt ast.Enum) error {
	typeId, err := a.Table.RegisterType(types.NewTypeEnum(stmt.TypeName))
	if err != nil {
		return semErrorf(stmt.Line, stmt.Col, "type error: %s", err)
	}

	for _, name := range stmt.Values {
		if err := a.Table.RegisterGlobal(name, typeId); err != nil {
			return semErrorf(stmt.Line, stmt.Col, "symbol error: %s", err)
		}
	}
	return nil
}

func (a *Analysis) checkInterfaceDecl(scp scope.Scope, decl ast.InterfaceDecl) error {
	methods := make([]types.Method, 0, len(decl.Contract))

	for _, m := range decl.Contract {
		args := make([]types.ID, 0, len(m.Params))
		for _, arg := range m.Params {
			id, err := NodeToTypeID(a.Table, scp, arg.TypeSpec)
			if err != nil {
				return semErrorf(arg.Line, arg.Col, "type error: %s", err)
			}
			args = append(args, id)
		}

		retType, err := NodeToTypeID(a.Table, scp, m.ReturnType)
		if err != nil {
			return semErrorf(m.Line, m.Col, "type error: %s", err)
		}

		methods = append(methods, types.Method{
			Name:       m.Name,
			Args:       args,
			ReturnType: retType,
		})

	}

	ifaceType, err := a.Table.RegisterType(types.NewInterfaceType(decl.Name, methods))
	if err != nil {
		return semErrorf(decl.Line, decl.Col, "type error: %s", err)
	}

	for _, met := range methods {
		methodType, err := a.Table.RegisterType(types.NewMethod(met.ReturnType, met.Args, ifaceType, met.Variadic))
		if err != nil {
			return semErrorf(decl.Line, decl.Col, "type error: %s", err)
		}

		err = a.Table.RegisterGlobal(symbols.FormatMethod(ifaceType, met.Name), methodType)
		if err != nil {
			return semErrorf(decl.Line, decl.Col, "symbol error: %s", err)
		}
	}
	return nil
}

func semErrorf(line int, col int, format string, elems ...interface{}) error {
	return fmt.Errorf("semantic error [%d:%d]: %s", line, col, fmt.Sprintf(format, elems...))
}
