package semantics

import (
	"errors"
	"fmt"

	"gitlab.com/egosummiki/litequel/ast"
	"gitlab.com/egosummiki/litequel/symbols/scope"
	"gitlab.com/egosummiki/litequel/types"
)

func DeduceType(table Table, scp scope.Scope, expr ast.Expression) (types.ID, error) {
	var typeStack []types.ID

	if expr.Boolean {
		return types.TypeBool, nil
	}

	for _, element := range expr.Elements {
		switch element.(type) {
		case ast.FunctionCall:
			if len(typeStack) == 0 {
				return types.TypeUnknown, nil
			}

			details, err := table.GetTypeDetails(typeStack[len(typeStack)-1])
			if err != nil {
				return types.TypeUnknown, err
			}

			if details.TypeVariant() != types.VariantFunction {
				continue // Filtration keeps the same type
			}

			funcDetails := details.(types.Function)
			typeStack[len(typeStack)-1] = funcDetails.ReturnType
		case ast.IndexOperator:
			index := element.(ast.IndexOperator)
			indexType, err := DeduceType(table, scp, index.Index)
			if err != nil {
				return types.TypeUnknown, err
			}
			if indexType == types.TypeRange {
				continue // Same as the list
			}

			if len(typeStack) == 0 {
				return types.TypeUnknown, nil
			}

			details, err := table.GetTypeDetails(typeStack[len(typeStack)-1])
			if err != nil {
				return types.TypeUnknown, err
			}

			collection, ok := details.(types.Collection)
			if !ok {
				return types.TypeUnknown, nil
			}

			typeStack[len(typeStack)-1] = collection.ElementType()
		case ast.ArrayLiteral:
			arr := element.(ast.ArrayLiteral)
			if len(arr.Elements) > 0 {
				first := arr.Elements[0]
				subType, err := DeduceType(table, scp, first)
				if err != nil {
					return types.TypeUnknown, err
				}
				if subType != types.TypeUnknown {
					listType, err := table.RegisterType(types.ListOf(subType))
					if err != nil {
						return types.TypeUnknown, err
					}
					typeStack = append(typeStack, listType)
					continue
				}
			}
			typeStack = append(typeStack, types.TypeArray)
		case ast.Literal:
			literal := element.(ast.Literal)
			switch literal.Kind {
			case ast.LiteralFloat:
				typeStack = append(typeStack, types.TypeFloat32)
			case ast.LiteralInt:
				typeStack = append(typeStack, types.TypeInt32)
			case ast.LiteralString:
				typeStack = append(typeStack, types.TypeString)
			default:
				return types.TypeUnknown, fmt.Errorf("invalid literal type")
			}
		case ast.Operator:
			operator := element.(ast.Operator)
			if operator.Kind == ".." {
				typeStack = typeStack[:len(typeStack)-2]
				typeStack = append(typeStack, types.TypeRange)
				continue
			}
			// Assume the result is same as the first type
			typeStack = typeStack[:len(typeStack)-1]
		case ast.Identifier:
			identifier := element.(ast.Identifier)
			symbol, err := table.Find(identifier.Name, scp)
			if err != nil {
				return types.TypeUnknown, err
			}

			typeStack = append(typeStack, symbol.Type)
		case ast.Constructor:
			typeID, err := NodeToTypeID(table, scp, element.(ast.Constructor).TypeSpec)
			if err != nil {
				return types.TypeUnknown, err
			}
			if typeID == types.TypeUnknown {
				return types.TypeUnknown, nil
			}
			typeStack = append(typeStack, typeID)
		case ast.FormatString:
			typeStack = append(typeStack, types.TypeString)
		case ast.ObjectProperty:
			if len(typeStack) == 0 {
				return types.TypeUnknown, nil
			}

			symbol, err := table.FindProperty(typeStack[len(typeStack)-1], element.(ast.ObjectProperty).Name)
			if err != nil {
				return types.TypeUnknown, err
			}

			typeStack[len(typeStack)-1] = symbol.Type
		case ast.MapConstruct:
			mapConst := element.(ast.MapConstruct)
			if len(mapConst.Values) == 0 {
				typeStack = append(typeStack, types.TypeMap)
				continue
			}

			t, err := DeduceType(table, scp, mapConst.Values[0].Value)
			if err != nil {
				return types.TypeUnknown, err
			}
			mapType, err := table.RegisterType(types.MapOf(types.TypeString, t))
			if err != nil {
				return types.TypeUnknown, err
			}
			typeStack = append(typeStack, mapType)
		case ast.OrStmt:
			continue
		case ast.Conversion:
			conv := element.(ast.Conversion)
			targetType, err := NodeToTypeID(table, scp, conv.TargetType)
			if err != nil {
				return types.TypeUnknown, err
			}
			typeStack = append(typeStack, targetType)
		case ast.TemplateApplication:
			temApp := element.(ast.TemplateApplication)
			templ, err := table.FindTemplate(temApp.Identifier.Name)
			if err != nil {
				return types.TypeUnknown, fmt.Errorf("templete error: %w", err)
			}
			switch templ.(type) {
			case ast.FunctionDefinition:
				funcTempl := templ.(ast.FunctionDefinition)
				funcType, err := CheckFunctionTemplateType(table, temApp.Args, scp, funcTempl)
				if err != nil {
					return types.TypeUnknown, err
				}

				typeStack = append(typeStack, funcType)
			default:
				return types.TypeUnknown, fmt.Errorf("invalid template type")
			}
		default:
			return types.TypeUnknown, nil
		}
	}

	if typeStack == nil || len(typeStack) != 1 {
		return types.TypeUnknown, nil
	}

	return typeStack[0], nil
}

func NodeToTypeID(symbols Table, scp scope.Scope, node ast.Type) (types.ID, error) {
	if len(node.Name) == 0 {
		return types.TypeUnknown, nil
	}
	if node.Variant == ast.MapType {
		if len(node.TemplateArgs) != 2 {
			return types.TypeUnknown, errors.New("specify key and value types of a map")
		}

		keyTypeArg, ok := node.TemplateArgs[0].(ast.Type)
		if !ok {
			return types.TypeUnknown, errors.New("map key template argument must be a type")
		}
		valueTypeArg, ok := node.TemplateArgs[1].(ast.Type)
		if !ok {
			return types.TypeUnknown, errors.New("map value template argument must be a type")
		}

		keyType, err := NodeToTypeID(symbols, scp, keyTypeArg)
		if err != nil {
			return types.TypeUnknown, err
		}
		valueType, err := NodeToTypeID(symbols, scp, valueTypeArg)
		if err != nil {
			return types.TypeUnknown, err
		}

		id, err := symbols.RegisterType(types.MapOf(keyType, valueType))
		if err != nil {
			return types.TypeUnknown, fmt.Errorf("type error: %w", err)
		}
		return id, nil
	}
	if node.Variant == ast.ListType {
		if len(node.TemplateArgs) != 1 {
			return types.TypeUnknown, errors.New( "specify element type of a list")
		}

		elemTypeArg, ok := node.TemplateArgs[0].(ast.Type)
		if !ok {
			return types.TypeUnknown, errors.New("map value template argument must be a type")
		}

		elemType, err := NodeToTypeID(symbols, scp, elemTypeArg)
		if err != nil {
			return types.TypeUnknown, err
		}

		id, err := symbols.RegisterType(types.ListOf(elemType))
		if err != nil {
			return types.TypeUnknown, fmt.Errorf("type error: %w", err)
		}
		return id, nil
	}
	if node.Variant == ast.FuncType {
		params := make([]types.ID, len(node.Function.Params))
		for i, p := range node.Function.Params {
			var err error
			params[i], err = NodeToTypeID(symbols, scp, p.TypeSpec)
			if err != nil {
				return types.TypeUnknown, err
			}
		}

		retType, err := NodeToTypeID(symbols, scp, node.Function.ReturnType)
		if err != nil {
			return types.TypeUnknown, err
		}

		id, err := symbols.RegisterType(types.NewFunction(retType, params, false))
		if err != nil {
			return types.TypeUnknown, err
		}

		return id, nil
	}
	if len(node.TemplateArgs) != 0 {
		template, err := symbols.FindTemplate(node.Name)
		if err != nil {
			return types.TypeUnknown, fmt.Errorf("template error: %w", err)
		}

		switch template.(type) {
		case ast.StructDefinition:
			id, err := ApplyStructTemplate(symbols, scp, node.TemplateArgs, template.(ast.StructDefinition))
			if err != nil {
				return types.TypeUnknown, fmt.Errorf("template error: %w", err)
			}
			return id, nil
		}

		return types.TypeUnknown, errors.New("invalid template")
	}

	typeID, err := symbols.GetTypeID(node.Name)
	if err != nil {
		return types.TypeUnknown, fmt.Errorf("type error: %w", err)
	}
	return typeID, nil
}
