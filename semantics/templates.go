package semantics

import (
	"fmt"

	"gitlab.com/egosummiki/litequel/ast"
	"gitlab.com/egosummiki/litequel/symbols"
	"gitlab.com/egosummiki/litequel/symbols/scope"
	"gitlab.com/egosummiki/litequel/types"
)

func findTypeInTemplateArgs(t *ast.Type, args []ast.TemplateParam) int {
	for i, arg := range args {
		if arg.Kind != ast.TemplateParamType {
			continue
		}
		if t.Name == arg.Value {
			return i
		}
	}
	return -1
}

func SubstituteType(params []ast.TemplateParam, args []ast.TypeOrLiteral, t *ast.Type) {
	tArg := findTypeInTemplateArgs(t, params)
	if tArg != -1 {
		*t = args[tArg].(ast.Type)
		return
	}
	for i, subtype := range t.TemplateArgs {
		switch subtype.(type) {
		case ast.Type:
			st := subtype.(ast.Type)
			SubstituteType(params, args, &st)
			t.TemplateArgs[i] = st
		}
	}
}

func ApplyStructTemplate(table Table, scp scope.Scope, args []ast.TypeOrLiteral, tem ast.StructDefinition) (types.ID, error) {
	if len(args) != len(tem.TemplateParams) {
		return types.TypeUnknown, fmt.Errorf("invalid number of arguments for template %s expected %d got %d", tem.Name, len(tem.TemplateParams), len(args))
	}

	properties := make([]types.TypeField, len(tem.Properties))
	for i, prop := range tem.Properties {
		SubstituteType(tem.TemplateParams, args, &prop.PropType)

		typeID, err := NodeToTypeID(table, scp, prop.PropType)
		if err != nil {
			return types.TypeUnknown, err
		}

		var annots []types.Annotation
		if len(prop.Annotations) != 0 {
			annots = make([]types.Annotation, len(prop.Annotations))
			for j, annot := range prop.Annotations {
				annots[j] = types.Annotation(annot)
			}
		}

		properties[i] = types.TypeField{
			TypeID: typeID,
			Annots: annots,
			Name:   prop.Name,
		}
	}

	structType := types.NewTypeStruct(symbols.FormatTemplateName(tem.Name, args), properties)
	typeId := table.FindType(structType)
	if typeId != types.TypeUnknown {
		return typeId, nil
	}

	typeId, err := table.RegisterType(structType)
	if err != nil {
		return types.TypeUnknown, fmt.Errorf("type error: %w", err)
	}

	a := Analysis{
		Table: table,
	}
	for _, method := range tem.MethodTemplates {
		tmpMethodName := method.Name
		applied, err := ApplyFunctionDecl(args, method)
		if err != nil {
			return types.TypeUnknown, err
		}
		applied.Name = tmpMethodName
		if err := a.CheckFunctionDeclStmt(scp, applied, typeId); err != nil {
			return types.TypeUnknown, err
		}
	}

	return typeId, nil
}

func ApplyFunctionType(args []ast.TypeOrLiteral, funcDecl *ast.FunctionDefinition) {
	SubstituteType(funcDecl.TemplateParams, args, &funcDecl.Return)

	funcParams := make([]ast.FuncDeclParam, len(funcDecl.Params))
	for i, arg := range funcDecl.Params {
		SubstituteType(funcDecl.TemplateParams, args, &arg.TypeSpec)
		funcParams[i] = arg
	}
	funcDecl.Params = funcParams

	funcDecl.Name = symbols.FormatTemplateName(funcDecl.Name, args)
}

func CheckFunctionTemplateType(table Table, args []ast.TypeOrLiteral, scp scope.Scope, decl ast.FunctionDefinition) (types.ID, error) {
	ApplyFunctionType(args, &decl)

	var receiverType types.ID
	var localScope scope.Scope

	if len(decl.Receiver.Name) != 0 {
		var err error
		receiverType, err = NodeToTypeID(table, scp, decl.Receiver)
		if err != nil {
			return types.TypeUnknown, err
		}

		localScope = scp.Sub(symbols.FormatMethod(receiverType, decl.Name))

		err = table.RegisterLocal(thisArgument, localScope, receiverType)
		if err != nil {
			return types.TypeUnknown, semErrorf(decl.Receiver.Line, decl.Receiver.Col, "type error: %s", err)
		}
		err = table.RegisterLocal(funcArgument, localScope, types.TypeUnknown)
		if err != nil {
			return types.TypeUnknown, semErrorf(decl.Receiver.Line, decl.Receiver.Col, "type error: %s", err)
		}
	} else {
		localScope = scp.Sub(decl.Name)
	}

	argTypes := make([]types.ID, len(decl.Params))
	for i, arg := range decl.Params {
		typeID, err := NodeToTypeID(table, localScope, arg.TypeSpec)
		if err != nil {
			return types.TypeUnknown, semErrorf(arg.TypeSpec.Line, arg.TypeSpec.Col, "type error: %s", err)
		}
		argTypes[i] = typeID
	}

	returnType, err := NodeToTypeID(table, scp, decl.Return)
	if err != nil {
		return types.TypeUnknown, semErrorf(decl.Return.Line, decl.Return.Col, "type error: %s", err)
	}

	if receiverType != types.TypeUnknown {
		methodType, err := table.RegisterMethod(receiverType, types.Method{
			Name:       decl.Name,
			Args:       argTypes,
			ReturnType: returnType,
			Variadic:   decl.Variadic,
		})
		if err != nil {
			return types.TypeUnknown, semErrorf(decl.Return.Line, decl.Return.Col, "symbol error: %s", err)
		}
		return methodType, nil
	}

	funcType, err := table.RegisterType(types.NewFunction(returnType, argTypes, decl.Variadic))
	if err != nil {
		return types.TypeUnknown, semErrorf(decl.Line, decl.Col, "type error: %s", err)
	}

	return funcType, nil
}

func ApplyFunctionDecl(args []ast.TypeOrLiteral, funcDecl ast.FunctionDefinition) (ast.FunctionDefinition, error) {
	ApplyFunctionType(args, &funcDecl)

	var err error
	funcDecl.Body, err = ApplyTemplate(funcDecl.TemplateParams, args, funcDecl.Body)
	if err != nil {
		return ast.FunctionDefinition{}, err
	}

	funcDecl.TemplateParams = nil

	return funcDecl, nil
}

func ApplyTemplate(params []ast.TemplateParam, args []ast.TypeOrLiteral, astNodes []interface{}) ([]interface{}, error) {
	result := make([]interface{}, 0, len(astNodes))
	for _, node := range astNodes {
		switch node.(type) {
		case ast.VariableDecl:
			varDecl := node.(ast.VariableDecl)
			SubstituteType(params, args, &varDecl.TypeSpec)
			result = append(result, node)
		case ast.Constructor:
			constr := node.(ast.Constructor)
			SubstituteType(params, args, &constr.TypeSpec)
			result = append(result, node)
		case ast.AssignStmt:
			assign := node.(ast.AssignStmt)
			applied, err := ApplyTemplate(params, args, assign.Value.Elements)
			if err != nil {
				return nil, err
			}
			assign.Value.Elements = applied
			result = append(result, assign)
		case ast.IfStmt:
			ifStmt := node.(ast.IfStmt)
			appliedThen, err := ApplyTemplate(params, args, ifStmt.Then)
			if err != nil {
				return nil, err
			}
			appliedElse, err := ApplyTemplate(params, args, ifStmt.Else)
			if err != nil {
				return nil, err
			}
			ifStmt.Then = appliedThen
			ifStmt.Else = appliedElse
			result = append(result, ifStmt)
		case ast.WhileStmt:
			whileStmt := node.(ast.WhileStmt)
			applied, err := ApplyTemplate(params, args, whileStmt.Body)
			if err != nil {
				return nil, err
			}
			whileStmt.Body = applied
			result = append(result, whileStmt)
		default:
			result = append(result, node)
		}
	}
	return result, nil
}
