package semantics

import (
	"testing"

	"gitlab.com/egosummiki/litequel/ast"
	"gitlab.com/egosummiki/litequel/symbols/scope"
	"gitlab.com/egosummiki/litequel/symbols/store"
	table2 "gitlab.com/egosummiki/litequel/symbols/table"
	"gitlab.com/egosummiki/litequel/types"
)

func TestDeduceType(t *testing.T) {
	table := table2.New(store.New(), scope.NewSymbolCounter())

	deduced, err := DeduceType(table, scope.Base, ast.Expression{
		Elements: []interface{}{
			ast.Literal{
				Kind:  ast.LiteralString,
				Value: "Hello",
			},
		},
	})
	if err != nil {
		t.Fatal(err)
	}
	if deduced != types.TypeString {
		t.Fatal("expected string")
	}

}
