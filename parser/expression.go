package parser

import (
	"fmt"

	"gitlab.com/egosummiki/litequel/ast"
	"gitlab.com/egosummiki/litequel/lexer"
	"gitlab.com/egosummiki/litequel/model"
	"gitlab.com/egosummiki/litequel/symbols"
	"gitlab.com/egosummiki/litequel/symbols/scope"
	"gitlab.com/egosummiki/litequel/tokens"
)

type instructionKind byte

const (
	noInstruction instructionKind = iota
	instructionPush
	instructionOperator
	instructionLeftBracket // Helper inst for parsing
	instructionCall
)

type instruction struct {
	Kind  instructionKind
	Token *tokens.Token
}

type expressionParser struct {
	stack  []instruction
	output []interface{}
	reader tokens.TokenReader
	stopAt []tokens.TokenType
	scope  scope.Scope
}

func feedExpressionForceBool(reader tokens.TokenReader, scope scope.Scope, forceBool bool, stopAt ...tokens.TokenType) (ast.Expression, tokens.TokenType, error) {
	parser := &expressionParser{
		stack:  make([]instruction, 0, 10),
		output: make([]interface{}, 0, 10),
		reader: reader,
		stopAt: stopAt,
		scope:  scope,
	}
	result := ast.Expression{}
	var err error
	var at tokens.TokenType

	// Read one token to test line, col
	token, ok := reader.Next()
	if !ok {
		return result, tokens.NoToken, endOfScriptError
	}
	result.Line = token.Line
	result.Col = token.Column
	reader.SeekBack()

	result.Elements, result.Boolean, at, err = parser.parse(forceBool)
	if err != nil {
		return result, tokens.NoToken, err
	}
	return result, at, nil
}

func feedExpression(reader tokens.TokenReader, scope scope.Scope, stopAt ...tokens.TokenType) (ast.Expression, tokens.TokenType, error) {
	return feedExpressionForceBool(reader, scope, false, stopAt...)
}

func feedBooleanExpression(reader tokens.TokenReader, scope scope.Scope, stopAt ...tokens.TokenType) (ast.Expression, tokens.TokenType, error) {
	return feedExpressionForceBool(reader, scope, true, stopAt...)
}

func (c *expressionParser) parse(forceBool bool) ([]interface{}, bool, tokens.TokenType, error) {
	var terminatedWith tokens.TokenType
	var token *tokens.Token
	var ok bool

	lastOperator := true
	parenLevel := 0
	hasBooleans := forceBool
	notMap := map[int]bool{}

parseLoop:
	for {
		token, ok = c.reader.Next()
		if !ok {
			return nil, false, tokens.NoToken, endOfScriptError
		}

		for _, kind := range c.stopAt {
			if token.Kind == kind && (kind != ')' && kind != '{' || parenLevel == 0) {
				terminatedWith = kind
				break parseLoop
			}
		}

		if token.IsType() {
			c.reader.SeekBack()

			parser := Parser{
				reader: c.reader,
				scope:  c.scope,
			}
			typeNode, err := parser.feedType()
			if err != nil {
				return nil, false, tokens.NoToken, err
			}

			token, ok := c.reader.Next()
			if !ok {
				return nil, false, tokens.NoToken, endOfScriptError
			}
			switch token.Kind {
			case '(':
				result, err := c.handleConversion(token.Line, token.Column, typeNode)
				if err != nil {
					return nil, false, tokens.NoToken, err
				}
				c.push(result)
				lastOperator = false
			case '.', '{':
				switch typeNode.Variant {
				case ast.MapType:
					result, err := c.handleMap(token.Line, token.Column, typeNode)
					if err != nil {
						return nil, false, tokens.NoToken, err
					}
					c.push(result)
					lastOperator = false
					continue
				case ast.ListType:
					result, err := c.handleArray(token.Line, token.Column)
					if err != nil {
						return nil, false, tokens.NoToken, err
					}
					c.push(result)
					lastOperator = false
					continue
				}
				c.reader.SeekBack()
				result, err := c.handleConstructor(token.Line, token.Column, typeNode)
				if err != nil {
					return nil, false, tokens.NoToken, err
				}
				c.push(result)
				lastOperator = false
			default:
				c.reader.SeekBack()
				c.push(typeNode)
				lastOperator = false
			}
			continue
		}

		if token.IsIdentifierOrLiteral() {
			if !lastOperator {
				c.reader.SeekBack()
				function, err := c.handleFunction(token.Line, token.Column, false)
				if err != nil {
					return nil, false, tokens.NoToken, err
				}
				c.push(function)
			} else if token.Kind == tokens.TokenStringDQ {
				fStr, err := c.handleFormatString(token.Line, token.Column, token)
				if err != nil {
					return nil, false, tokens.NoToken, err
				}
				c.push(fStr)
			} else {
				c.pushInstruction(instruction{Kind: instructionPush, Token: token})
			}
			lastOperator = false
			continue
		}

		if !token.IsOperator() {
			return nil, false, tokens.NoToken, parserError(token, fmt.Sprintf("unexpected token %s '%s' expected an operand or an operator", token.Kind.String(), token.Content))
		}

		switch token.Kind {
		case tokens.TokenLambda:
			lambda, err := c.feedLambda(token.Line, token.Column)
			if err != nil {
				return nil, false, tokens.NoToken, endOfScriptError
			}

			c.push(lambda)
			lastOperator = false
		case tokens.Join(':', ':'):
			c.reader.SeekBack()
			c.reader.SeekBack()
			token, ok := c.reader.Next()
			if !ok {
				return nil, false, tokens.NoToken, endOfScriptError // this shouldn't really happened
			}
			if !token.IsIdentifierOrLiteral() {
				return nil, false, tokens.NoToken, parserError(token, "moduleName must be a lower case literal")
			}
			namespaceName := token.Content
			_, _ = c.reader.Next() // ::
			token, ok = c.reader.Next()
			if !ok {
				return nil, false, tokens.NoToken, endOfScriptError
			}
			switch {
			case token.IsIdentifierOrLiteral():
				c.pop()
				token.Content = symbols.FormatNamespace(namespaceName, token.Content)
				c.pushInstruction(instruction{Kind: instructionPush, Token: token})
			default:
				return nil, false, tokens.NoToken, parserError(token, "expected an identifier")
			}
		case '.':
			if lastOperator {
				return nil, false, tokens.NoToken, parserError(token, fmt.Sprintf("unexpected '.'"))
			}
			token, ok = c.reader.Next()
			if !ok {
				return nil, false, tokens.NoToken, endOfScriptError
			}
			if token.Kind == tokens.TokenTypePtr {
				c.push(ast.Reference{
					Line: token.Line,
					Col:  token.Column,
				})
				continue
			}
			err := expectKind(token, tokens.TokenIdentifier)
			if err != nil {
				return nil, false, tokens.NoToken, err
			}
			// If there is a call on the stack we should pass it
			if len(c.stack) > 0 && c.stack[len(c.stack)-1].Kind == instructionCall {
				c.popStack()
			}
			c.push(ast.ObjectProperty{
				Name: token.Content,
				Line: token.Line,
				Col:  token.Column,
			})
			lastOperator = false
		case tokens.Join('&', '>'), tokens.Join('&', '<'):
			if lastOperator {
				return nil, false, tokens.NoToken, parserError(token, fmt.Sprintf("unexpected '<:' or ':>"))
			}

			line, col := token.Line, token.Column
			desc := token.Kind == tokens.Join('&', '<')

			token, ok := c.reader.Next()
			if !ok {
				return nil, false, tokens.NoToken, endOfScriptError
			}

			c.push(ast.SortStmt{
				Descending: desc,
				Field:      token.Content,
				Line:       line,
				Col:        col,
			})
			lastOperator = false
		case '!':
			hasBooleans = true
			notMap[parenLevel] = true
			lastOperator = true
		case tokens.Join('&', '&'):
			hasBooleans = true
			c.emptyStackScope()
			cond := true
			jump := true
			for k, v := range notMap {
				if k >= parenLevel && v {
					cond = !cond
					notMap[k] = false
				}
			}
			for i := parenLevel - 1; i >= 0; i-- {
				if notMap[i] {
					jump = !jump
				}
			}
			c.push(ast.BooleanOp{
				Level:       parenLevel + 1,
				JumpOnFalse: cond,
				JumpToElse:  jump,
			})
			lastOperator = true
		case tokens.Join('|', '|'):
			hasBooleans = true
			c.emptyStackScope()
			cond := false
			jump := false
			for k, v := range notMap {
				if k >= parenLevel && v {
					cond = !cond
					notMap[k] = false
				}
			}
			for i := parenLevel - 1; i >= 0; i-- {
				if notMap[i] {
					jump = !jump
				}
			}
			c.push(ast.BooleanOp{
				Level:       parenLevel,
				JumpOnFalse: cond,
				JumpToElse:  jump,
			})
			notMap[parenLevel] = false
			lastOperator = true
		case '[':
			if lastOperator {
				p := Parser{
					reader: c.reader,
					scope:  c.scope,
				}
				args, err := p.feedTemplateArguments()
				if err != nil {
					return nil, false, tokens.NoToken, err
				}

				token, ok := c.reader.Next()
				if !ok {
					return nil, false, tokens.NoToken, endOfScriptError
				}

				if err := expectKind(token, tokens.TokenIdentifier); err != nil {
					return nil, false, tokens.NoToken, err
				}

				c.push(ast.TemplateApplication{
					Args: args,
					Identifier: ast.Identifier{
						Name: token.Content,
						Line: token.Line,
						Col:  token.Column,
					},
					Line: token.Line,
					Col:  token.Column,
				})
			} else {
				expr, _, err := feedExpression(c.reader, c.scope, ']')
				if err != nil {
					return nil, false, tokens.NoToken, err
				}
				c.push(ast.IndexOperator{Index: expr, Line: token.Line, Col: token.Column})
			}
			lastOperator = false
		case ',':
			return nil, false, tokens.NoToken, parserError(token, "found ',' outside a function call")
		case '(':
			if !lastOperator {
				function, err := c.handleFunction(token.Line, token.Column, true)
				if err != nil {
					return nil, false, tokens.NoToken, err
				}
				c.push(function)
				lastOperator = false
			} else {
				c.pushStack(instructionLeftBracket, token)
				lastOperator = true
				parenLevel++
			}
		case ')':
			if len(c.stack) == 0 {
				return nil, false, tokens.NoToken, parserError(token, "Unparied right bracket.")
			}
			err := c.emptyStackUntilLeftBracket(token)
			if err != nil {
				return nil, false, tokens.NoToken, err
			}
			c.popStack()
			parenLevel--
			lastOperator = false
		default:
			level := model.OperatorOrder[token.Content]
			if level == 0 {
				return nil, false, tokens.NoToken, parserError(token, fmt.Sprintf("Unexpected symbol '%s'. Expected an operator.", token.Content))
			}
			if lastOperator {
				if token.Kind == '-' { // Just insert the zero on random - sign
					c.pushInstruction(instruction{Kind: instructionPush, Token: &tokens.Token{
						Kind:    tokens.TokenDecimal,
						Content: "0",
						Line:    token.Line,
						Column:  token.Column,
					}})
				} else {
					return nil, false, tokens.NoToken, parserError(token, "Unexpected operator after another operator.")
				}
			}
			for len(c.stack) > 0 && model.OperatorOrder[c.stack[len(c.stack)-1].Token.Content] <= level {
				c.pushInstruction(c.popStack())
			}
			c.pushStack(instructionOperator, token)
			lastOperator = true
		}
	}
	c.emptyStack()
	if len(c.output) == 0 { // Empty expression is still an expression
		return c.output, false, terminatedWith, nil
	}
	if lastOperator {
		return nil, false, tokens.NoToken, parserError(token, "Missing operand")
	}
	if hasBooleans {
		cond := true
		for _, v := range notMap {
			if v {
				cond = !cond
			}
		}
		c.push(ast.BooleanOp{
			Level:       parenLevel,
			JumpOnFalse: cond,
			JumpToElse:  true,
			Line:        token.Line,
			Col:         token.Column,
		})
	}
	return c.output, hasBooleans, terminatedWith, nil
}

func (c *expressionParser) handleOr(line, col int) (*ast.OrStmt, error) {
	r := ast.OrStmt{
		Line: line,
		Col:  col,
	}
	token, ok := c.reader.Next()
	if !ok {
		return nil, endOfScriptError
	}

	if err := expectKind(token, tokens.TokenIdentifier); err != nil {
		return nil, err
	}

	r.ErrName = token.Content
	return &r, nil
}

func (c *expressionParser) handleConversion(line, col int, typeNode ast.Type) (ast.Conversion, error) {
	var err error
	conversion := ast.Conversion{
		Line:       line,
		Col:        col,
		TargetType: typeNode,
	}
	conversion.Value, _, err = feedExpression(c.reader, c.scope, ')')
	if err != nil {
		return conversion, err
	}

	return conversion, nil
}

func (c *expressionParser) handleConstructor(line, col int, typeNode ast.Type) (ast.Constructor, error) {
	result := ast.Constructor{
		TypeSpec: typeNode,
		Line:     line,
		Col:      col,
	}

	token, ok := c.reader.Next()
	if !ok {
		return result, endOfScriptError
	}
	if token.Kind == '.' {
		c.reader.SeekBack()
		return result, nil
	}
	err := expectKind(token, '{')
	if err != nil {
		return result, err
	}
	for {
		token, ok = c.reader.Next()
		if !ok {
			return result, endOfScriptError
		}
		if token.Kind == '}' {
			break
		}
		err := expectKind(token, tokens.TokenIdentifier)
		if err != nil {
			return result, err
		}
		arg := token.Content
		token, ok = c.reader.Next()
		if !ok {
			return result, endOfScriptError
		}
		err = expectKind(token, ':')
		if err != nil {
			return result, err
		}
		expr, at, err := feedExpression(c.reader, c.scope, ',', '}')
		result.Values = append(result.Values,
			struct {
				Arg   string
				Value ast.Expression
			}{
				Arg:   arg,
				Value: expr,
			})
		if at == '}' {
			break
		}
	}

	return result, nil
}

func (c *expressionParser) handleArray(line, col int) (ast.ArrayLiteral, error) {
	result := ast.ArrayLiteral{
		Line: line,
		Col:  col,
	}
	for {
		expr, at, err := feedExpression(c.reader, c.scope, ',', '}')
		if err != nil {
			return result, err
		}
		if len(expr.Elements) > 0 {
			result.Elements = append(result.Elements, expr)
		}
		if at == '}' {
			return result, nil
		}
	}
}

func (c *expressionParser) handleFunction(line, col int, braced bool) (ast.FunctionCall, error) {
	result := ast.FunctionCall{
		Line: line,
		Col:  col,
	}

	for {
		expr, end, err := feedExpression(c.reader, c.scope, ',', ')', ';')
		if err != nil {
			return result, err
		}
		if len(expr.Elements) > 0 {
			result.Arguments = append(result.Arguments, expr)
		}
		switch end {
		case ',':
			continue
		case ')':
			if !braced {
				c.reader.SeekBack()
				return result, nil
			}
			token, ok := c.reader.Next()
			if !ok {
				return result, endOfScriptError
			}
			if token.Kind != '?' {
				c.reader.SeekBack()
				return result, nil
			}
			result.Or, err = c.handleOr(token.Line, token.Column)
			if err != nil {
				return result, err
			}
			return result, nil
		case ';':
			if braced {
				return result, parserErrorLineCol(line, col, "unexpected ; ending function call, expected )")
			}
			c.reader.SeekBack()
			return result, nil
		}
	}
}

func (c *expressionParser) handleFormatString(line, col int, stringToken *tokens.Token) (ast.FormatString, error) {
	result := ast.FormatString{
		Line: line,
		Col:  col,
	}
	var buff []byte
	runeStr := []rune(stringToken.Content)
	scope := 0
	for _, ch := range runeStr {
		switch ch {
		case '{':
			scope++
			if scope == 1 {
				continue
			}
		case '}':
			scope--
			if scope == 0 {
				tns, err := lexer.ReadTokens(buff)
				if err != nil {
					return result, err
				}
				expr, _, err := feedExpression(tns, c.scope, ';')
				if err != nil {
					return result, err
				}
				result.Format = append(result.Format, struct {
					Position int
					Value    ast.Expression
				}{Position: len(result.Content), Value: expr})
				buff = []byte{}
				continue
			}
		}
		if scope > 0 {
			buff = append(buff, byte(ch))
		} else {
			result.Content = append(result.Content, ch)
		}
	}
	return result, nil
}

func (c *expressionParser) feedLambda(line, col int) (ast.Lambda, error) {
	result := ast.Lambda{
		Line: line,
		Col:  col,
	}

	parser := Parser{reader: c.reader, scope: c.scope}

	token, ok := c.reader.Next()
	if !ok {
		return result, endOfScriptError
	}

	if token.Kind == '(' {
		var err error
		result.Params, result.Variadic, err = parser.feedFuncParams()
		if err != nil {
			return ast.Lambda{}, err
		}

		token, ok = c.reader.Next()
		if !ok {
			return result, endOfScriptError
		}
	}

	if token.Kind == tokens.Join('-', '>') {
		var err error
		result.Return, err = parser.feedType()
		if err != nil {
			return result, err
		}
		token, ok = c.reader.Next()
		if !ok {
			return result, endOfScriptError
		}
	}

	switch token.Kind {
	case '{':
		var err error
		result.Body, err = parser.readBlock()
		if err != nil {
			return result, err
		}
		token, ok = c.reader.Next()
		if !ok {
			return result, endOfScriptError
		}
		err = expectKind(token, ';')
		if err != nil {
			return result, err
		}
	case '=':
		expr, _, err := feedExpression(c.reader, c.scope, ';')
		if err != nil {
			return result, err
		}
		result.Body = []interface{}{
			ast.ReturnStmt{
				Line:  token.Line,
				Col:   token.Column,
				Value: expr,
			},
		}
	default:
		return result, unexpected(token, '{', '=')
	}

	return result, nil
}

func (c *expressionParser) handleMap(line int, column int, typeNode ast.Type) (ast.MapConstruct, error) {
	mapConst := ast.MapConstruct{
		TypeSpec: typeNode,
		Values: []struct {
			Key   ast.Expression
			Value ast.Expression
		}{},
		Line: line,
		Col:  column,
	}

mapLoop:
	for {
		// empty map, or finished after ,
		lookAHead, ok := c.reader.Next()
		if !ok {
			return mapConst, endOfScriptError
		}
		if lookAHead.Kind == '}' {
			return mapConst, nil
		} else {
			c.reader.SeekBack()
		}

		key, _, err := feedExpression(c.reader, c.scope, ':')
		if err != nil {
			return mapConst, err
		}

		value, stop, err := feedExpression(c.reader, c.scope, ',', '}')
		if err != nil {
			return mapConst, err
		}

		mapConst.Values = append(mapConst.Values, struct {
			Key   ast.Expression
			Value ast.Expression
		}{Key: key, Value: value})

		switch stop {
		case ',':
			continue
		case '}':
			break mapLoop
		}
	}

	return mapConst, nil
}

func (c *expressionParser) pushStack(kind instructionKind, token *tokens.Token) {
	c.stack = append(c.stack, instruction{Kind: kind, Token: token})
}

func (c *expressionParser) popStack() instruction {
	element := c.stack[len(c.stack)-1]
	c.stack = c.stack[:len(c.stack)-1]
	return element
}

func tokenKindToLiteralKind(kind tokens.TokenType) ast.LiteralKind {
	switch kind {
	case tokens.TokenFloat:
		return ast.LiteralFloat
	case tokens.TokenDecimal:
		return ast.LiteralInt
	case tokens.TokenString:
		return ast.LiteralString
	}
	return ast.LiteralNil
}

func (c *expressionParser) pushInstruction(element instruction) {
	switch element.Kind {
	case instructionPush:
		if element.Token.Kind == tokens.TokenIdentifier {
			c.push(ast.Identifier{
				Name: element.Token.Content,
				Line: element.Token.Line,
				Col:  element.Token.Column,
			})
		} else {
			c.push(ast.Literal{
				Kind:  tokenKindToLiteralKind(element.Token.Kind),
				Value: element.Token.Content,
				Line:  element.Token.Line,
				Col:   element.Token.Column,
			})
		}
	case instructionOperator:
		c.push(ast.Operator{
			Kind: element.Token.Content,
			Line: element.Token.Line,
			Col:  element.Token.Column,
		})
	}
}

func (c *expressionParser) push(element interface{}) {
	c.output = append(c.output, element)
}

func (c *expressionParser) pop() {
	if len(c.output) == 0 {
		return
	}
	c.output = c.output[:len(c.output)-1]
}

func (c *expressionParser) emptyStack() {
	for len(c.stack) > 0 {
		c.pushInstruction(c.popStack())
	}
}

func (c *expressionParser) emptyStackUntilLeftBracket(token *tokens.Token) error {
	for c.stack[len(c.stack)-1].Kind != instructionLeftBracket {
		c.pushInstruction(c.popStack())
		if len(c.stack) == 0 {
			return parserError(token, "unpaired right bracket")
		}
	}
	return nil
}

func (c *expressionParser) emptyStackScope() {
	for len(c.stack) > 0 && c.stack[len(c.stack)-1].Kind != instructionLeftBracket {
		c.pushInstruction(c.popStack())
	}
}

func (c *expressionParser) emptyCalls() {
	for len(c.stack) > 0 && c.stack[len(c.stack)-1].Kind == instructionCall {
		c.pushInstruction(c.popStack())
	}
}
