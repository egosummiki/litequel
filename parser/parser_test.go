package parser

import (
	"testing"
)

func TestParser_ScopeUp(t *testing.T) {
	type fields struct {
		scope string
	}
	tests := []struct {
		name   string
		fields fields
		wants  string
	}{
		{
			name:   "ScopeUp#1",
			fields: fields{scope: "foo.bar.goo"},
			wants:  "foo.bar",
		},
		{
			name:   "ScopeUp#2",
			fields: fields{scope: "foo"},
			wants:  "",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			p := &Parser{
				scope: tt.fields.scope,
			}
			p.ScopeUp()
			if p.scope != tt.wants {
				t.Errorf("Expected %s, got %s", tt.wants, p.scope)
			}
		})
	}
}
