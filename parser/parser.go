package parser

import (
	"gitlab.com/egosummiki/litequel/ast"
	"gitlab.com/egosummiki/litequel/runtime"
	"gitlab.com/egosummiki/litequel/symbols/scope"
	"gitlab.com/egosummiki/litequel/tokens"
)

type Parser struct {
	reader     tokens.TokenReader
	scope      scope.Scope
	modules    []runtime.Module
	moduleName string
}

func NewParser() *Parser {
	return &Parser{}
}

func (p *Parser) Modules() []runtime.Module {
	return p.modules
}

func (p *Parser) ScopeUp() {
	p.scope = p.scope.Parent()
}

func (p *Parser) ScopeDown(name string) {
	p.scope = p.scope.Sub(name)
}

func (p *Parser) ModuleName() string {
	return p.moduleName
}

func (p *Parser) Parse(reader tokens.TokenReader) ([]interface{}, error) {
	p.scope = ""
	p.reader = reader

	err := p.checkNameSpace()
	if err != nil {
		return nil, err
	}

	var instructionSet []interface{}
	for p.reader.HasTokens() {
		inst, err := p.feedStatement()
		if err != nil {
			return nil, err
		}
		instructionSet = append(instructionSet, inst)
	}
	return instructionSet, nil
}

func (p *Parser) checkNameSpace() error {
	token, ok := p.reader.Next()
	if !ok {
		return endOfScriptError
	}

	err := expectKind(token, tokens.TokenModule)
	if err != nil {
		p.reader.SeekBack()
		p.moduleName = "main"
		return nil
	}

	token, ok = p.reader.Next()
	if !ok {
		return endOfScriptError
	}
	err = expectKind(token, tokens.TokenIdentifier)
	if err != nil {
		return err
	}
	p.moduleName = token.Content

	token, ok = p.reader.Next()
	if !ok {
		return endOfScriptError
	}
	return expectKind(token, ';')
}

func (p *Parser) readBlock() ([]interface{}, error) {
	var instructionSet []interface{}
	for {
		token, ok := p.reader.Next()
		if !ok {
			return nil, endOfScriptError
		}
		if token.Kind == '}' {
			return instructionSet, nil
		}
		p.reader.SeekBack()
		inst, err := p.feedStatement()
		if err != nil {
			return nil, err
		}
		instructionSet = append(instructionSet, inst)
	}
}

func (p *Parser) feedStatement() (interface{}, error) {
	token, ok := p.reader.Next()
	if !ok {
		return nil, endOfScriptError
	}
	switch token.Kind {
	case tokens.TokenLet:
		return p.feedVariableDecl(token.Line, token.Column)
	case tokens.TokenConst:
		return p.feedConstDecl(token.Line, token.Column)
	case tokens.TokenIf:
		return p.feedIfStatement(token.Line, token.Column)
	case tokens.TokenFor:
		return p.feedForStatement(token.Line, token.Column)
	case tokens.TokenDef:
		return p.feedFuncDecl(token.Line, token.Column)
	case tokens.TokenReturn:
		return p.feedReturnStatement(token.Line, token.Column)
	case tokens.TokenBreak:
		return p.feedBreak(token.Line, token.Column)
	case tokens.TokenContinue:
		return p.feedContinue(token.Line, token.Column)
	case tokens.TokenWhile:
		return p.feedWhileStatement(token.Line, token.Column)
	case tokens.TokenPub:
		return p.feedPublic(token.Line, token.Column)
	case tokens.TokenFail:
		return p.feedFail(token.Line, token.Column)
	case tokens.TokenEnum:
		return p.feedEnumDecl(token.Line, token.Column)
	case tokens.TokenInterface:
		return p.feedInterfaceDecl(token.Line, token.Column)
	case tokens.TokenUse:
		err := p.feedInject(token.Line, token.Column)
		if err != nil {
			return nil, err
		}
		return nil, nil
	case tokens.TokenStruct:
		return p.feedStructDecl(token.Line, token.Column)
	default:
		p.reader.SeekBack()

		expr, at, err := feedExpression(p.reader, p.scope, ';', '=', '+'*128+'=', '-'*128+'=', '*'*128+'=', '/'*128+'=', '+'*128+'+', '-'*128+'-', '<'*128+'-')
		if err != nil {
			return nil, err
		}
		if at == ';' {
			expr.Line = token.Line
			expr.Col = token.Column
			return expr, nil
		}

		assignStmt, err := p.feedAssignment(token.Line, token.Column, expr, at)
		if err != nil {
			return nil, err
		}
		return assignStmt, nil
	}
}

func (p *Parser) feedPublic(line, col int) (interface{}, error) {
	stmt, err := p.feedStatement()
	if err != nil {
		return nil, err
	}
	switch stmt.(type) {
	case ast.FunctionDefinition:
		return stmt, nil
	case ast.ConstDecl:
		return stmt, nil
	case ast.StructDefinition:
		return stmt, nil
	default:
		return nil, parserErrorLineCol(line, col, "statement cannot be public")
	}
}

var assignOperation = map[tokens.TokenType]ast.AssigmentKind{
	'=':           ast.AssignEq,
	'+'*128 + '=': ast.AssignPlus,
	'-'*128 + '=': ast.AssignMinus,
	'*'*128 + '=': ast.AssignMult,
	'/'*128 + '=': ast.AssignDiv,
	'<'*128 + '-': ast.AssignAppend,
	'+'*128 + '+': ast.AssignPlusPlus,
	'-'*128 + '-': ast.AssignMinusMinus,
}

func (p *Parser) feedAssignment(line, col int, assignTo ast.Expression, kind tokens.TokenType) (ast.AssignStmt, error) {
	result := ast.AssignStmt{
		Line: line,
		Col:  col,
		Kind: assignOperation[kind],
	}
	var err error

	if len(assignTo.Elements) == 0 {
		return result, parserErrorLineCol(line, col, "nothing to assign to")
	}
	result.Target = assignTo

	if result.Kind == ast.AssignPlusPlus || result.Kind == ast.AssignMinusMinus {
		token, ok := p.reader.Next()
		if !ok {
			return result, endOfScriptError
		}
		err = expectKind(token, ';')
		if err != nil {
			return result, err
		}

		return result, nil
	}

	result.Value, _, err = feedExpression(p.reader, p.scope, ';')
	if err != nil {
		return ast.AssignStmt{}, err
	}

	return result, nil
}

func (p *Parser) feedTemplateArguments() ([]ast.TypeOrLiteral, error) {
	var result []ast.TypeOrLiteral
	for {
		token, ok := p.reader.Next()
		if !ok {
			return nil, endOfScriptError
		}

		if token.IsLiteral() {
			result = append(result, ast.Literal{
				Kind:  tokenKindToLiteralKind(token.Kind),
				Value: token.Content,
				Line:  token.Line,
				Col:   token.Column,
			})
		} else {
			p.reader.SeekBack()
			sub, err := p.feedType()
			if err != nil {
				return nil, err
			}
			result = append(result, sub)
		}

		token, ok = p.reader.Next()
		if !ok {
			return nil, endOfScriptError
		}

		if token.Kind == ',' {
			continue
		}

		err := expectKind(token, ']')
		if err != nil {
			return nil, err
		}
		break
	}
	return result, nil
}

func (p *Parser) feedFuncType() (ast.FunctionType, error) {
	var result ast.FunctionType

	token, ok := p.reader.Next()
	if !ok {
		return result, endOfScriptError
	}
	if err := expectKind(token, '('); err != nil {
		return ast.FunctionType{}, nil
	}

	result.Line = token.Line
	result.Col = token.Column

	for {
		token, ok := p.reader.Next()
		if !ok {
			return result, endOfScriptError
		}
		if token.Kind == ')' {
			break
		}

		methodArg := ast.FunctionParam{
			Line: token.Line,
			Col:  token.Column,
		}

		if token.Kind == tokens.TokenIdentifier {
			methodArg.Name = token.Content
			token, ok = p.reader.Next()
			if !ok {
				return result, endOfScriptError
			}
			if err := expectKind(token, ':'); err != nil {
				return result, err
			}
		} else {
			p.reader.SeekBack()
		}

		var err error
		methodArg.TypeSpec, err = p.feedType()
		if err != nil {
			return ast.FunctionType{}, err
		}

		result.Params = append(result.Params, methodArg)

		token, ok = p.reader.Next()
		if !ok {
			return result, endOfScriptError
		}
		if token.Kind == ')' {
			break
		}
		err = expectKind(token, ',')
		if err != nil {
			return result, err
		}
	}

	token, ok = p.reader.Next()
	if !ok {
		return result, endOfScriptError
	}

	if token.Kind == tokens.Join('-', '>') {
		var err error
		result.ReturnType, err = p.feedType()
		if err != nil {
			return ast.FunctionType{}, err
		}
	} else {
		p.reader.SeekBack()
	}

	return result, nil
}

func (p *Parser) feedType() (ast.Type, error) {
	var result ast.Type

	token, ok := p.reader.Next()
	if !ok {
		return ast.Type{}, endOfScriptError
	}
	if !token.IsType() {
		return ast.Type{}, parserError(token, "expected type")
	}

	result.Name = token.Content
	result.Line = token.Line
	result.Col = token.Column

	switch token.Kind {
	case tokens.TokenTypeMap:
		result.Variant = ast.MapType
	case tokens.TokenTypeVector:
		result.Variant = ast.ListType
	case tokens.TokenTypeFunc:
		result.Variant = ast.FuncType
		funcType, err := p.feedFuncType()
		if err != nil {
			return ast.Type{}, err
		}
		result.Function = &funcType
		return result, nil
	default:
		result.Variant = ast.OrdinaryType
	}

	token, ok = p.reader.Next()
	if !ok {
		return ast.Type{}, endOfScriptError
	}
	switch token.Kind {
	case '[':
		var err error
		result.TemplateArgs, err = p.feedTemplateArguments()
		if err != nil {
			return ast.Type{}, err
		}
	default:
		p.reader.SeekBack()
	}

	return result, nil
}

func (p *Parser) feedFuncParams() ([]ast.FuncDeclParam, bool, error) {
	var result []ast.FuncDeclParam
	variadic := false

argLoop:
	for {
		token, ok := p.reader.Next()
		if !ok {
			return nil, false, endOfScriptError
		}
		switch token.Kind {
		case '.'*128*128 + '.'*128 + '.':
			// Variadic
			variadic = true
			tokenName, ok := p.reader.Next()
			if !ok {
				return nil, false, endOfScriptError
			}
			err := expectKind(token, tokens.TokenIdentifier)
			if err != nil {
				return nil, false, err
			}

			var variadicArgType ast.Type

			token, ok = p.reader.Next()
			if !ok {
				return nil, false, endOfScriptError
			}
			if token.Kind == ':' {
				variadicArgType, err = p.feedType()
				if err != nil {
					return nil, false, err
				}
			} else {
				variadicArgType = ast.Type{
					Line:    tokenName.Line,
					Col:     tokenName.Column,
					Variant: ast.ListType,
					TemplateArgs: []ast.TypeOrLiteral{
						ast.Type{
							Line: tokenName.Line,
							Col:  tokenName.Column,
						},
					},
				}
				p.reader.SeekBack()
			}

			result = append(result, ast.FuncDeclParam{
				Name:     tokenName.Content,
				TypeSpec: variadicArgType,
			})
			token, ok = p.reader.Next()
			if !ok {
				return nil, false, endOfScriptError
			}
			err = expectKind(token, ')')
			if err != nil {
				return nil, false, err
			}
			fallthrough
		case ')':
			break argLoop
		}
		err := expectKind(token, tokens.TokenIdentifier)
		if err != nil {
			return nil, false, err
		}

		arg := token.Content
		var symbolType ast.Type

		token, ok = p.reader.Next()
		if !ok {
			return nil, false, endOfScriptError
		}

		if token.Kind == ':' {
			symbolType, err = p.feedType()
			if err != nil {
				return nil, false, err
			}
			token, ok = p.reader.Next()
			if !ok {
				return nil, false, endOfScriptError
			}
		}

		result = append(result, ast.FuncDeclParam{
			Name:     arg,
			TypeSpec: symbolType,
		})

		if token.Kind == ')' {
			break
		}
		err = expectKind(token, ',')
		if err != nil {
			return nil, false, err
		}
	}

	return result, variadic, nil
}

func (p *Parser) feedFuncDecl(line, col int) (ast.FunctionDefinition, error) {
	result := ast.FunctionDefinition{
		Line:     line,
		Col:      col,
		Variadic: false,
	}

	token, ok := p.reader.Next()
	if !ok {
		return result, endOfScriptError
	}

	if token.Kind == '[' {
		var err error
		result.TemplateParams, err = p.feedTemplateParams()
		if err != nil {
			return ast.FunctionDefinition{}, err
		}

		token, ok = p.reader.Next()
		if !ok {
			return result, endOfScriptError
		}

		if token.Kind == ';' {
			token, ok = p.reader.Next()
			if !ok {
				return result, endOfScriptError
			}
		}
	}

	if token.Kind == tokens.TokenTypeLiteral {
		p.reader.SeekBack()
		var err error
		result.Receiver, err = p.feedType()
		if err != nil {
			return result, err
		}
		token, ok = p.reader.Next()
		if !ok {
			return result, endOfScriptError
		}
		err = expectKind(token, '.')
		if err != nil {
			return result, err
		}
		token, ok = p.reader.Next()
		if !ok {
			return result, endOfScriptError
		}
	}

	err := expectKind(token, tokens.TokenIdentifier)
	if err != nil {
		return result, err
	}
	result.Name = token.Content

	token, ok = p.reader.Next()
	if !ok {
		return result, endOfScriptError
	}
	err = expectKind(token, '(')
	if err != nil {
		return result, err
	}

	result.Params, result.Variadic, err = p.feedFuncParams()
	if err != nil {
		return result, err
	}

	token, ok = p.reader.Next()
	if !ok {
		return result, endOfScriptError
	}

	if token.Kind == tokens.Join('-', '>') {
		result.Return, err = p.feedType()
		if err != nil {
			return result, err
		}
		token, ok = p.reader.Next()
		if !ok {
			return result, endOfScriptError
		}
	}

	switch token.Kind {
	case '{':
		result.Body, err = p.readBlock()
		if err != nil {
			return result, err
		}
		token, ok = p.reader.Next()
		if !ok {
			return result, endOfScriptError
		}
		err = expectKind(token, ';')
		if err != nil {
			return result, err
		}
	case '=':
		expr, _, err := feedExpression(p.reader, p.scope, ';')
		if err != nil {
			return result, err
		}
		result.Body = []interface{}{
			ast.ReturnStmt{
				Line:  token.Line,
				Col:   token.Column,
				Value: expr,
			},
		}
	default:
		return result, unexpected(token, '{', '=')
	}

	return result, nil
}

func (p *Parser) feedConstDecl(line, col int) (ast.ConstDecl, error) {
	result := ast.ConstDecl{
		Line: line,
		Col:  col,
	}

	token, ok := p.reader.Next()
	if !ok {
		return result, endOfScriptError
	}
	err := expectKind(token, tokens.TokenIdentifier)
	if err != nil {
		return result, err
	}
	result.Name = token.Content

	token, ok = p.reader.Next()
	if !ok {
		return result, endOfScriptError
	}
	switch token.Kind {
	case ':':
		result.TypeSpec, err = p.feedType()
		if err != nil {
			return result, err
		}
		token, ok := p.reader.Next()
		if !ok {
			return result, endOfScriptError
		}
		err := expectKind(token, ';')
		if err != nil {
			return result, err
		}
	case '=':
		result.Value, _, err = feedExpression(p.reader, p.scope, ';')
		if err != nil {
			return result, err
		}
	default:
		return result, parserError(token, "unexpected token, expected ':' or '='.")
	}

	return result, nil
}

func (p *Parser) feedVariableDecl(line, col int) (ast.VariableDecl, error) {
	result := ast.VariableDecl{
		Line: line,
		Col:  col,
	}

	// let ? = 1
	token, ok := p.reader.Next()
	if !ok {
		return result, endOfScriptError
	}
	err := expectKind(token, tokens.TokenIdentifier)
	if err != nil {
		return result, err
	}
	result.Name = token.Content

	// let name ?
	token, ok = p.reader.Next()
	if !ok {
		return result, endOfScriptError
	}

	if token.Kind == ':' {
		result.TypeSpec, err = p.feedType()
		if err != nil {
			return result, err
		}
		token, ok = p.reader.Next()
		if !ok {
			return result, endOfScriptError
		}
	}

	switch token.Kind {
	case ';':
		result.Value = ast.Expression{
			Boolean: false,
			Line:    result.Line,
			Col:     result.Col,
		}
	case '=':
		result.Value, _, err = feedExpression(p.reader, p.scope, ';')
		if err != nil {
			return result, err
		}
	default:
		return ast.VariableDecl{}, unexpected(token, '=', ';')
	}
	return result, nil
}

func (p *Parser) feedReturnStatement(line, col int) (ast.ReturnStmt, error) {
	result := ast.ReturnStmt{
		Line: line,
		Col:  col,
	}
	var err error
	result.Value, _, err = feedExpression(p.reader, p.scope, ';')
	if err != nil {
		return result, err
	}
	return result, nil
}

func (p *Parser) feedBlockOrStatement(line, col int, endToken tokens.TokenType) ([]interface{}, error) {
	switch endToken {
	case '{':
		block, err := p.readBlock()
		if err != nil {
			return nil, err
		}

		token, ok := p.reader.Next()
		if !ok {
			return nil, endOfScriptError
		}

		if token.Kind != ';' {
			p.reader.SeekBack()
		}

		return block, nil
	case ':':
		stmt, err := p.feedStatement()
		if err != nil {
			return nil, err
		}
		return []interface{}{stmt}, nil
	}
	p.reader.Position()

	return nil, parserErrorLineCol(line, col, "unexpected token")
}

func (p *Parser) feedIfStatement(line, col int) (ast.IfStmt, error) {
	result := ast.IfStmt{
		Line: line,
		Col:  col,
	}
	var err error
	var endToken tokens.TokenType

	result.Condition, endToken, err = feedBooleanExpression(p.reader, p.scope, '{', ':')
	if err != nil {
		return ast.IfStmt{}, err
	}

	result.Then, err = p.feedBlockOrStatement(line, col, endToken)
	if err != nil {
		return ast.IfStmt{}, err
	}

	token, ok := p.reader.Next()
	if !ok {
		return result, nil
	}

	if token.Kind == tokens.TokenElse {
		token, ok := p.reader.Next()
		if !ok {
			return ast.IfStmt{}, endOfScriptError
		}
		if token.Kind == tokens.TokenIf {
			elseIf, err := p.feedIfStatement(token.Line, token.Column)
			if err != nil {
				return ast.IfStmt{}, err
			}
			result.Else = []interface{}{elseIf}
		} else {
			result.Else, err = p.feedBlockOrStatement(token.Line, token.Column, token.Kind)
			if err != nil {
				return ast.IfStmt{}, err
			}
		}
	} else {
		p.reader.SeekBack()
	}

	return result, nil
}

func (p *Parser) feedWhileStatement(line, col int) (ast.WhileStmt, error) {
	result := ast.WhileStmt{
		Line: line,
		Col:  col,
	}
	var err error

	result.Condition, _, err = feedBooleanExpression(p.reader, p.scope, '{')
	if err != nil {
		return result, err
	}

	result.Body, err = p.readBlock()
	if err != nil {
		return result, err
	}

	token, ok := p.reader.Next()
	if !ok {
		return result, endOfScriptError
	}

	err = expectKind(token, ';')
	if err != nil {
		return result, err
	}

	return result, nil
}

func (p *Parser) feedForStatement(line, col int) (ast.ForStmt, error) {
	result := ast.ForStmt{
		Line: line,
		Col:  col,
	}
	var err error

	// for ? in 0..10 {
	token, ok := p.reader.Next()
	if !ok {
		return ast.ForStmt{}, endOfScriptError
	}
	err = expectKind(token, tokens.TokenIdentifier)
	if err != nil {
		return ast.ForStmt{}, err
	}
	result.What = token.Content

	// There should be an in
	token, ok = p.reader.Next()
	if !ok {
		return ast.ForStmt{}, endOfScriptError
	}
	err = expectKind(token, tokens.TokenIn)
	if err != nil {
		return ast.ForStmt{}, err
	}

	// for a in ? {
	result.Over, _, err = feedExpression(p.reader, p.scope, '{')
	if err != nil {
		return ast.ForStmt{}, err
	}

	// for a in 0..10 {
	// ?
	// }
	result.Body, err = p.readBlock()
	if err != nil {
		return ast.ForStmt{}, err
	}

	// We should have a ; here
	token, ok = p.reader.Next()
	if !ok {
		return ast.ForStmt{}, endOfScriptError
	}
	err = expectKind(token, ';')
	if err != nil {
		return ast.ForStmt{}, err
	}

	return result, nil
}

func (p *Parser) feedBreak(line, col int) (ast.BreakStmt, error) {
	result := ast.BreakStmt{
		Line: line,
		Col:  col,
	}
	token, ok := p.reader.Next()
	if !ok {
		return result, endOfScriptError
	}
	err := expectKind(token, ';')
	if err != nil {
		return result, err
	}
	return result, nil
}

func (p *Parser) feedFail(line, col int) (ast.FailStmt, error) {
	result := ast.FailStmt{
		Line: line,
		Col:  col,
	}
	var err error

	result.Message, _, err = feedExpression(p.reader, p.scope, ';')
	if err != nil {
		return result, err
	}

	return result, nil
}

func (p *Parser) feedContinue(line, col int) (ast.ContinueStmt, error) {
	result := ast.ContinueStmt{
		Line: line,
		Col:  col,
	}
	token, ok := p.reader.Next()
	if !ok {
		return result, endOfScriptError
	}
	err := expectKind(token, ';')
	if err != nil {
		return result, err
	}
	return result, nil
}

func (p *Parser) feedAnnotation() ([]ast.Annotation, error) {
	var r []ast.Annotation
annotLoop:
	for {
		a := ast.Annotation{}

		token, ok := p.reader.Next()
		if !ok {
			return nil, endOfScriptError
		}
		err := expectKind(token, tokens.TokenIdentifier)
		if err != nil {
			return nil, err
		}

		a.Key = token.Content

		token, ok = p.reader.Next()
		if !ok {
			return nil, endOfScriptError
		}
		switch token.Kind {
		case ',':
			a.Value = ""
			r = append(r, a)
			continue
		case ']':
			a.Value = ""
			r = append(r, a)
			break annotLoop
		case ':':
		default:
			return nil, parserError(token, "unexpected token")
		}

		token, ok = p.reader.Next()
		if !ok {
			return nil, endOfScriptError
		}
		err = expectKind(token, tokens.TokenString)
		if err != nil {
			return nil, err
		}

		a.Value = token.Content
		r = append(r, a)

		token, ok = p.reader.Next()
		if !ok {
			return nil, endOfScriptError
		}
		switch token.Kind {
		case ',':
		case ']':
			break annotLoop
		default:
			return nil, parserError(token, "unexpected token")
		}
	}
	token, ok := p.reader.Next()
	if !ok {
		return nil, endOfScriptError
	}
	if token.Kind != ';' { // optional semicolon
		p.reader.SeekBack()
	}
	return r, nil
}

func (p *Parser) feedTemplateParams() ([]ast.TemplateParam, error) {
	var result []ast.TemplateParam
	for {
		token, ok := p.reader.Next()
		if !ok {
			return result, endOfScriptError
		}
		if token.Kind == ']' {
			break
		}

		switch token.Kind {
		case tokens.TokenIdentifier:
			result = append(result, ast.TemplateParam{
				Value: token.Content,
				Kind:  ast.TemplateParamValue,
			})
		case tokens.TokenTypeLiteral:
			result = append(result, ast.TemplateParam{
				Value: token.Content,
				Kind:  ast.TemplateParamType,
			})
		default:
			return nil, parserError(token, "unexpected token")
		}

		token, ok = p.reader.Next()
		if !ok {
			return nil, endOfScriptError
		}

		if token.Kind == ']' {
			break
		}

		err := expectKind(token, ',')
		if err != nil {
			return nil, err
		}
	}

	return result, nil
}

func (p *Parser) feedStructDecl(line, col int) (ast.StructDefinition, error) {
	result := ast.StructDefinition{
		Line: line,
		Col:  col,
	}

	token, ok := p.reader.Next()
	if !ok {
		return result, endOfScriptError
	}
	err := expectKind(token, tokens.TokenTypeLiteral)
	if err != nil {
		return result, err
	}
	result.Name = token.Content

	token, ok = p.reader.Next()
	if !ok {
		return result, endOfScriptError
	}

	if token.Kind == '[' {
		result.TemplateParams, err = p.feedTemplateParams()
		if err != nil {
			return ast.StructDefinition{}, err
		}

		token, ok = p.reader.Next()
		if !ok {
			return result, endOfScriptError
		}
	}

	err = expectKind(token, '{')
	if err != nil {
		return result, err
	}

fieldLoop:
	for {
		token, ok = p.reader.Next()
		if !ok {
			return result, endOfScriptError
		}
		var annotations []ast.Annotation
		switch token.Kind {
		case '}':
			break fieldLoop
		case '[':
			annotations, err = p.feedAnnotation()
			if err != nil {
				return result, err
			}
			token, ok = p.reader.Next()
			if !ok {
				return result, endOfScriptError
			}
		}
		err = expectKind(token, tokens.TokenIdentifier)
		if err != nil {
			return result, err
		}

		arg := token.Content

		token, ok = p.reader.Next()
		if !ok {
			return result, endOfScriptError
		}
		err = expectKind(token, ':')
		if err != nil {
			return result, err
		}

		propType, err := p.feedType()
		if err != nil {
			return result, err
		}

		result.Properties = append(result.Properties, ast.StructProperty{
			Name:        arg,
			PropType:    propType,
			Annotations: annotations,
		})

		token, ok = p.reader.Next()
		if !ok {
			return result, endOfScriptError
		}
		if token.Kind == '}' {
			break
		}
		err = expectKind(token, ';')
		if err != nil {
			return result, err
		}
	}

	token, ok = p.reader.Next()
	if !ok {
		return result, endOfScriptError
	}
	err = expectKind(token, ';')
	if err != nil {
		return result, err
	}

	return result, nil
}

func (p *Parser) feedInject(line, col int) error {
	expr, _, err := feedExpression(p.reader, p.scope, ';')
	if err != nil {
		return err
	}

	if len(expr.Elements) == 0 {
		return parserErrorLineCol(line, col, "empty inject statement")
	}

	return nil
}

func (p *Parser) feedEnumDecl(line, col int) (ast.Enum, error) {
	result := ast.Enum{
		Line: line,
		Col:  col,
	}

	token, ok := p.reader.Next()
	if !ok {
		return result, endOfScriptError
	}

	err := expectKind(token, tokens.TokenTypeLiteral)
	if err != nil {
		return result, err
	}

	result.TypeName = token.Content

	token, ok = p.reader.Next()
	if !ok {
		return result, endOfScriptError
	}

	err = expectKind(token, '{')
	if err != nil {
		return result, err
	}

	for {
		token, ok = p.reader.Next()
		if !ok {
			return result, endOfScriptError
		}

		if token.Kind == '}' {
			break
		}

		err = expectKind(token, tokens.TokenIdentifier)
		if err != nil {
			return result, err
		}

		result.Values = append(result.Values, token.Content)

		token, ok = p.reader.Next()
		if !ok {
			return result, endOfScriptError
		}

		switch token.Kind {
		case tokens.TokenIdentifier:
			p.reader.SeekBack()
		case ',', ';':
		case '}':
			break
		default:
			return result, parserError(token, "unexpected token")
		}
	}

	return result, nil
}

func (p *Parser) feedInterfaceDecl(line, col int) (ast.InterfaceDecl, error) {
	result := ast.InterfaceDecl{
		Line: line,
		Col:  col,
	}

	token, ok := p.reader.Next()
	if !ok {
		return result, endOfScriptError
	}

	err := expectKind(token, tokens.TokenTypeLiteral)
	if err != nil {
		return result, err
	}

	result.Name = token.Content

	token, ok = p.reader.Next()
	if !ok {
		return result, endOfScriptError
	}

	err = expectKind(token, '{')
	if err != nil {
		return result, err
	}

	for {
		token, ok = p.reader.Next()
		if !ok {
			return result, endOfScriptError
		}

		if token.Kind == '}' {
			break
		}

		err = expectKind(token, tokens.TokenIdentifier)
		if err != nil {
			return result, err
		}

		method := ast.FunctionDecl{
			Name: token.Content,
		}

		token, ok = p.reader.Next()
		if !ok {
			return result, endOfScriptError
		}
		if err = expectKind(token, '('); err != nil {
			return result, err
		}

		method.FunctionType, err = p.feedFuncType()
		if err != nil {
			return ast.InterfaceDecl{}, err
		}

		token, ok = p.reader.Next()
		if !ok {
			return result, endOfScriptError
		}
		if err = expectKind(token, ';'); err != nil {
			return result, err
		}

		result.Contract = append(result.Contract, method)
	}

	token, ok = p.reader.Next()
	if !ok {
		return result, endOfScriptError
	}
	err = expectKind(token, ';')
	if err != nil {
		return result, err
	}

	return result, nil
}
