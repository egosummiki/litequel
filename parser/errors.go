package parser

import (
	"fmt"

	"gitlab.com/egosummiki/litequel/tokens"
)

func parserError(token *tokens.Token, message string) error {
	return parserErrorLineCol(token.Line, token.Column, message)
}

func parserErrorLineCol(line, col int, message string) error {
	return fmt.Errorf("parser error [%d:%d]: %s", line, col, message)
}

var endOfScriptError = fmt.Errorf("parser error: unexpected end of script")

func expectKind(token *tokens.Token, kind tokens.TokenType) error {
	if token.Kind == kind {
		return nil
	}
	return unexpected(token, kind)
}

func unexpected(token *tokens.Token, expected ...tokens.TokenType) error {
	errMsg := fmt.Sprintf("unexpected %s expected %s", token.String(), expected[0].String())
	if len(expected) > 1 {
		for _, tk := range expected[1 : len(expected)-1] {
			errMsg += ", " + tk.String()
		}
		errMsg += " or " + expected[len(expected)-1].String()
	}
	return parserError(token, errMsg)
}
