project litequel

binary lq 'main.lq'

module examples {
    'examples/qsort.lq'
}

module math {
    'math/math.lq'
    'math/sql.lq'
}
