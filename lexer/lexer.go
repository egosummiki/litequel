package lexer

import (
	"gitlab.com/egosummiki/litequel/tokens"
)

const (
	symbolNoCombine byte = 1
	symbolCombine   byte = 2
)

var symbols []byte = []byte{
	'{':  symbolNoCombine,
	'}':  symbolNoCombine,
	'(':  symbolNoCombine,
	')':  symbolNoCombine,
	'[':  symbolNoCombine,
	']':  symbolNoCombine,
	'<':  symbolCombine,
	'>':  symbolCombine,
	'!':  symbolCombine,
	'@':  symbolCombine,
	'#':  symbolCombine,
	'$':  symbolCombine,
	'%':  symbolCombine,
	'^':  symbolCombine,
	'&':  symbolCombine,
	'*':  symbolCombine,
	'~':  symbolCombine,
	'=':  symbolCombine,
	'?':  symbolCombine,
	'|':  symbolCombine,
	'+':  symbolCombine,
	'-':  symbolCombine,
	'.':  symbolCombine,
	',':  symbolNoCombine,
	':':  symbolCombine,
	';':  symbolCombine,
	'/':  symbolCombine,
	'\\': symbolCombine,
}

var keywords = map[string]tokens.TokenType{
	"struct":    tokens.TokenStruct,
	"let":       tokens.TokenLet,
	"if":        tokens.TokenIf,
	"else":      tokens.TokenElse,
	"break":     tokens.TokenBreak,
	"continue":  tokens.TokenContinue,
	"for":       tokens.TokenFor,
	"while":     tokens.TokenWhile,
	"def":       tokens.TokenDef,
	"const":     tokens.TokenConst,
	"return":    tokens.TokenReturn,
	"use":       tokens.TokenUse,
	"mod":       tokens.TokenModule,
	"pub":       tokens.TokenPub,
	"string":    tokens.TokenTypeString,
	"in":        tokens.TokenIn,
	"fail":      tokens.TokenFail,
	"or":        tokens.TokenOr,
	"enum":      tokens.TokenEnum,
	"interface": tokens.TokenInterface,
	"vec":       tokens.TokenTypeVector,
	"bool":      tokens.TokenTypeBool,
	"int8":      tokens.TokenTypeInt8,
	"int16":     tokens.TokenTypeInt16,
	"int32":     tokens.TokenTypeInt32,
	"int":       tokens.TokenTypeInt32,
	"int64":     tokens.TokenTypeInt64,
	"uint8":     tokens.TokenTypeUInt8,
	"uint16":    tokens.TokenTypeUInt16,
	"uint32":    tokens.TokenTypeUInt32,
	"uint64":    tokens.TokenTypeUInt64,
	"float32":   tokens.TokenTypeFloat32,
	"float":     tokens.TokenTypeFloat32,
	"float64":   tokens.TokenTypeFloat64,
	"func":      tokens.TokenTypeFunc,
	"error":     tokens.TokenTypeError,
	"map":       tokens.TokenTypeMap,
	"ptr":       tokens.TokenTypePtr,
	"lambda":    tokens.TokenLambda,
}

func ReadTokens(buffer []byte) (tokens.TokenReader, error) {
	reader := newByteBuffer(buffer)
	collector := tokens.NewTokenCollector()
	literal := ""
	for {
		c, ok := reader.readByte()
		if !ok {
			handleLiteral(collector, reader, &literal)
			handleAddSemicolon(collector, reader)
			return collector.GetReader(), nil
		}

		switch c {
		case ' ', '\t', '\r':
			handleLiteral(collector, reader, &literal)
			continue
		case '\n':
			handleLiteral(collector, reader, &literal)
			handleAddSemicolon(collector, reader)
			continue
		case '/':
			handleLiteral(collector, reader, &literal)
			err := handleComments(collector, reader)
			if err != nil {
				return nil, err
			}
			continue
		case '"':
			fallthrough
		case '\'':
			handleLiteral(collector, reader, &literal)
			err := handleString(collector, reader, c)
			if err != nil {
				return nil, err
			}
			continue
		}

		if int(c) < len(symbols) {
			switch symbols[c] {
			case symbolCombine:
				if c == '.' && analyzeLiteral(literal) == tokens.TokenDecimal { // Floating points
					fore, ok := reader.readByte()
					reader.seekBack()
					if ok && fore != '.' {
						break
					}
				}
				handleLiteral(collector, reader, &literal)
				handleCombinedSymbol(collector, reader, byte(c))
				continue
			case symbolNoCombine:
				handleLiteral(collector, reader, &literal)
				collector.AddToken(tokens.TokenType(c), string(c), reader.getLine(), reader.getCol())
				continue
			}
		}

		literal += string(c)
	}
}

func analyzeLiteral(content string) tokens.TokenType {
	if len(content) == 0 {
		return tokens.NoToken
	}
	if kw := keywords[content]; kw != tokens.NoToken {
		return kw
	}
	correct := true
	if len(content) > 2 {
		switch content[0:2] {
		case "0x":
			for _, c := range content[2:] {
				if c != '_' && (c < '0' || c > '9' && c < 'A' || c > 'F' && c < 'a' || c > 'f') {
					correct = false
					break
				}
			}
			if correct {
				return tokens.TokenHexadecimal
			}
		case "0b":
			for _, c := range content[2:] {
				if c != '_' && (c < '0' || c > '1') {
					correct = false
					break
				}
			}
			if correct {
				return tokens.TokenBinary
			}
		}
	}
	correct = true
	points := 0
	for _, c := range content {
		if c != '_' && (c < '0' || c > '9') {
			if c == '.' {
				points++
				continue
			}
			correct = false
			break
		}
	}
	if correct {
		if points == 0 {
			return tokens.TokenDecimal
		} else if points == 1 {
			return tokens.TokenFloat
		}
	}
	first := content[0]
	if first >= 'A' && first <= 'Z' {
		return tokens.TokenTypeLiteral
	}
	return tokens.TokenIdentifier
}

func handleComments(collector tokens.TokenCollector, reader scriptReader) error {
	c, ok := reader.readByte()
	if !ok {
		return reader.fatal("Unexpected / at the end of file.")
	}
	if c == '*' {
		for {
			c, ok = reader.readByte()
			if !ok {
				return nil
			}
			if c == '*' {
				c, ok = reader.readByte()
				if !ok {
					return nil
				}
				if c == '/' {
					break
				}
			}
		}
	} else if c == '/' {
		for {
			c, ok = reader.readByte()
			if !ok {
				return nil
			}
			if c == '\n' {
				reader.seekBack()
				break
			}
		}
	} else {
		reader.seekBack()
		handleCombinedSymbol(collector, reader, '/')
		return nil
	}
	return nil
}

func handleString(collector tokens.TokenCollector, reader scriptReader, strChar rune) error {
	str := ""
	for {
		c, ok := reader.readByte()
		if !ok {
			return reader.fatal("unclosed string")
		}
		if c == '\\' {
			c, ok = reader.readByte()
			if !ok {
				return reader.fatal("unclosed string")
			}
			switch c {
			case 'n':
				str += string(10)
			case 'r':
				str += string(13)
			default:
				str += string(c)
			}
		} else {
			if c == strChar {
				break
			}
			str += string(c)
		}
	}
	// We need to subtract 2 because of the quotes characters
	if strChar == '"' {
		collector.AddToken(tokens.TokenStringDQ, str, reader.getLine(), reader.getCol()-2)
	} else {
		collector.AddToken(tokens.TokenString, str, reader.getLine(), reader.getCol()-2)
	}
	return nil
}

func handleAddSemicolon(collector tokens.TokenCollector, reader scriptReader) {
	last := collector.Last()
	if last.IsIdentifierOrLiteral() || last.IsType() ||
		last.Kind == '+'*128+'+' ||
		last.Kind == '-'*128+'-' ||
		last.Kind == ')' ||
		last.Kind == '}' ||
		last.Kind == ']' ||
		last.Kind == '>' ||
		last.Kind == tokens.TokenReturn ||
		last.Kind == tokens.TokenBreak ||
		last.Kind == tokens.TokenContinue {
		reader.seekBack()
		collector.AddToken(tokens.TokenType(';'), ";", reader.getLine(), reader.getCol()+1)
		reader.seekForward()
	}
}

func handleCombinedSymbol(collector tokens.TokenCollector, reader scriptReader, c byte) {
	id := 0
	str := ""
	for symbols[c] == symbolCombine {
		id = id*128 + int(c)
		str += string(c)
		cr, ok := reader.readByte()
		if !ok || int(cr) > len(symbols) {
			break
		}
		c = byte(cr)
	}
	reader.seekBack()
	collector.AddToken(tokens.TokenType(id), str, reader.getLine(), reader.getCol())
}

func handleLiteral(collector tokens.TokenCollector, reader scriptReader, literal *string) {
	if len(*literal) > 0 {
		// literal handling is performed one byte after so column need to be decreased
		reader.seekBack()
		collector.AddToken(analyzeLiteral(*literal), *literal, reader.getLine(), reader.getCol())
		reader.seekForward()
		*literal = ""
	}
}
