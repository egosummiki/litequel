package lexer

import (
	"fmt"
	"io/ioutil"
)

type scriptBuffer struct {
	buffer        []rune
	line          int
	column        int
	position      int
	lastLineWidth int
}

type scriptReader interface {
	readByte() (rune, bool)
	seekBack()
	seekForward() bool
	fatal(msg string) error
	getLine() int
	getCol() int
}

func newScriptBuffer(filename string) (*scriptBuffer, error) {
	buffer, err := ioutil.ReadFile(filename)
	if err != nil {
		return nil, err
	}
	return newByteBuffer(buffer), nil
}

func newByteBuffer(buff []byte) *scriptBuffer {
	return &scriptBuffer{
		buffer:        []rune(string(buff)),
		line:          1,
		column:        1,
		position:      -1,
		lastLineWidth: 0,
	}
}

func (sb *scriptBuffer) readByte() (rune, bool) {
	if ok := sb.seekForward(); ok {
		return sb.buffer[sb.position], true
	}
	return 0, false
}

func (sb *scriptBuffer) seekBack() {
	sb.position--
	sb.column--
	if sb.column < 1 {
		sb.column += sb.lastLineWidth
		sb.line--
	}
}

// This should be used only after seekBack
func (sb *scriptBuffer) seekForward() bool {
	sb.position++
	if sb.position >= len(sb.buffer) {
		sb.column++
		return false
	}
	sb.column++
	if sb.buffer[sb.position] == '\n' {
		sb.line++
		sb.lastLineWidth = sb.column - 1
		sb.column = 1
	}
	return true
}

func (sb *scriptBuffer) getLine() int {
	return sb.line
}

func (sb *scriptBuffer) getCol() int {
	return sb.column
}

func (sb *scriptBuffer) fatal(msg string) error {
	return fmt.Errorf("Lexer Error at line %d col %d: %s", sb.line, sb.column, msg)
}
