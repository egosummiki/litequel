package lexer

import (
	"reflect"
	"testing"

	"gitlab.com/egosummiki/litequel/tokens"
)

func TestReadTokens(t *testing.T) {
	type args struct {
		buffer []byte
	}
	tests := []struct {
		name    string
		args    args
		want    []tokens.Token
		wantErr bool
	}{
		{
			name: "ReadTokens#1",
			args: args{
				buffer: []byte("let variable = 0x453F"),
			},
			want: []tokens.Token{
				{
					Kind:    tokens.TokenLet,
					Content: "let",
					Line:    1,
					Column:  1,
				},
				{
					Kind:    tokens.TokenIdentifier,
					Content: "variable",
					Line:    1,
					Column:  5,
				},
				{
					Kind:    tokens.TokenType('='),
					Content: "=",
					Line:    1,
					Column:  14,
				},
				{
					Kind:    tokens.TokenHexadecimal,
					Content: "0x453F",
					Line:    1,
					Column:  16,
				},
				{
					Kind:    tokens.TokenType(';'),
					Content: ";",
					Line:    1,
					Column:  22,
				},
			},
			wantErr: false,
		},
		{
			name: "ReadTokens#2",
			args: args{
				buffer: []byte("return something\ndoSomething()"),
			},
			want: []tokens.Token{
				{
					Kind:    tokens.TokenReturn,
					Content: "return",
					Line:    1,
					Column:  1,
				},
				{
					Kind:    tokens.TokenIdentifier,
					Content: "something",
					Line:    1,
					Column:  8,
				},
				{
					Kind:    tokens.TokenType(';'),
					Content: ";",
					Line:    1,
					Column:  17,
				},
				{
					Kind:    tokens.TokenIdentifier,
					Content: "doSomething",
					Line:    2,
					Column:  1,
				},
				{
					Kind:    tokens.TokenType('('),
					Content: "(",
					Line:    2,
					Column:  12,
				},
				{
					Kind:    tokens.TokenType(')'),
					Content: ")",
					Line:    2,
					Column:  13,
				},
				{
					Kind:    tokens.TokenType(';'),
					Content: ";",
					Line:    2,
					Column:  14,
				},
			},
			wantErr: false,
		},
		{
			name: "ReadTokens#3",
			args: args{
				buffer: []byte("def printHello() {\n\tprint('Hello World')\n}\n\nprintHello()"),
			},
			want: []tokens.Token{
				{
					Kind:    tokens.TokenDef,
					Content: "def",
					Line:    1,
					Column:  1,
				},
				{
					Kind:    tokens.TokenIdentifier,
					Content: "printHello",
					Line:    1,
					Column:  5,
				},
				{
					Kind:    tokens.TokenType('('),
					Content: "(",
					Line:    1,
					Column:  15,
				},
				{
					Kind:    tokens.TokenType(')'),
					Content: ")",
					Line:    1,
					Column:  16,
				},
				{
					Kind:    tokens.TokenType('{'),
					Content: "{",
					Line:    1,
					Column:  18,
				},
				{
					Kind:    tokens.TokenIdentifier,
					Content: "print",
					Line:    2,
					Column:  2,
				},
				{
					Kind:    tokens.TokenType('('),
					Content: "(",
					Line:    2,
					Column:  7,
				},
				{
					Kind:    tokens.TokenString,
					Content: "Hello World",
					Line:    2,
					Column:  8,
				},
				{
					Kind:    tokens.TokenType(')'),
					Content: ")",
					Line:    2,
					Column:  21,
				},
				{
					Kind:    tokens.TokenType(';'),
					Content: ";",
					Line:    2,
					Column:  22,
				},
				{
					Kind:    tokens.TokenType('}'),
					Content: "}",
					Line:    3,
					Column:  1,
				},
				{
					Kind:    tokens.TokenType(';'),
					Content: ";",
					Line:    3,
					Column:  2,
				},
				{
					Kind:    tokens.TokenIdentifier,
					Content: "printHello",
					Line:    5,
					Column:  1,
				},
				{
					Kind:    tokens.TokenType('('),
					Content: "(",
					Line:    5,
					Column:  11,
				},
				{
					Kind:    tokens.TokenType(')'),
					Content: ")",
					Line:    5,
					Column:  12,
				},
				{
					Kind:    tokens.TokenType(';'),
					Content: ";",
					Line:    5,
					Column:  13,
				},
			},
			wantErr: false,
		},
		{
			name: "ReadTokens#4",
			args: args{
				buffer: []byte("34.5677"),
			},
			want: []tokens.Token{
				{
					Kind:    tokens.TokenFloat,
					Content: "34.5677",
					Line:    1,
					Column:  1,
				},
				{
					Kind:    tokens.TokenType(';'),
					Content: ";",
					Line:    1,
					Column:  8,
				},
			},
			wantErr: false,
		},
		{
			name: "ReadTokens#5",
			args: args{
				buffer: []byte("let str = 'This is some string'\nlet floatingpoint = 23.678"),
			},
			want: []tokens.Token{
				{
					Kind:    tokens.TokenLet,
					Content: "let",
					Line:    1,
					Column:  1,
				},
				{
					Kind:    tokens.TokenIdentifier,
					Content: "str",
					Line:    1,
					Column:  5,
				},
				{
					Kind:    tokens.TokenType('='),
					Content: "=",
					Line:    1,
					Column:  9,
				},
				{
					Kind:    tokens.TokenString,
					Content: "This is some string",
					Line:    1,
					Column:  11,
				},
				{
					Kind:    tokens.TokenType(';'),
					Content: ";",
					Line:    1,
					Column:  32,
				},
				{
					Kind:    tokens.TokenLet,
					Content: "let",
					Line:    2,
					Column:  1,
				},
				{
					Kind:    tokens.TokenIdentifier,
					Content: "floatingpoint",
					Line:    2,
					Column:  5,
				},
				{
					Kind:    tokens.TokenType('='),
					Content: "=",
					Line:    2,
					Column:  19,
				},
				{
					Kind:    tokens.TokenFloat,
					Content: "23.678",
					Line:    2,
					Column:  21,
				},
				{
					Kind:    tokens.TokenType(';'),
					Content: ";",
					Line:    2,
					Column:  27,
				},
			},
			wantErr: false,
		},
		{
			name: "ReadTokens#6 Unicode yay",
			args: args{
				buffer: []byte("żółć 'ręką' 我爱你 по-русски"),
			},
			want: []tokens.Token{
				{
					Kind:    tokens.TokenIdentifier,
					Content: "żółć",
					Line:    1,
					Column:  1,
				},
				{
					Kind:    tokens.TokenString,
					Content: "ręką",
					Line:    1,
					Column:  6,
				},
				{
					Kind:    tokens.TokenIdentifier,
					Content: "我爱你",
					Line:    1,
					Column:  13,
				},
				{
					Kind:    tokens.TokenIdentifier,
					Content: "по",
					Line:    1,
					Column:  17,
				},
				{
					Kind:    tokens.TokenType('-'),
					Content: "-",
					Line:    1,
					Column:  19,
				},
				{
					Kind:    tokens.TokenIdentifier,
					Content: "русски",
					Line:    1,
					Column:  20,
				},
				{
					Kind:    tokens.TokenType(';'),
					Content: ";",
					Line:    1,
					Column:  26,
				},
			},
			wantErr: false,
		},
		{
			name: "ReadTokens#7 Ranges",
			args: args{
				buffer: []byte("0..10"),
			},
			want: []tokens.Token{
				{
					Kind:    tokens.TokenDecimal,
					Content: "0",
					Line:    1,
					Column:  1,
				},
				{
					Kind:    tokens.TokenType('.'*128 + '.'),
					Content: "..",
					Line:    1,
					Column:  2,
				},
				{
					Kind:    tokens.TokenDecimal,
					Content: "10",
					Line:    1,
					Column:  4,
				},
				{
					Kind:    tokens.TokenType(';'),
					Content: ";",
					Line:    1,
					Column:  6,
				},
			},
			wantErr: false,
		},
		{
			name: "ReadTokens#8 +=",
			args: args{
				buffer: []byte("a += 5"),
			},
			want: []tokens.Token{
				{
					Kind:    tokens.TokenIdentifier,
					Content: "a",
					Line:    1,
					Column:  1,
				},
				{
					Kind:    tokens.TokenType('+'*128 + '='),
					Content: "+=",
					Line:    1,
					Column:  3,
				},
				{
					Kind:    tokens.TokenDecimal,
					Content: "5",
					Line:    1,
					Column:  6,
				},
				{
					Kind:    tokens.TokenType(';'),
					Content: ";",
					Line:    1,
					Column:  7,
				},
			},
			wantErr: false,
		},
		{
			name: "ReadTokens#9 !=",
			args: args{
				buffer: []byte("a != b"),
			},
			want: []tokens.Token{
				{
					Kind:    tokens.TokenIdentifier,
					Content: "a",
					Line:    1,
					Column:  1,
				},
				{
					Kind:    tokens.TokenType('!'*128 + '='),
					Content: "!=",
					Line:    1,
					Column:  3,
				},
				{
					Kind:    tokens.TokenIdentifier,
					Content: "b",
					Line:    1,
					Column:  6,
				},
				{
					Kind:    tokens.TokenType(';'),
					Content: ";",
					Line:    1,
					Column:  7,
				},
			},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := ReadTokens(tt.args.buffer)
			if (err != nil) != tt.wantErr {
				t.Errorf("ReadTokens() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			gotTokens := tokens.ReadAll(got)
			if !reflect.DeepEqual(gotTokens, tt.want) {
				t.Errorf("ReadTokens() = %v, want %v", gotTokens, tt.want)
			}
		})
	}
}
