" Vim syntax file
" Language:		Litequel
" Maintainer:	Mikołaj Bednarek <mikolaj@bednarek.xyz>

if exists("b:current_syntax")
  finish
endif

let s:cpo_sav = &cpo
set cpo&vim

syn keyword lqCondition if else
syn keyword lqLoop for while
syn keyword lqStruct struct interface
syn keyword lqControlFlow break continue return
syn keyword lqOtherKeyword using pub
syn keyword lqDefinition let def const
syn keyword lqSpecial true false this func
syn keyword lqType int int8 int16 int32 int64 ptr byte
syn keyword lqType uint uint8 uint16 uint32 uint64
syn keyword lqType float float32 float64
syn keyword lqType bool string list map chain
syn keyword lqLeftArrow <- conceal cchar=←
syn keyword lqFail fail none

syn match  lqCustomType	"\<[A-Z][a-zA-Z0-9]\+" display

syn match lqOperator display "\%(+\|-\|/\|*\|=\|\^\|&\||\|!\|>\|<\|%\)=\?"
syn keyword lqIn in

syn region    lqString      start=+"+ skip=+\\\\\|\\"+ end=+"+
syn region    lqString      start=+'+ skip=+\\\\\|\\"+ end=+'+

syn match     lqDecNumber   display "\<[0-9][0-9_]*\%([iu]\%(size\|8\|16\|32\|64\|128\)\)\="
syn match     lqHexNumber   display "\<0x[a-fA-F0-9_]\+\%([iu]\%(size\|8\|16\|32\|64\|128\)\)\="
syn match     lqOctNumber   display "\<0o[0-7_]\+\%([iu]\%(size\|8\|16\|32\|64\|128\)\)\="
syn match     lqBinNumber   display "\<0b[01_]\+\%([iu]\%(size\|8\|16\|32\|64\|128\)\)\="

syn match     lqFloat       display "\<[0-9][0-9_]*\.\%([^[:cntrl:][:space:][:punct:][:digit:]]\|_\|\.\)\@!"
syn match     lqFloat       display "\<[0-9][0-9_]*\%(\.[0-9][0-9_]*\)\%([eE][+-]\=[0-9_]\+\)\=\(f32\|f64\)\="
syn match     lqFloat       display "\<[0-9][0-9_]*\%(\.[0-9][0-9_]*\)\=\%([eE][+-]\=[0-9_]\+\)\(f32\|f64\)\="
syn match     lqFloat       display "\<[0-9][0-9_]*\%(\.[0-9][0-9_]*\)\=\%([eE][+-]\=[0-9_]\+\)\=\(f32\|f64\)"
syn match     lqCatchErr    display "?[a-z][0-9a-zA-Z]\+"

syn match     lqFuncCall    "\w\(\w\)*("he=e-1,me=e-1

syn keyword     lqTodo              contained TODO FIXME XXX BUG
syn cluster     lqCommentGroup      contains=lqTodo
syn region      lqComment           start="/\*" end="\*/" contains=@lqCommentGroup,@Spell
syn region      lqComment           start="//" end="$" contains=@lqCommentGroup,@Spell

hi def link     lqComment           Comment
hi def link     lqTodo              Todo

hi def link lqType Type
hi def link lqCustomType Type
hi def link lqCondition Conditional
hi def link lqLoop Conditional
hi def link lqControlFlow Keyword
hi def link lqStruct Keyword
hi def link lqOtherKeyword Keyword
hi def link lqDefinition Keyword
hi def link lqSpecial Keyword
hi def link lqLeftArrow Operator
hi def link lqOperator Operator
hi def link lqFail Operator
hi def link lqCatchErr Operator
hi def link lqIn Keyword
hi def link lqString String
hi def link lqDecNumber Number
hi def link lqHexNumber Number
hi def link lqOctNumber Number
hi def link lqBinNumber Number
hi def link lqFloat Number
hi def link lqFuncCall Special

let b:current_syntax = "litequel"
