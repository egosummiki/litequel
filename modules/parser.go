package modules

import (
	"errors"
	"fmt"

	"gitlab.com/egosummiki/litequel/tokens"
)

type Parser struct{}

var errNoTokensLeft = errors.New("error parsing module file: unexpected end of file")

func parserError(token *tokens.Token, message string) error {
	return fmt.Errorf("error parsing module file [%d:%d]: %s", token.Line, token.Column, message)
}

type maybeToken struct {
	token *tokens.Token
	ok    bool
}

func tkn(token *tokens.Token, ok bool) maybeToken {
	return maybeToken{
		token: token,
		ok:    ok,
	}
}

func expect(token maybeToken, kind tokens.TokenType) error {
	if !token.ok {
		return errNoTokensLeft
	}

	if token.token.Kind != kind {
		return parserError(token.token, "unexpected token")
	}

	return nil
}

func expectKeyword(token maybeToken, name string) error {
	if !token.ok {
		return errNoTokensLeft
	}

	if token.token.Kind != tokens.TokenIdentifier {
		return parserError(token.token, fmt.Sprintf("unexpected token: expected %s keyword", name))
	}

	return nil
}

func check(token maybeToken, kind tokens.TokenType) (*tokens.Token, error) {
	if !token.ok {
		return nil, errNoTokensLeft
	}

	if token.token.Kind != kind {
		return nil, parserError(token.token, "unexpected token")
	}

	return token.token, nil
}

func (p *Parser) Parse(reader tokens.TokenReader) (Project, error) {
	if err := expectKeyword(tkn(reader.Next()), "project"); err != nil {
		return Project{}, nil
	}

	token, err := check(tkn(reader.Next()), tokens.TokenIdentifier)
	if err != nil {
		return Project{}, err
	}

	project := Project{
		Name: token.Content,
	}

	if err := expect(tkn(reader.Next()), ';'); err != nil {
		return Project{}, err
	}

	for {
		token, ok := reader.Next()
		if !ok {
			return project, nil
		}
		if token.Kind != tokens.TokenIdentifier {
			return project, parserError(token, "unexpected token")
		}

		if err != nil {
			return Project{}, err
		}

		switch token.Content {
		case "module":
			module, err := p.parseModule(reader)
			if err != nil {
				return Project{}, err
			}
			project.Modules = append(project.Modules, module)
		case "binary":
			binary, err := p.parseBinary(reader)
			if err != nil {
				return Project{}, err
			}
			project.Binaries = append(project.Binaries, binary)
		default:
			return Project{}, parserError(token, "unexpected token")
		}
	}
}

func (p *Parser) parseModule(reader tokens.TokenReader) (RawModule, error) {
	token, err := check(tkn(reader.Next()), tokens.TokenIdentifier)
	if err != nil {
		return RawModule{}, err
	}

	module := RawModule{
		Name: token.Content,
	}

	if err := expect(tkn(reader.Next()), '{'); err != nil {
		return RawModule{}, err
	}

	for {
		token, ok := reader.Next()
		if !ok {
			return RawModule{}, errNoTokensLeft
		}

		if token.Kind == '}' {
			break
		}

		if token.Kind != tokens.TokenString {
			return RawModule{}, parserError(token, "unexpected token")
		}

		module.Files = append(module.Files, token.Content)

		if err := expect(tkn(reader.Next()), ';'); err != nil {
			return RawModule{}, err
		}
	}

	if err := expect(tkn(reader.Next()), ';'); err != nil {
		return RawModule{}, err
	}

	return module, nil
}

func (p *Parser) parseBinary(reader tokens.TokenReader) (Binary, error) {
	token, err := check(tkn(reader.Next()), tokens.TokenIdentifier)
	if err != nil {
		return Binary{}, err
	}
	result := Binary{
		Name: token.Content,
	}

	token, err = check(tkn(reader.Next()), tokens.TokenString)
	if err != nil {
		return Binary{}, err
	}
	result.Entrypoint = token.Content

	if err := expect(tkn(reader.Next()), ';'); err != nil {
		return Binary{}, err
	}

	return result, err
}
