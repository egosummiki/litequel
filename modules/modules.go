package modules

import (
	"io/ioutil"

	"gitlab.com/egosummiki/litequel/generation"
	"gitlab.com/egosummiki/litequel/lexer"
	"gitlab.com/egosummiki/litequel/parser"
	"gitlab.com/egosummiki/litequel/runtime/execution"
	"gitlab.com/egosummiki/litequel/symbols"
	"gitlab.com/egosummiki/litequel/symbols/scope"
	"gitlab.com/egosummiki/litequel/symbols/store"
	table2 "gitlab.com/egosummiki/litequel/symbols/table"
)

type ModuleManager struct {
}

type Project struct {
	Name         string
	Binaries     []Binary
	Dependencies []Dependency
	Modules      []RawModule
}

type Binary struct {
	Name       string
	Entrypoint string
}

type Dependency struct {
	Name string
	Url  string
}

type RawModule struct {
	Name  string
	Files []string
}

type Module struct {
	Name    string
	Table   symbols.Table
	Buckets *execution.Bucket
}

func (mm *ModuleManager) LoadModule(file string) error {
	buffer, err := ioutil.ReadFile(file)
	if err != nil {
		return err
	}

	tokens, err := lexer.ReadTokens(buffer)
	if err != nil {
		return err
	}

	table := table2.New(store.New(), scope.NewSymbolCounter())
	//litequel.RegisterBuiltInSymbols(table)

	parsing := parser.NewParser()

	ast, err := parsing.Parse(tokens)
	if err != nil {
		return err
	}

	exec, err := generation.GenerateInstructions(nil, table, "", ast)
	if err != nil {
		return err
	}

	_ = exec

	return nil
}
