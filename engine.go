package litequel

import (
	"gitlab.com/egosummiki/litequel/model/json"
	"gitlab.com/egosummiki/litequel/model/net"
	"gitlab.com/egosummiki/litequel/model/reflect"
	"gitlab.com/egosummiki/litequel/model/unsafe"
	"gitlab.com/egosummiki/litequel/modules"
	"gitlab.com/egosummiki/litequel/semantics"
	"gitlab.com/egosummiki/litequel/symbols/scope"
	"gitlab.com/egosummiki/litequel/symbols/store"
	"gitlab.com/egosummiki/litequel/symbols/table"
	"gitlab.com/egosummiki/litequel/types"

	"math/rand"
	"time"

	"gitlab.com/egosummiki/litequel/generation"
	"gitlab.com/egosummiki/litequel/lexer"
	"gitlab.com/egosummiki/litequel/model"
	//"gitlab.com/egosummiki/litequel/model/function"
	"gitlab.com/egosummiki/litequel/model/std"
	"gitlab.com/egosummiki/litequel/parser"
	"gitlab.com/egosummiki/litequel/runtime"
	"gitlab.com/egosummiki/litequel/runtime/execution"
	"gitlab.com/egosummiki/litequel/runtime/instruction"
	"gitlab.com/egosummiki/litequel/symbols"
	"gitlab.com/egosummiki/litequel/tokens"
)

type Engine struct {
	ctx         *runtime.Context
	functions   map[string]runtime.Object
	properties  map[types.ID][]string
	modules     modules.ModuleManager
	symbolTable symbols.Table
	parser      *parser.Parser
}

func NewEngine() *Engine {
	symbolTable := table.New(store.New(), scope.NewSymbolCounter())
	for _, prim := range types.PrimitiveDetails {
		symbolTable.MustRegisterType(prim)
	}

	RegisterBuiltInSymbols(symbolTable)
	rand.Seed(time.Now().Unix())
	parser := parser.NewParser()
	return &Engine{
		symbolTable: symbolTable,
		parser:      parser,
	}
}

func RegisterBuiltInSymbols(symbolTable symbols.Table) {
	funcInputType := symbolTable.MustRegisterType(types.NewFunction(types.TypeString, nil, false))
	funcReadType := symbolTable.MustRegisterType(types.NewFunction(types.TypeInt32, nil, false))
	funcDisasemType := symbolTable.MustRegisterType(types.NewFunction(types.TypeUnknown, []types.ID{types.TypeUnknown}, false))
	funcLenType := symbolTable.MustRegisterType(types.NewFunction(types.TypeInt32, []types.ID{types.TypeUnknown}, false))
	funcRandType := symbolTable.MustRegisterType(types.NewFunction(types.TypeInt32, nil, true))
	funcOrdType := symbolTable.MustRegisterType(types.NewFunction(types.TypeInt8, []types.ID{types.TypeString}, false))
	funcChrType := symbolTable.MustRegisterType(types.NewFunction(types.TypeString, []types.ID{types.TypeInt8}, false))
	funcSMTPAuthType := symbolTable.MustRegisterType(types.NewFunction(types.TypeUnknown, nil, true))
	funcSendMailType := symbolTable.MustRegisterType(types.NewFunction(types.TypeString, nil, true))
	funcTakeStringReturnString := symbolTable.MustRegisterType(types.NewFunction(types.TypeString, []types.ID{types.TypeString}, false))
	funcUnixTimeType := symbolTable.MustRegisterType(types.NewFunction(types.TypeInt64, nil, false))
	funcDref := symbolTable.MustRegisterType(types.NewFunction(types.TypeUnknown, []types.ID{types.TypeInt32, types.TypePointer}, false))

	symbolTable.MustRegisterGlobal("input", funcInputType)
	symbolTable.MustRegisterGlobal("readInt", funcReadType)
	symbolTable.MustRegisterGlobal("disasem", funcDisasemType)
	symbolTable.MustRegisterGlobal("len", funcLenType)
	symbolTable.MustRegisterGlobal("rand", funcRandType)
	symbolTable.MustRegisterGlobal("ord", funcOrdType)
	symbolTable.MustRegisterGlobal("chr", funcChrType)
	symbolTable.MustRegisterGlobal("smtpAuth", funcSMTPAuthType)
	symbolTable.MustRegisterGlobal("sendMail", funcSendMailType)
	symbolTable.MustRegisterGlobal("base64", funcTakeStringReturnString)
	symbolTable.MustRegisterGlobal("base64dec", funcTakeStringReturnString)
	symbolTable.MustRegisterGlobal("unixtime", funcUnixTimeType)
	symbolTable.MustRegisterGlobal("readfile", funcTakeStringReturnString)
	symbolTable.MustRegisterGlobal("unsafe::dref", funcDref)

	symbolTable.MustRegisterGlobal("true", types.TypeInt32)
	symbolTable.MustRegisterGlobal("false", types.TypeInt32)
	symbolTable.MustRegisterGlobal("out", types.TypeBuffer)
	symbolTable.MustRegisterGlobal("none", types.TypeUnknown)

	metListRem := symbolTable.MustRegisterType(types.NewMethod(types.TypeUnknown, []types.ID{types.TypeInt32}, types.TypeArray, false))
	metBuffPrint := symbolTable.MustRegisterType(types.NewMethod(types.TypeUnknown, []types.ID{types.TypeString}, types.TypeBuffer, true))
	metStringUpper := symbolTable.MustRegisterType(types.NewMethod(types.TypeString, nil, types.TypeString, true))

	symbolTable.MustRegisterProperty(types.TypeArray, "len", types.TypeInt32)
	symbolTable.MustRegisterProperty(types.TypeArray, "rem", metListRem)
	symbolTable.MustRegisterProperty(types.TypeString, "len", types.TypeInt32)
	symbolTable.MustRegisterProperty(types.TypeRange, "len", types.TypeInt32)
	symbolTable.MustRegisterProperty(types.TypeRange, "min", types.TypeInt32)
	symbolTable.MustRegisterProperty(types.TypeRange, "max", types.TypeInt32)
	symbolTable.MustRegisterProperty(types.TypeBuffer, "print", metBuffPrint)
	symbolTable.MustRegisterProperty(types.TypeString, "upper", metStringUpper)
	symbolTable.MustRegisterProperty(types.TypeError, "msg", types.TypeString)
	symbolTable.MustRegisterProperty(types.TypeError, "line", types.TypeInt32)
	symbolTable.MustRegisterProperty(types.TypeError, "col", types.TypeInt32)
	symbolTable.MustRegisterProperty(types.TypeError, "class", types.TypeString)

	reflect.AddSymbols(symbolTable)
	json.AddSymbols(symbolTable)
}

func (e *Engine) prepareContext(exec *execution.Execution) *runtime.Error {
	e.ctx = runtime.NewRuntimeContext(exec, e.symbolTable.GenerateReflector(), e.symbolTable.ScopeReferences(scope.Global))
	modules := e.parser.Modules()
	for _, m := range modules {
		e.ctx.AddModule(m)
		if err := m.OnInit(e.ctx, e.symbolTable); err != nil {
			return err
		}
	}

	e.ctx.SetGlobal(e.symbolTable.MustFind("input", scope.Global).Reference, std.InputFunc{})
	e.ctx.SetGlobal(e.symbolTable.MustFind("readInt", scope.Global).Reference, std.ReadIntFunc{})
	e.ctx.SetGlobal(e.symbolTable.MustFind("disasem", scope.Global).Reference, std.DisasemFunc{})
	e.ctx.SetGlobal(e.symbolTable.MustFind("ord", scope.Global).Reference, std.OrdFunc{})
	e.ctx.SetGlobal(e.symbolTable.MustFind("chr", scope.Global).Reference, std.ChrFunc{})
	e.ctx.SetGlobal(e.symbolTable.MustFind("len", scope.Global).Reference, std.LenFunc{})
	e.ctx.SetGlobal(e.symbolTable.MustFind("rand", scope.Global).Reference, std.RandFunc{})
	e.ctx.SetGlobal(e.symbolTable.MustFind("unsafe::dref", scope.Global).Reference, unsafe.DRefFunc{})
	e.ctx.SetGlobal(e.symbolTable.MustFind("true", scope.Global).Reference, model.Int32Object(1))
	e.ctx.SetGlobal(e.symbolTable.MustFind("false", scope.Global).Reference, model.Int32Object(0))
	e.ctx.SetGlobal(e.symbolTable.MustFind("out", scope.Global).Reference, std.OutObj{})
	e.ctx.SetGlobal(e.symbolTable.MustFind("smtpAuth", scope.Global).Reference, net.SmtpAuthFunc{})
	e.ctx.SetGlobal(e.symbolTable.MustFind("sendMail", scope.Global).Reference, net.SendMailFunc{})
	e.ctx.SetGlobal(e.symbolTable.MustFind("base64", scope.Global).Reference, std.Base64Func{})
	e.ctx.SetGlobal(e.symbolTable.MustFind("base64dec", scope.Global).Reference, std.Base64DecFunc{})
	e.ctx.SetGlobal(e.symbolTable.MustFind("unixtime", scope.Global).Reference, std.TimeUnix{})
	e.ctx.SetGlobal(e.symbolTable.MustFind("readfile", scope.Global).Reference, std.ReadFile{})
	e.ctx.SetGlobal(e.symbolTable.MustFind("none", scope.Global).Reference, model.None)
	e.ctx.SetGlobal(e.symbolTable.MustFindProperty(types.TypeBuffer, "print").Reference, std.OutPrint{})
	e.ctx.SetGlobal(e.symbolTable.MustFindProperty(types.TypeString, "upper").Reference, model.UpperFunc{})

	reflect.InjectObjects(e.ctx, e.symbolTable)
	json.InjectObjects(e.ctx, e.symbolTable)
	return nil
}

func (e *Engine) Represent(exec *execution.Execution) {
	bucket := exec.GetBucket(0)
	size := len(bucket.Instructions)
	if size == 0 {
		return
	}
	last := bucket.Instructions[size-1]
	switch last.(type) {
	case instruction.PopInst:
		bucket.Instructions[size-1] = instruction.DescribeInst{}
	}
}

func (e *Engine) GetTokens(buffer []byte) (tokens.TokenReader, error) {
	return lexer.ReadTokens(buffer)
}

func (e *Engine) GetAST(tokens tokens.TokenReader) ([]interface{}, error) {
	ast, err := e.parser.Parse(tokens)
	if err != nil {
		return nil, err
	}

	analysis := semantics.Analysis{
		Table: e.symbolTable,
		Ast:   ast,
	}
	if err := analysis.PopulateSymbols(scope.Base); err != nil {
		return nil, err
	}

	return ast, nil
}

func (e *Engine) GetInstructions(prevExec *execution.Execution, ast []interface{}) (*execution.Execution, error) {
	return generation.GenerateInstructions(prevExec, e.symbolTable, "", ast)
}

func (e *Engine) Compile(buffer []byte) (*execution.Execution, error) {
	tkns, err := e.GetTokens(buffer)
	if err != nil {
		return nil, err
	}
	ast, err := e.GetAST(tkns)
	if err != nil {
		return nil, err
	}
	return e.GetInstructions(nil, ast)
}

func (e *Engine) Execute(exec *execution.Execution) *runtime.Error {
	if e.ctx == nil {
		if err := e.prepareContext(exec); err != nil {
			return err
		}
	} else {
		e.ctx.Update(exec, e.symbolTable.ScopeReferences(scope.Global))
	}
	return e.ctx.Run(0)
}

func (e *Engine) Evaluate(buffer []byte) error {
	instr, err := e.Compile(buffer)
	if err != nil {
		return err
	}
	rtErr := e.Execute(instr)
	if rtErr != nil {
		return rtErr
	}
	return nil
}
