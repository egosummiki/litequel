# Litequel

Litequel is a work in progress general pupose interpreted dynamic programming language.
The main goal of litequel is to query key-value databases. Litequel supports both imperative and structural style of programming.

## Try it out

Compile the interpreter and use it!
```bash
go install ./...
lqrun demo.lq
```

## Design ideas

+ `let` symbol to declare variables
+ `def` symbol to declare functions
+ `struct [name] {[name] : [type]; [name] : [type]; ....}` to declare structures

## Hello World

Pretty standard

```python
out.print('Hello World')
```

Same result can be achieved with "out" buffer:

```python
out <- 'Hello World\n'
```

## Variables

```javascript
let n = 34 // declaration
n += 61
n++
```

## Builtin Data Structures

### Strings

```javascript
let str2 = 'string example'
let str  = "template string {str2}"
let sum  = str + str2
let ch   = sum[3]
```

Double quote represents a string template that can contain formatted variables

```javascript
let name = 'John'
let surname = 'Smith'
let age = 34

out <- "{name} {surname} is {age} years old"
```

### Lists

```javascript
let v = [3, 6, 7]
v <- 45 // append
let slice = v[1 .. 2] // slice of an array
```

## Control flow

### If statement

```javascript
if a == 4 {
    out.print('A')
} else if a > 7 {
    out.print('B')
} else {
    out.print('C')
}
```

### For loop

For loop handles iterable objects.
also integers are iterable!

```javascript
for a in 10 { // 10 is not included
    out.print(a)
}
```

```javascript
for a in 2..10 { // Range is defined with ..
    out.print(a)
}
```

```javascript
for a in [4, 2, 1] { // We can iterate over arrays
    out.print(a)
}
```

### While loop

```javascript
let n = 0
while n < 10 {
    out.print(n)
    n++
}
```

## Function definition

```ruby
def foo(a: int, b: int) -> int {
    let result = 0
    for n in a {
        result += n*n*b - 2
    }
    return result
}
```

## Scopes and Globals

Litequel doesn't allow you to create global variables.
Function defined in the main scope are global by definition.
But functions defined in other functions are not and are treated as closures.

```ruby
let a = 30
def foo() {
    out.print(a)
}
```
This sucessully prints out '30', even though the variable is outside the scope of the function.

## Queries

See 'examples/boltdb'
