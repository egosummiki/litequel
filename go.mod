module gitlab.com/egosummiki/litequel

go 1.13

require (
	github.com/boltdb/bolt v1.3.1 // indirect
	github.com/google/uuid v1.1.1
	golang.org/x/sys v0.0.0-20191025090151-53bf42e6b339 // indirect
)
