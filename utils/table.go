package utils

func expandString(s string, size int) string {
	result := make([]byte, size)
	copy(result, s)
	for i := len(s); i < size; i++ {
		result[i] = ' '
	}
	return string(result)
}

// Table represents a cli table
type Table struct {
	maxRowSizes []int
	rows        []string
	labels      []string
}

// Creates a new table
func NewTable(labels ...string) (table *Table) {
	table = &Table{
		maxRowSizes: make([]int, len(labels)),
		labels:      make([]string, len(labels)),
		rows:        make([]string, 0, 100),
	}
	for i, label := range labels {
		table.labels[i] = label
		table.maxRowSizes[i] = len(label)
	}
	return
}

// Adds a row
func (t *Table) AddRow(values ...string) {
	for i, s := range values {
		if len(s) > t.maxRowSizes[i] {
			t.maxRowSizes[i] = len(s)
		}
	}
	for _, v := range values {
		t.rows = append(t.rows, v)
	}
}

// Print prints the table
func (t *Table) Print() string {
	var r string
	for i, label := range t.labels {
		r += expandString(label, t.maxRowSizes[i])
		r += "  "
	}
	r += "\n"

	sumSizes := (len(t.maxRowSizes) - 1) * 2
	for _, size := range t.maxRowSizes {
		sumSizes += size
	}
	for i := 0; i < sumSizes; i++ {
		r += "-"
	}
	r += "\n"

	for i, row := range t.rows {
		column := i % len(t.maxRowSizes)
		r += expandString(row, t.maxRowSizes[column])
		if column == len(t.maxRowSizes)-1 {
			r += "\n"
		} else {
			r += "  "
		}
	}

	return r
}
