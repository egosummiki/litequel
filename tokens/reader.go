package tokens

type tokenBuffer struct {
	buffer   []Token
	position int
}

func (tb *tokenBuffer) Next() (*Token, bool) {
	tb.position++
	if tb.position >= len(tb.buffer) {
		return nil, false
	}
	return &tb.buffer[tb.position], true
}

func (tb *tokenBuffer) SeekBack() {
	if tb.position >= 0 {
		tb.position--
	}
}

func (tb *tokenBuffer) SeekTo(position int) {
	tb.position = position
}

func (tb *tokenBuffer) Position() int {
	return tb.position
}

func (tb *tokenBuffer) HasTokens() bool {
	if tb.position+1 >= len(tb.buffer) {
		return false
	}
	return true
}

func (tb *tokenBuffer) EndsWithSemi() bool {
	if len(tb.buffer) == 0 {
		return true // Special case, returns true
	}
	return tb.buffer[len(tb.buffer)-1].Kind == TokenSemicolon
}

func (tb *tokenBuffer) EndsScope() bool {
	return tb.buffer[0].Kind == '}'
}

func (tb *tokenBuffer) Join(tr TokenReader) {
	tb.buffer = append(tb.buffer, tr.(*tokenBuffer).buffer...)
}

func NewMockReader(tokens []Token) TokenReader {
	return &tokenBuffer{buffer: tokens, position: -1}
}

func ReadAll(reader TokenReader) []Token {
	var tokens []Token
	for {
		token, ok := reader.Next()
		if !ok {
			return tokens
		}
		tokens = append(tokens, *token)
	}
}
