package tokens

import (
	"unicode/utf8"
)

type tokenSet []Token

func NewTokenCollector() TokenCollector {
	return &tokenSet{}
}

func (ts *tokenSet) AddToken(kind TokenType, content string, line, column int) {
	*ts = append(*ts, Token{
		Kind:    kind,
		Content: content,
		Line:    line,
		Column:  column - utf8.RuneCountInString(content),
	})
}

func (ts *tokenSet) Last() Token {
	if len(*ts) == 0 {
		return Token{}
	}
	return (*ts)[len(*ts)-1]
}

func (ts *tokenSet) GetReader() TokenReader {
	return &tokenBuffer{buffer: *ts, position: -1}
}
