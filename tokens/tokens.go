package tokens

import (
	"fmt"
)

type TokenType int32

const (
	NoToken TokenType = 0

	// Literals
	TokenIdentifier  TokenType = 1
	TokenString      TokenType = 2
	TokenStringDQ    TokenType = 3
	TokenHexadecimal TokenType = 4
	TokenBinary      TokenType = 5
	TokenDecimal     TokenType = 6
	TokenFloat       TokenType = 7

	// Symbol
	TokenIn TokenType = 8

	// Definitions
	TokenStruct    TokenType = 9
	TokenLet       TokenType = 10
	TokenDef       TokenType = 11
	TokenConst     TokenType = 12
	TokenUse       TokenType = 13
	TokenModule    TokenType = 14
	TokenPub       TokenType = 15
	TokenEnum      TokenType = 16
	TokenInterface TokenType = 17

	// Control flow
	TokenIf       TokenType = 18
	TokenElse     TokenType = 19
	TokenBreak    TokenType = 20
	TokenContinue TokenType = 21
	TokenFor      TokenType = 22
	TokenWhile    TokenType = 23
	TokenReturn   TokenType = 24
	TokenFail     TokenType = 25
	TokenOr       TokenType = 26
	TokenLambda   TokenType = 27

	// >= 33 Operators and Seperators

	TokenSemicolon TokenType = ';'

	// Types
	TokenTypeLiteral TokenType = 140
	TokenTypeString  TokenType = 141
	TokenTypeBool    TokenType = 142
	TokenTypeVector  TokenType = 143
	TokenTypeInt8    TokenType = 144
	TokenTypeInt16   TokenType = 145
	TokenTypeInt32   TokenType = 146
	TokenTypeInt64   TokenType = 147
	TokenTypeUInt8   TokenType = 148
	TokenTypeUInt16  TokenType = 149
	TokenTypeUInt32  TokenType = 150
	TokenTypeUInt64  TokenType = 151
	TokenTypeFloat32 TokenType = 152
	TokenTypeFloat64 TokenType = 153
	TokenTypeFunc    TokenType = 154
	TokenTypeError   TokenType = 155
	TokenTypeMap     TokenType = 156
	TokenTypePtr     TokenType = 157

	// CombinedOperators ex. '='*128 + '='
)

type Token struct {
	Kind    TokenType
	Content string
	Line    int
	Column  int
}

type TokenCollector interface {
	AddToken(kind TokenType, contents string, line, column int)
	Last() Token
	GetReader() TokenReader
}

type TokenReader interface {
	Next() (*Token, bool)
	Position() int
	SeekBack()
	SeekTo(int)
	HasTokens() bool
	EndsWithSemi() bool
	Join(TokenReader)
	EndsScope() bool
}

func (t *Token) IsIdentifierOrLiteral() bool {
	return t.Kind >= TokenIdentifier && t.Kind <= TokenFloat
}

func (t *Token) IsLiteral() bool {
	return t.Kind > TokenIdentifier && t.Kind <= TokenFloat
}

func (t *Token) IsOperator() bool {
	return t.Kind >= 33
}

func (t *Token) IsType() bool {
	return t.Kind >= TokenTypeLiteral && t.Kind <= TokenTypePtr
}

func (t *Token) String() string {
	if t.Kind == TokenIdentifier || t.Kind == TokenString || t.Kind == TokenDecimal {
		return fmt.Sprintf("%s \"%s\"", t.Kind.String(), t.Content)
	}
	return t.Kind.String()
}

func decodeOperator(op int32) string {
	var result []rune
	for op > 0 {
		result = append([]rune{rune(op % 128)}, result...)
		op /= 128
	}
	return string(result)
}

func (t TokenType) String() string {
	switch t {
	case NoToken:
		return "no a token"
	case TokenIdentifier:
		return "identifier"
	case TokenString:
		return "string"
	case TokenIn:
		return "in"
	case TokenHexadecimal:
		return "hexadecimal"
	case TokenBinary:
		return "binary"
	case TokenDecimal:
		return "decimal"
	case TokenFloat:
		return "float"
	case TokenStruct:
		return "is"
	case TokenLet:
		return "let"
	case TokenIf:
		return "if"
	case TokenElse:
		return "else"
	case TokenBreak:
		return "break"
	case TokenContinue:
		return "continue"
	case TokenFor:
		return "for"
	case TokenWhile:
		return "while"
	case TokenDef:
		return "def"
	case TokenConst:
		return "const"
	case TokenReturn:
		return "return"
	case TokenTypeLiteral:
		return "literal type"
	case TokenTypeString:
		return "string type"
	case TokenTypeBool:
		return "bool type"
	case TokenTypeVector:
		return "list type"
	case TokenTypeInt8:
		return "int8 type"
	case TokenTypeInt16:
		return "int16 type"
	case TokenTypeInt32:
		return "int32 type"
	case TokenTypeInt64:
		return "int64 type"
	case TokenTypeUInt8:
		return "uint8 type"
	case TokenTypeUInt16:
		return "uint16 type"
	case TokenTypeUInt32:
		return "uint32 type"
	case TokenTypeUInt64:
		return "uint64 type"
	case TokenTypeFloat32:
		return "float32 type"
	case TokenTypeFloat64:
		return "float64 type"
	case TokenTypeError:
		return "error type"
	case TokenTypeMap:
		return "map type"
	}

	return decodeOperator(int32(t))
}

func Join(values ...rune) TokenType {
	result := 0
	prod := 1
	for i := len(values) - 1; i >= 0; i-- {
		result += int(values[i]) * prod
		prod *= 128
	}
	return TokenType(result)
}
