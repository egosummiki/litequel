package reflect

import (
	"fmt"

	"gitlab.com/egosummiki/litequel/model"
	"gitlab.com/egosummiki/litequel/runtime"
	"gitlab.com/egosummiki/litequel/symbols"
	"gitlab.com/egosummiki/litequel/symbols/scope"
	"gitlab.com/egosummiki/litequel/types"
)

var ReflectType types.ID
var NameTypePairType types.ID

type ReflectObj struct{}

func (o ReflectObj) Describe(ctx *runtime.Context) string {
	return "<reflection>"
}

func (o ReflectObj) Type() types.ID {
	return ReflectType
}

type NameTypePair types.TypeField

func (p NameTypePair) Describe(ctx *runtime.Context) string {
	return fmt.Sprintf("NameType{name: \"%s\", type: %d}", p.Name, p.TypeID)
}

func (p NameTypePair) Convert(ctx *runtime.Context, typeID types.ID) (runtime.Object, *runtime.Error) {
	if typeID == types.TypeString {
		return model.StringObject(p.Describe(ctx)), nil
	}
	return model.None, nil
}

func (p NameTypePair) Type() types.ID {
	return NameTypePairType
}

func (p NameTypePair) GetAttribute(ctx *runtime.Context, index int) runtime.Object {
	switch index {
	case 0:
		return model.Int32Object(p.TypeID)
	case 1:
		return model.StringObject(p.Name)
	}

	return model.None
}

func (p NameTypePair) Index(ctx *runtime.Context, index runtime.Object) (runtime.Object, *runtime.Error) {
	if index, ok := index.(model.StringObject); ok {
		str := string(index)
		for _, a := range p.Annots {
			if a.Key == str {
				return model.StringObject(a.Value), nil
			}
		}
	}
	return model.None, nil
}

type TypeMethod struct{}

func (f TypeMethod) Describe(ctx *runtime.Context) string {
	return "<method type>"
}

func (f TypeMethod) Type() types.ID {
	return types.TypeFunction
}

func (f TypeMethod) Call(ctx *runtime.Context, numArgs int) *runtime.Error {
	stack := ctx.Stack()
	obj := stack.Pop()
	stack.Pop() // FUNC
	stack.Pop() // THIS
	stack.Push(model.StringObject(ctx.Reflector().GetTypeName(obj.Type())))
	return nil
}

type TidMethod struct{}

func (f TidMethod) Describe(ctx *runtime.Context) string {
	return "<method tid>"
}

func (f TidMethod) Type() types.ID {
	return types.TypeFunction
}

func (f TidMethod) Call(ctx *runtime.Context, numArgs int) *runtime.Error {
	stack := ctx.Stack()
	obj := stack.Pop()
	stack.Pop() // FUNC
	stack.Pop() // THIS
	stack.Push(model.Int32Object(obj.Type()))
	return nil
}

type TypeNameMethod struct{}

func (f TypeNameMethod) Describe(ctx *runtime.Context) string {
	return "<method typename>"
}

func (f TypeNameMethod) Type() types.ID {
	return types.TypeFunction
}

func (f TypeNameMethod) Call(ctx *runtime.Context, numArgs int) *runtime.Error {
	stack := ctx.Stack()
	obj, ok := stack.Pop().(model.Int32Object)
	if !ok {
		return ctx.Error("expected int32 type")
	}
	stack.Pop() // FUNC
	stack.Pop() // THIS
	stack.Push(model.StringObject(ctx.Reflector().GetTypeName(types.ID(obj))))
	return nil
}

type AttribsMethod struct{}

func (f AttribsMethod) Describe(ctx *runtime.Context) string {
	return "<method attribs>"
}

func (f AttribsMethod) Type() types.ID {
	return types.TypeFunction
}

func (f AttribsMethod) Call(ctx *runtime.Context, numArgs int) *runtime.Error {
	stack := ctx.Stack()
	obj := stack.Pop()
	stack.Pop() // FUNC
	stack.Pop() // THIS

	var r model.ListObject
	fields := ctx.Reflector().GetTypeAttribs(obj.Type())
	for _, f := range fields {
		r = append(r, NameTypePair(f))
	}
	stack.Push(&r)

	return nil
}

type AValueMethod struct{}

func (f AValueMethod) Describe(ctx *runtime.Context) string {
	return "<method avalue>"
}

func (f AValueMethod) Type() types.ID {
	return types.TypeFunction
}

func (f AValueMethod) Call(ctx *runtime.Context, numArgs int) *runtime.Error {
	if numArgs != 2 {
		return ctx.Error("invalid number of arguments")
	}
	stack := ctx.Stack()
	attrib, ok := stack.Pop().(model.Int32Object)
	if !ok {
		return ctx.Error("expected int32 type")
	}
	obj, ok := stack.Pop().(runtime.AttributeOwner)
	if !ok {
		return ctx.Error("object doesn't own attributes")
	}

	stack.Pop() // FUNC
	stack.Pop() // THIS
	stack.Push(obj.GetAttribute(ctx, int(attrib)))
	return nil
}

type NewMethod struct{}

func (f NewMethod) Describe(ctx *runtime.Context) string {
	return "<method new>"
}

func (f NewMethod) Type() types.ID {
	return types.TypeFunction
}

func (f NewMethod) Call(ctx *runtime.Context, numArgs int) *runtime.Error {
	if numArgs == 0 {
		return ctx.Error("invalid number of arguments")
	}

	stack := ctx.Stack()

	t, ok := stack.Pop().(model.Int32Object)
	if !ok {
		return ctx.Error("expected int32 type")
	}
	typeID := types.ID(t)

	numAttribs := len(ctx.Reflector().GetTypeAttribs(typeID))
	fields := make([]runtime.Object, numAttribs)
	for i := range fields {
		fields[i] = model.None
	}

	stack.Pop() // FUNC
	stack.Pop() // THIS
	stack.Push(&model.UserDefinedObject{
		Data:      fields,
		ObjType:   typeID,
		AddMethod: 0,
	})
	return nil
}

func AddSymbols(symbolTable symbols.Table) {
	ReflectType, _ = symbolTable.RegisterType(types.NewLanguageSpecific("Reflect"))
	NameTypePairType, _ = symbolTable.RegisterType(types.NewLanguageSpecific("NameType"))

	variadicReturnsString := symbolTable.MustRegisterType(types.NewMethod(types.TypeString, nil, ReflectType, true))
	variadicReturnsUnknown := symbolTable.MustRegisterType(types.NewMethod(types.TypeUnknown, nil, ReflectType, true))
	variadicReturnsInt32 := symbolTable.MustRegisterType(types.NewMethod(types.TypeInt32, nil, ReflectType, true))

	listOfPairs, err := symbolTable.RegisterType(types.ListOf(NameTypePairType))
	if err != nil {
		panic(err)
	}
	attribsType := symbolTable.MustRegisterType(types.NewMethod(listOfPairs, []types.ID{types.TypeUnknown}, ReflectType, true))

	err = symbolTable.RegisterGlobal("reflect", ReflectType)
	if err != nil {
		panic(err)
	}

	err = symbolTable.RegisterProperty(ReflectType, "type", variadicReturnsString)
	if err != nil {
		panic(err)
	}

	err = symbolTable.RegisterProperty(ReflectType, "tid", variadicReturnsString)
	if err != nil {
		panic(err)
	}


	err = symbolTable.RegisterProperty(ReflectType, "attribs", attribsType)
	if err != nil {
		panic(err)
	}

	err = symbolTable.RegisterProperty(ReflectType, "typename", variadicReturnsString)
	if err != nil {
		panic(err)
	}

	err = symbolTable.RegisterProperty(ReflectType, "valueAt", variadicReturnsString)
	if err != nil {
		panic(err)
	}

	err = symbolTable.RegisterProperty(ReflectType, "new", variadicReturnsUnknown)
	if err != nil {
		panic(err)
	}

	err = symbolTable.RegisterProperty(NameTypePairType, "type", variadicReturnsInt32)
	if err != nil {
		panic(err)
	}

	err = symbolTable.RegisterProperty(NameTypePairType, "name", variadicReturnsString)
	if err != nil {
		panic(err)
	}

}

func InjectObjects(ctx *runtime.Context, symbolTable symbols.Table) {
	ctx.SetGlobal(symbolTable.MustFind("reflect", scope.Global).Reference, ReflectObj{})
	ctx.SetGlobal(symbolTable.MustFindProperty(ReflectType, "type").Reference, TypeMethod{})
	ctx.SetGlobal(symbolTable.MustFindProperty(ReflectType, "tid").Reference, TidMethod{})
	ctx.SetGlobal(symbolTable.MustFindProperty(ReflectType, "attribs").Reference, AttribsMethod{})
	ctx.SetGlobal(symbolTable.MustFindProperty(ReflectType, "typename").Reference, TypeNameMethod{})
	ctx.SetGlobal(symbolTable.MustFindProperty(ReflectType, "valueAt").Reference, AValueMethod{})
	ctx.SetGlobal(symbolTable.MustFindProperty(ReflectType, "new").Reference, NewMethod{})
}