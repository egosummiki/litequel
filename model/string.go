package model

import (
	"fmt"
	"strconv"
	"strings"

	"gitlab.com/egosummiki/litequel/runtime"
	"gitlab.com/egosummiki/litequel/types"
)

// Also string expression
type StringObject []rune

func (se StringObject) Describe(ctx *runtime.Context) string {
	var preparedString string
	if len(se) > 10 {
		preparedString = string(se[:10]) + "..."
	} else {
		preparedString = string(se)
	}
	preparedString = strings.ReplaceAll(preparedString, "\n", "\\n")
	return fmt.Sprintf("<str len=%d, val=\"%s\">", len(se), preparedString)
}

func (se StringObject) Type() types.ID {
	return types.TypeString
}

func (se StringObject) Convert(ctx *runtime.Context, typeID types.ID) (runtime.Object, *runtime.Error) {
	switch typeID {
	case types.TypeString:
		return se, nil
	case types.TypeInt8:
		res, _ := strconv.Atoi(string(se))
		return Int8Object(res), nil
	case types.TypeInt16:
		res, _ := strconv.Atoi(string(se))
		return Int16Object(res), nil
	case types.TypeInt32:
		res, _ := strconv.Atoi(string(se))
		return Int32Object(res), nil
	case types.TypeInt64:
		res, _ := strconv.ParseInt(string(se), 10, 64)
		return Int64Object(res), nil
	case types.TypeFloat32:
		res, _ := strconv.ParseFloat(string(se), 32)
		return Float32Object(res), nil
	case types.TypeFloat64:
		res, _ := strconv.ParseFloat(string(se), 64)
		return Float64Object(res), nil
	}
	return None, nil
}

func (se StringObject) Index(ctx *runtime.Context, index runtime.Object) (runtime.Object, *runtime.Error) {
	if num, ok := index.(runtime.Number); ok {
		return StringObject([]rune{se[num.ToInt()]}), nil
	}
	if r, ok := index.(*Range); ok {
		return se[r.beg:r.end], nil
	}

	return None, nil
}

func (se StringObject) GetAttribute(ctx *runtime.Context, index int) runtime.Object {
	/*
		0 - len
		1 - rem
	*/
	switch index {
	case 0:
		return Int32Object(len(se))
	}

	return None
}

func (se StringObject) Iter(ctx *runtime.Context) runtime.Iterator {
	return &StringIterator{str: &se, at: -1}
}

func (se StringObject) Add(ctx *runtime.Context, obj runtime.Object) runtime.Object {
	conv, err := obj.(runtime.Convertible).Convert(ctx, types.TypeString)
	if err != nil {
		return None
	}
	return append(se, conv.(StringObject)...)
}

func (se StringObject) Compare(ctx *runtime.Context, other runtime.Object) (int, *runtime.Error) {
	if str, ok := other.(StringObject); ok {
		return strings.Compare(string(se), string(str)), nil
	}
	return 0, ctx.Error("objects are not comparable")
}

type UpperFunc struct {
}

func (f UpperFunc) Call(ctx *runtime.Context, numArgs int) *runtime.Error {
	stack := ctx.Stack()
	stack.Pop() // FUNC
	obj, ok := stack.Pop().(StringObject)
	if !ok {
		stack.Push(StringObject(""))
		return nil
	}
	stack.Push(StringObject(strings.ToUpper(string(obj))))
	return nil
}

func (f UpperFunc) Describe(ctx *runtime.Context) string {
	return "<method upper>"
}

func (f UpperFunc) Type() types.ID {
	return types.TypeFunction
}

type StringFormatObject struct {
	Content []rune
	Format  []int
}

func (sfo StringFormatObject) String(ctx *runtime.Context) []rune {
	var result []rune
	stack := ctx.Stack()
	formatIndex := 0
	for i, c := range sfo.Content {
		if formatIndex < len(sfo.Format) && sfo.Format[formatIndex] == i {
			obj := stack.Pop()
			conv, err := obj.(runtime.Convertible).Convert(ctx, types.TypeString)
			if err != nil {
				return []rune{}
			}
			result = append(result, []rune(conv.(StringObject))...)
			formatIndex++
		}
		result = append(result, c)
	}
	if formatIndex < len(sfo.Format) {
		obj := stack.Pop()
		conv, err := obj.(runtime.Convertible).Convert(ctx, types.TypeString)
		if err != nil {
			return []rune{}
		}
		result = append(result, []rune(conv.(StringObject))...)
	}
	return result
}

func (sfo StringFormatObject) Describe(ctx *runtime.Context) string {
	return fmt.Sprintf("<str_fmt contents=\"%s\", positions=%v>", strings.ReplaceAll(string(sfo.Content), "\n", "\\n"), sfo.Format)
}

func (sfo StringFormatObject) Type() types.ID {
	return types.TypeString
}

type StringIterator struct {
	str *StringObject
	at  int
}

func (l *StringIterator) Describe(ctx *runtime.Context) string {
	return "string_iter"
}

func (l *StringIterator) Type() types.ID {
	return types.TypeRange
}

func (l *StringIterator) Next(ctx *runtime.Context) runtime.Object {
	l.at++
	if l.at >= len(*l.str) {
		return None
	}
	return StringObject([]rune{(*l.str)[l.at]})
}
