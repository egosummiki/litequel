package model

import (
	"fmt"

	"gitlab.com/egosummiki/litequel/runtime"
	"gitlab.com/egosummiki/litequel/types"
)

type MapObject map[string]runtime.Object

func (mo MapObject) Type() types.ID {
	return types.TypeMap
}

func (mo MapObject) Describe(ctx *runtime.Context) string {
	return "<map>"
}

func (mo MapObject) Convert(ctx *runtime.Context, typeID types.ID) (runtime.Object, *runtime.Error) {
	switch typeID {
	case types.TypeString:
		if len(mo) == 0 {
			return StringObject("map{}"), nil
		}
		first := true
		result := "map{"
		for key, value := range mo {
			if first {
				first = false
				result += fmt.Sprintf("\"%s\": %s", key, representObject(ctx, value))
				continue
			}
			result += fmt.Sprintf(", \"%s\": %s", key, representObject(ctx, value))
		}
		return StringObject(result + "}"), nil
	case types.TypeMap:
		return mo, nil
	default:
		details := ctx.Reflector().GetDetails(typeID)
		if details.TypeVariant() == types.VariantMap {
			return mo, nil
		}
	}
	return None, ctx.Errorf("map cannot be converted to type %d", typeID)
}

func (mo MapObject) Index(ctx *runtime.Context, index runtime.Object) (runtime.Object, *runtime.Error) {
	if str, ok := index.(StringObject); ok {
		if value, ok := mo[string(str)]; ok {
			return value, nil
		}
		return None, ctx.Errorf("key does not exists")
	}

	if con, ok := index.(runtime.Convertible); ok {
		con.Convert(ctx, types.TypeString)
	}

	return None, ctx.Error("invalid index type")
}

func (mo MapObject) SetIndex(ctx *runtime.Context, index runtime.Object, value runtime.Object) *runtime.Error {
	if str, ok := index.(StringObject); ok {
		mo[string(str)] = value
		return nil
	}
	return ctx.Error("invalid index type")
}
