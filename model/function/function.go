package function

import (
	"fmt"

	"gitlab.com/egosummiki/litequel/model"
	"gitlab.com/egosummiki/litequel/runtime"
	"gitlab.com/egosummiki/litequel/types"
)

type Function struct {
	BucketID int
	NumArgs  int
	Locals   int
}

func (f Function) Type() types.ID {
	return types.TypeFunction
}

func (f Function) Describe(ctx *runtime.Context) string {
	return "<userfunc>"
}

func (f Function) Call(ctx *runtime.Context, numArgs int) *runtime.Error {
	if f.NumArgs != numArgs {
		return ctx.Error("function call: invalid number of arguments")
	}
	stack := ctx.Stack()
	stack.PushFrame(numArgs)
	err := ctx.Run(f.BucketID)
	if err != nil {
		stack.ClearFrame()
		stack.PopFrame()
		stack.Push(model.None) // Push dummy return value
		return err
	}
	stack.PopFrame()
	returnValue := stack.Pop()
	stack.SlideBack(numArgs + f.Locals + 1)
	stack.Push(returnValue)
	return nil
}

type VariadicFunc Function

func (f VariadicFunc) Type() types.ID {
	return types.TypeFunction
}

func (f VariadicFunc) Describe(ctx *runtime.Context) string {
	return "<userfunc_variadic>"
}

func (f VariadicFunc) Call(ctx *runtime.Context, numArgs int) *runtime.Error {
	// last arg is variadic
	// n-1 = non variadic arguments
	variadicNum := numArgs - f.NumArgs + 1
	if variadicNum < 0 {
		return ctx.Error("function call: invalid number of arguments")
	}
	stack := ctx.Stack()
	vargs := make(model.ListObject, variadicNum)
	for i := 1; i <= variadicNum; i++ {
		vargs[variadicNum-i] = stack.Pop()
	}
	stack.Push(&vargs)
	stack.PushFrame(f.NumArgs)
	err := ctx.Run(f.BucketID)
	if err != nil {
		stack.ClearFrame()
		stack.PopFrame()
		stack.Push(model.None) // Push dummy return value
		return err
	}
	stack.PopFrame()
	returnValue := stack.Pop()
	stack.SlideBack(f.NumArgs + f.Locals + 1)
	stack.Push(returnValue)
	return nil
}

type Method struct {
	BucketID int
	NumArgs  int
	Locals   int
}

func (f Method) Type() types.ID {
	return types.TypeFunction
}

func (f Method) Describe(ctx *runtime.Context) string {
	return fmt.Sprintf("<userfunc>")
}

func (f Method) Call(ctx *runtime.Context, numArgs int) *runtime.Error {
	if f.NumArgs != numArgs {
		panic("function call: invalid number of arguments")
	}
	stack := ctx.Stack()
	stack.PushFrame(numArgs + 2)
	err := ctx.Run(f.BucketID)
	if err != nil {
		stack.ClearFrame()
		stack.PopFrame()
		stack.Push(model.None) // Push dummy return value
		return err
	}
	stack.PopFrame()
	returnValue := stack.Pop()
	stack.SlideBack(numArgs + f.Locals + 2)
	stack.Push(returnValue)
	return nil
}

type GoFunction struct {
	Name     string
	Callback func([]runtime.Object) runtime.Object
}

func (f GoFunction) Type() types.ID {
	return types.TypeFunction
}

func (f GoFunction) Describe(ctx *runtime.Context) string {
	return f.Name
}

func (f GoFunction) Call(ctx *runtime.Context, numArgs int) *runtime.Error {
	stack := ctx.Stack()
	args := make([]runtime.Object, numArgs)
	for i := numArgs - 1; i >= 0; i-- {
		args[i] = stack.Pop()
	}
	stack.Push(f.Callback(args))
	return nil
}
