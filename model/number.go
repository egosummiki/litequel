package model

import (
	"fmt"
	"strconv"

	"gitlab.com/egosummiki/litequel/runtime"
	"gitlab.com/egosummiki/litequel/types"
)

type Int8Object int8
type Int16Object int16
type Int32Object int32
type Int64Object int64
type Uint8Object uint8
type Uint16Object uint16
type Uint32Object uint32
type Uint64Object uint64
type Float32Object float32
type Float64Object float64

func NewInt32Object(content string) Int32Object {
	num, _ := strconv.Atoi(content)
	return Int32Object(num)
}
func NewFloat32Object(content string) Float32Object {
	num, _ := strconv.ParseFloat(content, 32)
	return Float32Object(num)
}

func (i Int8Object) Type() types.ID {
	return types.TypeInt8
}
func (i Int16Object) Type() types.ID {
	return types.TypeInt16
}
func (i Int32Object) Type() types.ID {
	return types.TypeInt32
}
func (i Int64Object) Type() types.ID {
	return types.TypeInt16
}
func (i Uint8Object) Type() types.ID {
	return types.TypeUint8
}
func (i Uint16Object) Type() types.ID {
	return types.TypeUint16
}
func (i Uint32Object) Type() types.ID {
	return types.TypeUint32
}
func (i Uint64Object) Type() types.ID {
	return types.TypeUint16
}
func (i Float32Object) Type() types.ID {
	return types.TypeFloat32
}
func (i Float64Object) Type() types.ID {
	return types.TypeFloat64
}

func (i Int8Object) Describe(ctx *runtime.Context) string {
	return fmt.Sprintf("<int8 value=%d>", i)
}
func (i Int16Object) Describe(ctx *runtime.Context) string {
	return fmt.Sprintf("<int16 value=%d>", i)
}
func (i Int32Object) Describe(ctx *runtime.Context) string {
	return fmt.Sprintf("<int32 value=%d>", i)
}
func (i Int64Object) Describe(ctx *runtime.Context) string {
	return fmt.Sprintf("<int64 value=%d>", i)
}
func (i Uint8Object) Describe(ctx *runtime.Context) string {
	return fmt.Sprintf("<uint8 value=%d>", i)
}
func (i Uint16Object) Describe(ctx *runtime.Context) string {
	return fmt.Sprintf("<uint16 value=%d>", i)
}
func (i Uint32Object) Describe(ctx *runtime.Context) string {
	return fmt.Sprintf("<uint32 value=%d>", i)
}
func (i Uint64Object) Describe(ctx *runtime.Context) string {
	return fmt.Sprintf("<uint64 value=%d>", i)
}
func (i Float32Object) Describe(ctx *runtime.Context) string {
	return fmt.Sprintf("<float32 value=%f>", i)
}
func (i Float64Object) Describe(ctx *runtime.Context) string {
	return fmt.Sprintf("<float64 value=%f>", i)
}

func (i Int8Object) Convert(ctx *runtime.Context, typeID types.ID) (runtime.Object, *runtime.Error) {
	switch typeID {
	case types.TypeString:
		return StringObject(strconv.Itoa(int(i))), nil
	case types.TypeInt8:
		return i, nil
	case types.TypeInt16:
		return Int16Object(i), nil
	case types.TypeInt32:
		return Int32Object(i), nil
	case types.TypeInt64:
		return Int64Object(i), nil
	case types.TypeUint8:
		return Uint8Object(i), nil
	case types.TypeUint16:
		return Uint16Object(i), nil
	case types.TypeUint32:
		return Uint32Object(i), nil
	case types.TypeUint64:
		return Uint64Object(i), nil
	case types.TypeFloat32:
		return Float32Object(i), nil
	case types.TypeFloat64:
		return Float64Object(i), nil
	}
	return None, nil
}
func (i Int16Object) Convert(ctx *runtime.Context, typeID types.ID) (runtime.Object, *runtime.Error) {
	switch typeID {
	case types.TypeString:
		return StringObject(strconv.Itoa(int(i))), nil
	case types.TypeInt8:
		return Int8Object(i), nil
	case types.TypeInt16:
		return i, nil
	case types.TypeInt32:
		return Int32Object(i), nil
	case types.TypeInt64:
		return Int64Object(i), nil
	case types.TypeUint8:
		return Uint8Object(i), nil
	case types.TypeUint16:
		return Uint16Object(i), nil
	case types.TypeUint32:
		return Uint32Object(i), nil
	case types.TypeUint64:
		return Uint64Object(i), nil
	case types.TypeFloat32:
		return Float32Object(i), nil
	case types.TypeFloat64:
		return Float64Object(i), nil
	}
	return None, nil
}
func (i Int32Object) Convert(ctx *runtime.Context, typeID types.ID) (runtime.Object, *runtime.Error) {
	switch typeID {
	case types.TypeString:
		return StringObject(strconv.Itoa(int(i))), nil
	case types.TypeInt8:
		return Int8Object(i), nil
	case types.TypeInt16:
		return Int16Object(i), nil
	case types.TypeInt32:
		return i, nil
	case types.TypeInt64:
		return Int64Object(i), nil
	case types.TypeUint8:
		return Uint8Object(i), nil
	case types.TypeUint16:
		return Uint16Object(i), nil
	case types.TypeUint32:
		return Uint32Object(i), nil
	case types.TypeUint64:
		return Uint64Object(i), nil
	case types.TypeFloat32:
		return Float32Object(i), nil
	case types.TypeFloat64:
		return Float64Object(i), nil
	}
	return None, nil
}
func (i Int64Object) Convert(ctx *runtime.Context, typeID types.ID) (runtime.Object, *runtime.Error) {
	switch typeID {
	case types.TypeString:
		return StringObject(strconv.Itoa(int(i))), nil
	case types.TypeInt8:
		return Int8Object(i), nil
	case types.TypeInt16:
		return Int16Object(i), nil
	case types.TypeInt32:
		return Int32Object(i), nil
	case types.TypeInt64:
		return i, nil
	case types.TypeUint8:
		return Uint8Object(i), nil
	case types.TypeUint16:
		return Uint16Object(i), nil
	case types.TypeUint32:
		return Uint32Object(i), nil
	case types.TypeUint64:
		return Uint64Object(i), nil
	case types.TypeFloat32:
		return Float32Object(i), nil
	case types.TypeFloat64:
		return Float64Object(i), nil
	}
	return None, nil
}
func (i Uint8Object) Convert(ctx *runtime.Context, typeID types.ID) (runtime.Object, *runtime.Error) {
	switch typeID {
	case types.TypeString:
		return StringObject(strconv.Itoa(int(i))), nil
	case types.TypeInt8:
		return Int8Object(i), nil
	case types.TypeInt16:
		return Int16Object(i), nil
	case types.TypeInt32:
		return Int32Object(i), nil
	case types.TypeInt64:
		return Int64Object(i), nil
	case types.TypeUint8:
		return i, nil
	case types.TypeUint16:
		return Uint16Object(i), nil
	case types.TypeUint32:
		return Uint32Object(i), nil
	case types.TypeUint64:
		return Uint64Object(i), nil
	case types.TypeFloat32:
		return Float32Object(i), nil
	case types.TypeFloat64:
		return Float64Object(i), nil
	}
	return None, nil
}
func (i Uint16Object) Convert(ctx *runtime.Context, typeID types.ID) (runtime.Object, *runtime.Error) {
	switch typeID {
	case types.TypeString:
		return StringObject(strconv.Itoa(int(i))), nil
	case types.TypeInt8:
		return Int8Object(i), nil
	case types.TypeInt16:
		return Int16Object(i), nil
	case types.TypeInt32:
		return Int32Object(i), nil
	case types.TypeInt64:
		return Int64Object(i), nil
	case types.TypeUint8:
		return Uint8Object(i), nil
	case types.TypeUint16:
		return i, nil
	case types.TypeUint32:
		return Uint32Object(i), nil
	case types.TypeUint64:
		return Uint64Object(i), nil
	case types.TypeFloat32:
		return Float32Object(i), nil
	case types.TypeFloat64:
		return Float64Object(i), nil
	}
	return None, nil
}
func (i Uint32Object) Convert(ctx *runtime.Context, typeID types.ID) (runtime.Object, *runtime.Error) {
	switch typeID {
	case types.TypeString:
		return StringObject(strconv.Itoa(int(i))), nil
	case types.TypeInt8:
		return Int8Object(i), nil
	case types.TypeInt16:
		return Int16Object(i), nil
	case types.TypeInt32:
		return Int32Object(i), nil
	case types.TypeInt64:
		return Int64Object(i), nil
	case types.TypeUint8:
		return Uint8Object(i), nil
	case types.TypeUint16:
		return Uint16Object(i), nil
	case types.TypeUint32:
		return i, nil
	case types.TypeUint64:
		return Uint64Object(i), nil
	case types.TypeFloat32:
		return Float32Object(i), nil
	case types.TypeFloat64:
		return Float64Object(i), nil
	}
	return None, nil
}
func (i Uint64Object) Convert(ctx *runtime.Context, typeID types.ID) (runtime.Object, *runtime.Error) {
	switch typeID {
	case types.TypeString:
		return StringObject(strconv.Itoa(int(i))), nil
	case types.TypeInt8:
		return Int8Object(i), nil
	case types.TypeInt16:
		return Int16Object(i), nil
	case types.TypeInt32:
		return Int32Object(i), nil
	case types.TypeInt64:
		return Int64Object(i), nil
	case types.TypeUint8:
		return Uint8Object(i), nil
	case types.TypeUint16:
		return Uint16Object(i), nil
	case types.TypeUint32:
		return Uint32Object(i), nil
	case types.TypeUint64:
		return i, nil
	case types.TypeFloat32:
		return Float32Object(i), nil
	case types.TypeFloat64:
		return Float64Object(i), nil
	}
	return None, nil
}
func (i Float32Object) Convert(ctx *runtime.Context, typeID types.ID) (runtime.Object, *runtime.Error) {
	switch typeID {
	case types.TypeString:
		return StringObject(strconv.FormatFloat(float64(i), 'f', 5, 32)), nil
	case types.TypeInt8:
		return Int8Object(i), nil
	case types.TypeInt16:
		return Int16Object(i), nil
	case types.TypeInt32:
		return Int32Object(i), nil
	case types.TypeInt64:
		return Int64Object(i), nil
	case types.TypeUint8:
		return Uint8Object(i), nil
	case types.TypeUint16:
		return Uint16Object(i), nil
	case types.TypeUint32:
		return Uint32Object(i), nil
	case types.TypeUint64:
		return Uint64Object(i), nil
	case types.TypeFloat32:
		return i, nil
	case types.TypeFloat64:
		return Float64Object(i), nil
	}
	return None, nil
}
func (i Float64Object) Convert(ctx *runtime.Context, typeID types.ID) (runtime.Object, *runtime.Error) {
	switch typeID {
	case types.TypeString:
		return StringObject(strconv.FormatFloat(float64(i), 'f', 5, 64)), nil
	case types.TypeInt8:
		return Int8Object(i), nil
	case types.TypeInt16:
		return Int16Object(i), nil
	case types.TypeInt32:
		return Int32Object(i), nil
	case types.TypeInt64:
		return Int64Object(i), nil
	case types.TypeUint8:
		return Uint8Object(i), nil
	case types.TypeUint16:
		return Uint16Object(i), nil
	case types.TypeUint32:
		return Uint32Object(i), nil
	case types.TypeUint64:
		return Uint64Object(i), nil
	case types.TypeFloat32:
		return Float32Object(i), nil
	case types.TypeFloat64:
		return i, nil
	}
	return None, nil
}

func (i Int8Object) Add(ctx *runtime.Context, obj runtime.Object) runtime.Object {
	conv, err := obj.(runtime.Convertible).Convert(ctx, types.TypeInt8)
	if err != nil {
		return None
	}
	return i + conv.(Int8Object)
}
func (i Int16Object) Add(ctx *runtime.Context, obj runtime.Object) runtime.Object {
	conv, err := obj.(runtime.Convertible).Convert(ctx, types.TypeInt16)
	if err != nil {
		return None
	}
	return i + conv.(Int16Object)
}
func (i Int32Object) Add(ctx *runtime.Context, obj runtime.Object) runtime.Object {
	conv, err := obj.(runtime.Convertible).Convert(ctx, types.TypeInt32)
	if err != nil {
		return None
	}
	return i + conv.(Int32Object)
}
func (i Int64Object) Add(ctx *runtime.Context, obj runtime.Object) runtime.Object {
	conv, err := obj.(runtime.Convertible).Convert(ctx, types.TypeInt64)
	if err != nil {
		return None
	}
	return i + conv.(Int64Object)
}
func (i Uint8Object) Add(ctx *runtime.Context, obj runtime.Object) runtime.Object {
	conv, err := obj.(runtime.Convertible).Convert(ctx, types.TypeUint8)
	if err != nil {
		return None
	}
	return i + conv.(Uint8Object)
}
func (i Uint16Object) Add(ctx *runtime.Context, obj runtime.Object) runtime.Object {
	conv, err := obj.(runtime.Convertible).Convert(ctx, types.TypeUint16)
	if err != nil {
		return None
	}
	return i + conv.(Uint16Object)
}
func (i Uint32Object) Add(ctx *runtime.Context, obj runtime.Object) runtime.Object {
	conv, err := obj.(runtime.Convertible).Convert(ctx, types.TypeUint32)
	if err != nil {
		return None
	}
	return i + conv.(Uint32Object)
}
func (i Uint64Object) Add(ctx *runtime.Context, obj runtime.Object) runtime.Object {
	conv, err := obj.(runtime.Convertible).Convert(ctx, types.TypeUint64)
	if err != nil {
		return None
	}
	return i + conv.(Uint64Object)
}
func (i Float32Object) Add(ctx *runtime.Context, obj runtime.Object) runtime.Object {
	conv, err := obj.(runtime.Convertible).Convert(ctx, types.TypeFloat32)
	if err != nil {
		return None
	}
	return i + conv.(Float32Object)
}
func (i Float64Object) Add(ctx *runtime.Context, obj runtime.Object) runtime.Object {
	conv, err := obj.(runtime.Convertible).Convert(ctx, types.TypeFloat64)
	if err != nil {
		return None
	}
	return i + conv.(Float64Object)
}

func (i Int8Object) Subtract(ctx *runtime.Context, obj runtime.Object) runtime.Object {
	conv, err := obj.(runtime.Convertible).Convert(ctx, types.TypeInt8)
	if err != nil {
		return None
	}
	return i - conv.(Int8Object)
}

func (i Int16Object) Subtract(ctx *runtime.Context, obj runtime.Object) runtime.Object {
	conv, err := obj.(runtime.Convertible).Convert(ctx, types.TypeInt16)
	if err != nil {
		return None
	}
	return i - conv.(Int16Object)
}
func (i Int32Object) Subtract(ctx *runtime.Context, obj runtime.Object) runtime.Object {
	conv, err := obj.(runtime.Convertible).Convert(ctx, types.TypeInt32)
	if err != nil {
		return None
	}
	return i - conv.(Int32Object)
}
func (i Int64Object) Subtract(ctx *runtime.Context, obj runtime.Object) runtime.Object {
	conv, err := obj.(runtime.Convertible).Convert(ctx, types.TypeInt64)
	if err != nil {
		return None
	}
	return i - conv.(Int64Object)
}
func (i Uint8Object) Subtract(ctx *runtime.Context, obj runtime.Object) runtime.Object {
	conv, err := obj.(runtime.Convertible).Convert(ctx, types.TypeUint8)
	if err != nil {
		return None
	}
	return i - conv.(Uint8Object)
}
func (i Uint16Object) Subtract(ctx *runtime.Context, obj runtime.Object) runtime.Object {
	conv, err := obj.(runtime.Convertible).Convert(ctx, types.TypeUint16)
	if err != nil {
		return None
	}
	return i - conv.(Uint16Object)
}
func (i Uint32Object) Subtract(ctx *runtime.Context, obj runtime.Object) runtime.Object {
	conv, err := obj.(runtime.Convertible).Convert(ctx, types.TypeUint32)
	if err != nil {
		return None
	}
	return i - conv.(Uint32Object)
}
func (i Uint64Object) Subtract(ctx *runtime.Context, obj runtime.Object) runtime.Object {
	conv, err := obj.(runtime.Convertible).Convert(ctx, types.TypeUint64)
	if err != nil {
		return None
	}
	return i - conv.(Uint64Object)
}
func (i Float32Object) Subtract(ctx *runtime.Context, obj runtime.Object) runtime.Object {
	conv, err := obj.(runtime.Convertible).Convert(ctx, types.TypeFloat32)
	if err != nil {
		return None
	}
	return i - conv.(Float32Object)
}
func (i Float64Object) Subtract(ctx *runtime.Context, obj runtime.Object) runtime.Object {
	conv, err := obj.(runtime.Convertible).Convert(ctx, types.TypeFloat64)
	if err != nil {
		return None
	}
	return i - conv.(Float64Object)
}
func (i Int8Object) Multiply(ctx *runtime.Context, obj runtime.Object) runtime.Object {
	conv, err := obj.(runtime.Convertible).Convert(ctx, types.TypeInt8)
	if err != nil {
		return None
	}
	return i * conv.(Int8Object)
}
func (i Int16Object) Multiply(ctx *runtime.Context, obj runtime.Object) runtime.Object {
	conv, err := obj.(runtime.Convertible).Convert(ctx, types.TypeInt16)
	if err != nil {
		return None
	}
	return i * conv.(Int16Object)
}
func (i Int32Object) Multiply(ctx *runtime.Context, obj runtime.Object) runtime.Object {
	conv, err := obj.(runtime.Convertible).Convert(ctx, types.TypeInt32)
	if err != nil {
		return None
	}
	return i * conv.(Int32Object)
}
func (i Int64Object) Multiply(ctx *runtime.Context, obj runtime.Object) runtime.Object {
	conv, err := obj.(runtime.Convertible).Convert(ctx, types.TypeInt64)
	if err != nil {
		return None
	}
	return i * conv.(Int64Object)
}
func (i Uint8Object) Multiply(ctx *runtime.Context, obj runtime.Object) runtime.Object {
	conv, err := obj.(runtime.Convertible).Convert(ctx, types.TypeUint8)
	if err != nil {
		return None
	}
	return i * conv.(Uint8Object)
}
func (i Uint16Object) Multiply(ctx *runtime.Context, obj runtime.Object) runtime.Object {
	conv, err := obj.(runtime.Convertible).Convert(ctx, types.TypeUint16)
	if err != nil {
		return None
	}
	return i * conv.(Uint16Object)
}
func (i Uint32Object) Multiply(ctx *runtime.Context, obj runtime.Object) runtime.Object {
	conv, err := obj.(runtime.Convertible).Convert(ctx, types.TypeUint32)
	if err != nil {
		return None
	}
	return i * conv.(Uint32Object)
}
func (i Uint64Object) Multiply(ctx *runtime.Context, obj runtime.Object) runtime.Object {
	conv, err := obj.(runtime.Convertible).Convert(ctx, types.TypeUint64)
	if err != nil {
		return None
	}
	return i * conv.(Uint64Object)
}
func (i Float32Object) Multiply(ctx *runtime.Context, obj runtime.Object) runtime.Object {
	conv, err := obj.(runtime.Convertible).Convert(ctx, types.TypeFloat32)
	if err != nil {
		return None
	}
	return i * conv.(Float32Object)
}
func (i Float64Object) Multiply(ctx *runtime.Context, obj runtime.Object) runtime.Object {
	conv, err := obj.(runtime.Convertible).Convert(ctx, types.TypeFloat64)
	if err != nil {
		return None
	}
	return i * conv.(Float64Object)
}
func (i Int8Object) Divide(ctx *runtime.Context, obj runtime.Object) runtime.Object {
	conv, err := obj.(runtime.Convertible).Convert(ctx, types.TypeInt8)
	if err != nil {
		return None
	}
	return i / conv.(Int8Object)
}
func (i Int16Object) Divide(ctx *runtime.Context, obj runtime.Object) runtime.Object {
	conv, err := obj.(runtime.Convertible).Convert(ctx, types.TypeInt16)
	if err != nil {
		return None
	}
	return i / conv.(Int16Object)
}
func (i Int32Object) Divide(ctx *runtime.Context, obj runtime.Object) runtime.Object {
	conv, err := obj.(runtime.Convertible).Convert(ctx, types.TypeInt32)
	if err != nil {
		return None
	}
	return i / conv.(Int32Object)
}
func (i Int64Object) Divide(ctx *runtime.Context, obj runtime.Object) runtime.Object {
	conv, err := obj.(runtime.Convertible).Convert(ctx, types.TypeInt64)
	if err != nil {
		return None
	}
	return i / conv.(Int64Object)
}
func (i Uint8Object) Divide(ctx *runtime.Context, obj runtime.Object) runtime.Object {
	conv, err := obj.(runtime.Convertible).Convert(ctx, types.TypeUint8)
	if err != nil {
		return None
	}
	return i / conv.(Uint8Object)
}
func (i Uint16Object) Divide(ctx *runtime.Context, obj runtime.Object) runtime.Object {
	conv, err := obj.(runtime.Convertible).Convert(ctx, types.TypeUint16)
	if err != nil {
		return None
	}
	return i / conv.(Uint16Object)
}
func (i Uint32Object) Divide(ctx *runtime.Context, obj runtime.Object) runtime.Object {
	conv, err := obj.(runtime.Convertible).Convert(ctx, types.TypeUint32)
	if err != nil {
		return None
	}
	return i / conv.(Uint32Object)
}
func (i Uint64Object) Divide(ctx *runtime.Context, obj runtime.Object) runtime.Object {
	conv, err := obj.(runtime.Convertible).Convert(ctx, types.TypeUint64)
	if err != nil {
		return None
	}
	return i / conv.(Uint64Object)
}
func (i Float32Object) Divide(ctx *runtime.Context, obj runtime.Object) runtime.Object {
	conv, err := obj.(runtime.Convertible).Convert(ctx, types.TypeFloat32)
	if err != nil {
		return None
	}
	return i / conv.(Float32Object)
}
func (i Float64Object) Divide(ctx *runtime.Context, obj runtime.Object) runtime.Object {
	conv, err := obj.(runtime.Convertible).Convert(ctx, types.TypeFloat64)
	if err != nil {
		return None
	}
	return i / conv.(Float64Object)
}
func (i Int8Object) Modulo(ctx *runtime.Context, obj runtime.Object) runtime.Object {
	conv, err := obj.(runtime.Convertible).Convert(ctx, types.TypeInt8)
	if err != nil {
		return None
	}
	return i % conv.(Int8Object)
}
func (i Int16Object) Modulo(ctx *runtime.Context, obj runtime.Object) runtime.Object {
	conv, err := obj.(runtime.Convertible).Convert(ctx, types.TypeInt16)
	if err != nil {
		return None
	}
	return i % conv.(Int16Object)
}
func (i Int32Object) Modulo(ctx *runtime.Context, obj runtime.Object) runtime.Object {
	conv, err := obj.(runtime.Convertible).Convert(ctx, types.TypeInt32)
	if err != nil {
		return None
	}
	return i % conv.(Int32Object)
}
func (i Int64Object) Modulo(ctx *runtime.Context, obj runtime.Object) runtime.Object {
	conv, err := obj.(runtime.Convertible).Convert(ctx, types.TypeInt64)
	if err != nil {
		return None
	}
	return i % conv.(Int64Object)
}
func (i Uint8Object) Modulo(ctx *runtime.Context, obj runtime.Object) runtime.Object {
	conv, err := obj.(runtime.Convertible).Convert(ctx, types.TypeUint8)
	if err != nil {
		return None
	}
	return i % conv.(Uint8Object)
}
func (i Uint16Object) Modulo(ctx *runtime.Context, obj runtime.Object) runtime.Object {
	conv, err := obj.(runtime.Convertible).Convert(ctx, types.TypeUint16)
	if err != nil {
		return None
	}
	return i % conv.(Uint16Object)
}
func (i Uint32Object) Modulo(ctx *runtime.Context, obj runtime.Object) runtime.Object {
	conv, err := obj.(runtime.Convertible).Convert(ctx, types.TypeUint32)
	if err != nil {
		return None
	}
	return i % conv.(Uint32Object)
}
func (i Uint64Object) Modulo(ctx *runtime.Context, obj runtime.Object) runtime.Object {
	conv, err := obj.(runtime.Convertible).Convert(ctx, types.TypeUint64)
	if err != nil {
		return None
	}
	return i % conv.(Uint64Object)
}

func (i Int32Object) ToInt() int {
	return int(i)
}
func (i Float32Object) ToInt() int {
	return int(i)
}

func (i Int32Object) Iter(ctx *runtime.Context) runtime.Iterator {
	return NewRange(-1, int(i))
}

func (i Int8Object) Compare(ctx *runtime.Context, other runtime.Object) (int, *runtime.Error) {
	conv, err := other.(runtime.Convertible).Convert(ctx, types.TypeInt32)
	if err != nil {
		return 0, err
	}
	return int(i - conv.(Int8Object)), nil
}
func (i Int16Object) Compare(ctx *runtime.Context, other runtime.Object) (int, *runtime.Error) {
	conv, err := other.(runtime.Convertible).Convert(ctx, types.TypeInt16)
	if err != nil {
		return 0, err
	}
	return int(i - conv.(Int16Object)), nil
}
func (i Int32Object) Compare(ctx *runtime.Context, other runtime.Object) (int, *runtime.Error) {
	conv, err := other.(runtime.Convertible).Convert(ctx, types.TypeInt32)
	if err != nil {
		return 0, err
	}
	return int(i - conv.(Int32Object)), nil
}
func (i Int64Object) Compare(ctx *runtime.Context, other runtime.Object) (int, *runtime.Error) {
	conv, err := other.(runtime.Convertible).Convert(ctx, types.TypeInt64)
	if err != nil {
		return 0, err
	}
	return int(i - conv.(Int64Object)), nil
}
func (i Float32Object) Compare(ctx *runtime.Context, other runtime.Object) (int, *runtime.Error) {
	conv, err := other.(runtime.Convertible).Convert(ctx, types.TypeFloat32)
	if err != nil {
		return 0, err
	}
	return int(i - conv.(Float32Object)), nil
}
func (i Float64Object) Compare(ctx *runtime.Context, other runtime.Object) (int, *runtime.Error) {
	conv, err := other.(runtime.Convertible).Convert(ctx, types.TypeFloat64)
	if err != nil {
		return 0, err
	}
	return int(i - conv.(Float64Object)), nil
}

