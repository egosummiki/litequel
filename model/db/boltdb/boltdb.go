package boltdb

import (
	"gitlab.com/egosummiki/litequel/model"
	"gitlab.com/egosummiki/litequel/runtime"
	"gitlab.com/egosummiki/litequel/types"
)

type LoadBoltDBFunc struct{}

func (f LoadBoltDBFunc) Call(ctx *runtime.Context, numArgs int) *runtime.Error {
	if numArgs != 1 {
		return nil
	}
	stack := ctx.Stack()

	filename, ok := stack.Pop().(model.StringObject)
	if !ok {
		return nil
	}

	_ = filename
	return nil
}

func (f LoadBoltDBFunc) Describe(ctx *runtime.Context) string {
	return "<bolddbload>"
}

func (f LoadBoltDBFunc) Type() types.ID {
	return types.TypeFunction
}
