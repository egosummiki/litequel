package model

import (
	"fmt"
	"sort"

	"gitlab.com/egosummiki/litequel/runtime"
	"gitlab.com/egosummiki/litequel/types"
	"gitlab.com/egosummiki/litequel/utils"
)

type ListObject []runtime.Object

func (l *ListObject) Describe(ctx *runtime.Context) string {
	return fmt.Sprintf("<list len=%d>", len(*l))
}

func representObject(ctx *runtime.Context, object runtime.Object) string {
	if str, ok := object.(StringObject); ok {
		return fmt.Sprintf("\"%s\"", string(str))
	}
	conv, err := object.(runtime.Convertible).Convert(ctx, types.TypeString)
	if err != nil {
		return "###INVALID###"
	}
	return string(conv.(StringObject))
}

func (l *ListObject) Convert(ctx *runtime.Context, typeID types.ID) (runtime.Object, *runtime.Error) {
	if typeID == types.TypeString {
		if len(*l) == 0 {
			return StringObject("<empty_list>"), nil
		}
		first := (*l)[0] //
		if ud, ok := first.(*UserDefinedObject); ok {
			reflect := ctx.Reflector()
			fields := reflect.GetTypeAttribs(ud.Type())

			var labels []string
			for _, f := range fields {
				labels = append(labels, f.Name)
			}

			table := utils.NewTable(labels...)
			for _, value := range *l {
				var rows []string
				for _, d := range value.(*UserDefinedObject).Data {
					if c, ok := d.(runtime.Convertible); ok {
						conv, err := c.Convert(ctx, types.TypeString)
						if err != nil {
							return nil, err
						}
						rows = append(rows, string(conv.(StringObject)))
						continue
					}
					rows = append(rows, "<unknown>")
				}
				table.AddRow(rows...)
			}
			return StringObject(table.Print()), nil
		}
		result := fmt.Sprintf("[%s", representObject(ctx, (*l)[0]))
		for _, value := range (*l)[1:] {
			result += fmt.Sprintf(", %s", representObject(ctx, value))
		}
		return StringObject(result + "]"), nil
	}
	return l, nil
}

func (l *ListObject) Type() types.ID {
	return types.TypeArray
}

func (l *ListObject) Append(ctx *runtime.Context, obj runtime.Object) *runtime.Error {
	*l = append(*l, obj)
	return nil
}

func (l *ListObject) Iter(ctx *runtime.Context) runtime.Iterator {
	return &ListIterator{list: l, at: -1}
}

func (l *ListObject) Index(ctx *runtime.Context, index runtime.Object) (runtime.Object, *runtime.Error) {
	if num, ok := index.(runtime.Convertible); ok {
		conv, err := num.Convert(ctx, types.TypeInt32)
		if err != nil {
			return nil, err
		}
		index := int(conv.(Int32Object))
		if index >= len(*l) {
			return nil, ctx.Error("index out of range")
		}
		if index < 0 {
			index += len(*l)
			if index < 0 {
				return nil, ctx.Error("index out of range")
			}
		}
		return (*l)[index], nil
	}
	if r, ok := index.(*Range); ok {
		return &SliceObject{list: l, Min: r.beg, Max: r.end, at: -1}, nil
	}

	return None, nil
}

func (l *ListObject) SetIndex(ctx *runtime.Context, index runtime.Object, value runtime.Object) *runtime.Error {
	if num, ok := index.(runtime.Number); ok {
		(*l)[num.ToInt()] = value
	}
	return nil
}

func (l *ListObject) GetAttribute(ctx *runtime.Context, index int) runtime.Object {
	/*
		0 - len
		1 - rem
	*/
	switch index {
	case 0:
		return Int32Object(len(*l))
	case 1:
		return ListRemove{}
	}

	return None
}

func (l *ListObject) Filter(ctx *runtime.Context, condition runtime.Callable) (runtime.Object, *runtime.Error) {
	var filtered ListObject
	stack := ctx.Stack()
	for _, obj := range *l {
		stack.Push(condition.(runtime.Object))
		stack.Push(obj)
		if userDef, ok := obj.(*UserDefinedObject); ok {
			for _, prop := range userDef.Data {
				stack.Push(prop)
			}
			if err := condition.Call(ctx, len(userDef.Data)+1); err != nil {
				return None, nil
			}
		} else {
			if err := condition.Call(ctx, 1); err != nil {
				return None, nil
			}
		}
		result := stack.Pop()
		resultInt, ok := result.(Int32Object)
		if !ok {
			return nil, nil
		}
		if int(resultInt) != 0 {
			filtered = append(filtered, obj)
		}
	}
	return &filtered, nil
}

func (l *ListObject) Project(ctx *runtime.Context, projectionFunc runtime.Callable) (runtime.Object, *runtime.Error) {
	var projection ListObject
	stack := ctx.Stack()
	for _, obj := range *l {
		stack.Push(projectionFunc.(runtime.Object))
		stack.Push(obj)
		userDef, ok := obj.(*UserDefinedObject)
		if ok {
			for _, prop := range userDef.Data {
				stack.Push(prop)
			}
			err := projectionFunc.Call(ctx, len(userDef.Data)+1)
			if err != nil {
				return nil, err
			}
		} else {
			err := projectionFunc.Call(ctx, 1)
			if err != nil {
				return nil, err
			}
		}
		projection = append(projection, stack.Pop())
	}
	return &projection, nil
}

type listSorter struct {
	list *ListObject
	prop int
	desc bool
	ctx  *runtime.Context //sorry
}

func (l listSorter) Len() int {
	return len(*l.list)
}

func (l listSorter) Less(i, j int) bool {
	res, err := (*l.list)[i].(*UserDefinedObject).GetAttribute(l.ctx, l.prop).(runtime.Comparable).Compare(l.ctx, (*l.list)[j].(*UserDefinedObject).GetAttribute(l.ctx, l.prop))
	if err != nil {
		return false
	}

	return l.desc != (res < 0)
}

func (l listSorter) Swap(i, j int) {
	(*l.list)[i], (*l.list)[j] = (*l.list)[j], (*l.list)[i]
}

func (l *ListObject) Sort(ctx *runtime.Context, prop int, desc bool) (runtime.Object, *runtime.Error) {
	sort.Sort(listSorter{
		list: l,
		prop: prop,
		desc: desc,
		ctx:  ctx,
	})
	return l, nil
}


type ListIterator struct {
	list *ListObject
	at   int
}

func (l *ListIterator) Describe(ctx *runtime.Context) string {
	return "list_iter"
}

func (l *ListIterator) Type() types.ID {
	return types.TypeRange
}

func (l *ListIterator) Next(ctx *runtime.Context) runtime.Object {
	l.at++
	if l.at >= len(*l.list) {
		return None
	}
	return (*l.list)[l.at]
}

type SliceObject struct {
	list *ListObject
	Min  int
	Max  int
	at   int
}

func (l *SliceObject) Describe(ctx *runtime.Context) string {
	return "slice"
}

func (l *SliceObject) Type() types.ID {
	return types.TypeRange
}

func (l *SliceObject) Index(ctx *runtime.Context, index runtime.Object) (runtime.Object, *runtime.Error) {
	if num, ok := index.(runtime.Number); ok {
		return (*l.list)[l.Min+num.ToInt()], nil
	}
	if r, ok := index.(*Range); ok {
		slice := SliceObject{l.list, l.Min + r.beg, l.Min + r.end, -1}
		return &slice, nil
	}

	return None, nil
}

func (l *SliceObject) SetIndex(ctx *runtime.Context, index runtime.Object, value runtime.Object) *runtime.Error {
	if num, ok := index.(runtime.Number); ok {
		(*l.list)[l.Min+num.ToInt()] = value
	}
	return nil
}

func (l *SliceObject) Iter(ctx *runtime.Context) runtime.Iterator {
	l.at = -1
	return l
}

func (l *SliceObject) Next(ctx *runtime.Context) runtime.Object {
	l.at++
	index := l.Min + l.at
	if index >= l.Max {
		return None
	}
	return (*l.list)[index]
}

type ListRemove struct {
}

func (fn ListRemove) Describe(ctx *runtime.Context) string {
	return "<func rem>"
}

func (fn ListRemove) Type() types.ID {
	return types.TypeFunction
}

func (fn ListRemove) Call(ctx *runtime.Context, numArgs int) *runtime.Error {
	if numArgs != 1 {
		panic("Wrong number of arguments")
	}

	stack := ctx.Stack()
	index := stack.Pop().(Int32Object)
	stack.Pop()
	this := stack.Pop().(*ListObject)

	*this = append((*this)[:index], (*this)[index+1:]...)
	stack.Push(None)
	return nil
}
