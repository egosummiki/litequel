package model

import (
	"gitlab.com/egosummiki/litequel/runtime"
	"gitlab.com/egosummiki/litequel/types"
)

type Error struct {
	*runtime.Error
}

func (e *Error) Describe(ctx *runtime.Context) string {
	return e.Error.Error()
}

func (e *Error) Type() types.ID {
	return types.TypeError
}

func (e *Error) Convert(ctx *runtime.Context, typeID types.ID) (runtime.Object, *runtime.Error) {
	switch typeID {
	case types.TypeString:
		return StringObject(e.Error.Error()), nil
	}
	return None, nil
}

func (e *Error) GetAttribute(ctx *runtime.Context, index int) runtime.Object {
	switch index {
	case 0:
		return StringObject(e.Error.Msg())
	case 1:
		return Int32Object(e.Error.Line())
	case 2:
		return Int32Object(e.Error.Col())
	case 3:
		return StringObject(e.Error.Class())
	}
	return None
}
