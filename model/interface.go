package model

import (
	"fmt"

	"gitlab.com/egosummiki/litequel/runtime"
	"gitlab.com/egosummiki/litequel/types"
)

type InterfaceObject struct {
	UnderlyingObj runtime.Object
	ObjType       types.ID
	InterfaceType types.ID
	CallMapping   map[int]int
}

func (i InterfaceObject) Describe(ctx *runtime.Context) string {
	return fmt.Sprintf("<interface: %s>", i.UnderlyingObj.Describe(ctx))
}

func (i InterfaceObject) Type() types.ID {
	return i.InterfaceType
}

type InterfaceMethod struct {
	MethodId int
}

func (i InterfaceMethod) Call(ctx *runtime.Context, numArgs int) *runtime.Error {
	stack := ctx.Stack()
	args := make([]runtime.Object, numArgs)
	for i := numArgs - 1; i >= 0; i-- {
		args[i] = stack.Pop()
	}
	_ = stack.Pop()
	obj := stack.Pop()
	ifaceObj, ok := obj.(InterfaceObject)
	if !ok {
		return ctx.Error("interface method called on non interface object")
	}

	underObj := ctx.GetGlobal(ifaceObj.CallMapping[i.MethodId])
	underCallable, ok := underObj.(runtime.Callable)
	if !ok {
		return ctx.Error("method of an underlying object is not callable")
	}

	stack.Push(ifaceObj.UnderlyingObj)
	stack.Push(underObj)
	for _, arg := range args {
		stack.Push(arg)
	}

	if err := underCallable.Call(ctx, numArgs); err != nil {
		return err
	}

	return nil
}

func (i InterfaceMethod) Describe(ctx *runtime.Context) string {
	return "<iface method>"
}

func (i InterfaceMethod) Type() types.ID {
	return types.TypeUnknown
}
