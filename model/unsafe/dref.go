package unsafe

import (
	"gitlab.com/egosummiki/litequel/model"
	"gitlab.com/egosummiki/litequel/runtime"
	"gitlab.com/egosummiki/litequel/types"
)

type DRefFunc struct{
}

func (d DRefFunc) Describe(ctx *runtime.Context) string {
	return "<func dref>"
}

func (d DRefFunc) Type() types.ID {
	return types.TypeFunction
}

func (d DRefFunc) Call(ctx *runtime.Context, numArgs int) *runtime.Error {
	if numArgs != 2 {
		return ctx.Error("invalid number of arguments")
	}

	stack := ctx.Stack()
	ptr, ok := stack.Pop().(Pointer)
	if !ok {
		return ctx.Error("dref invalid type")
	}

	tp, ok := stack.Pop().(model.Int32Object)
	if !ok {
		return ctx.Error("dref invalid type")
	}

	stack.Push(DRef(ctx, types.ID(tp), ptr))
	return nil
}
