package unsafe

import (
	"fmt"
	"reflect"
	"unsafe"

	"gitlab.com/egosummiki/litequel/model"
	"gitlab.com/egosummiki/litequel/runtime"
	"gitlab.com/egosummiki/litequel/types"
)

type Pointer uintptr

func (p Pointer) Describe(ctx *runtime.Context) string {
	return fmt.Sprintf("<ptr %d>", p)
}

func (p Pointer) Type() types.ID {
	return types.TypePointer
}

func Ref(ctx *runtime.Context, obj runtime.Object) (Pointer, *runtime.Error) {
	v := reflect.ValueOf(obj)
	if v.Kind() != reflect.Ptr && v.Kind() != reflect.Slice {
		return 0, ctx.Error("cannot reference this object")
	}
	return Pointer(v.Pointer()), nil
}

func DRef(ctx *runtime.Context, typeID types.ID, ptr Pointer) runtime.Object {
	switch typeID {
	case types.TypeInt32:
		return drefInt32(ptr)
	}
	variant := ctx.Reflector().GetDetails(typeID).TypeVariant()
	if variant == types.VariantStruct {
		return drefUserDef(ptr)
	}
	return model.None
}

func drefInt32(ptr Pointer) runtime.Object {
	return *(*model.Int32Object)(unsafe.Pointer(ptr))
}

func drefUserDef(ptr Pointer) runtime.Object {
	return (*model.UserDefinedObject)(unsafe.Pointer(ptr))
}
