package model

import (
	"fmt"

	"gitlab.com/egosummiki/litequel/runtime"
	"gitlab.com/egosummiki/litequel/types"
)

type Range struct {
	beg, at, end int
}

func NewRange(min, max int) *Range {
	return &Range{beg: min, end: max, at: min}
}

func (r *Range) Iter(ctx *runtime.Context) runtime.Iterator {
	r.at = r.beg - 1
	return r
}

func (r *Range) Next(ctx *runtime.Context) runtime.Object {
	r.at++
	if r.at < r.end {
		return Int32Object(r.at)
	}
	return None
}

func (l *Range) GetAttribute(ctx *runtime.Context, index int) runtime.Object {
	/*
		0 - len
		1 - min
		2 - max
	*/
	switch index {
	case 0:
		return Int32Object(l.end - l.beg)
	case 1:
		return Int32Object(l.beg)
	case 2:
		return Int32Object(l.end)
	}

	return None
}

func (r *Range) Describe(ctx *runtime.Context) string {
	return fmt.Sprintf("<range beg=%d end=%d>", r.beg, r.end)
}

func (i *Range) Type() types.ID {
	return types.TypeRange
}

func (r *Range) Index(ctx *runtime.Context, index runtime.Object) (runtime.Object, *runtime.Error) {
	return Int32Object(index.(runtime.Number).ToInt() + r.beg), nil
}

func (r *Range) Project(ctx *runtime.Context, projectionFunc runtime.Callable) (runtime.Object, *runtime.Error) {
	var projection ListObject
	stack := ctx.Stack()
	for i := r.beg; i < r.end; i++ {
		stack.Push(projectionFunc.(runtime.Object))
		stack.Push(Int32Object(i))
		projectionFunc.Call(ctx, 1)
		projection = append(projection, stack.Pop())
	}
	return &projection, nil
}

func (r *Range) Size() int {
	return r.end - r.beg
}