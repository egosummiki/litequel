package model

type SymbolType byte
type Scope byte

const (
	NoSymbol       SymbolType = 0
	SymbolVariable SymbolType = 1
	SymbolFunction SymbolType = 2
	SymbolClass    SymbolType = 3
)

const (
	UnknownScope  Scope = 0
	ScopeGlobal   Scope = 1
	ScopeLocal    Scope = 2
	ScopeArgument Scope = 3
	ScopeExport   Scope = 4
)

type Symbol struct {
	Kind  SymbolType
	Scope Scope
}
