package json

import (
	"bytes"
	"fmt"
	"strconv"

	"gitlab.com/egosummiki/litequel/lexer"
	"gitlab.com/egosummiki/litequel/model"
	"gitlab.com/egosummiki/litequel/runtime"
	"gitlab.com/egosummiki/litequel/symbols"
	"gitlab.com/egosummiki/litequel/symbols/scope"
	"gitlab.com/egosummiki/litequel/tokens"
	"gitlab.com/egosummiki/litequel/types"
)

func EncodeObject(ctx *runtime.Context, obj runtime.Object) string {
	if userDef, ok := obj.(*model.UserDefinedObject); ok {
		return encodeUserDefined(ctx, userDef)
	}
	if list, ok := obj.(*model.ListObject); ok {
		return encodeList(ctx, list)
	}
	if str, ok := obj.(model.StringObject); ok {
		return encodeString(str)
	}
	if convertible, ok := obj.(runtime.Convertible); ok {
		return encodeConvertible(ctx, convertible)
	}
	return "\"invalid\""
}

func encodeList(ctx *runtime.Context, list *model.ListObject) string {
	if len(*list) == 0 {
		return "[]"
	}
	r := fmt.Sprintf("[%s", EncodeObject(ctx, (*list)[0]))
	for _, el := range (*list)[1:] {
		r += fmt.Sprintf(", %s", EncodeObject(ctx, el))
	}
	return r+"]"
}

func encodeConvertible(ctx *runtime.Context, obj runtime.Convertible) string {
	conv, err := obj.Convert(ctx, types.TypeString)
	if err != nil {
		return "null"
	}
	return string(conv.(model.StringObject))
}

func encodeString(obj model.StringObject) string {
	return fmt.Sprintf("\"%s\"", string(obj))
}

func fieldToKey(field types.TypeField) string {
	for _, a := range field.Annots {
		switch a.Key {
		case "json", "key":
			return a.Value
		}
	}
	return field.Name
}

func encodeUserDefined(ctx *runtime.Context, obj *model.UserDefinedObject) string {
	fields := ctx.Reflector().GetTypeAttribs(obj.Type())
	if len(fields) == 0 || len(fields) != len(obj.Data) {
		return "{}"
	}
	r := "{"
	first := 0
	for first < len(fields) {
		keyName := fieldToKey(fields[first])
		if len(keyName) > 0 {
			r += fmt.Sprintf("\"%s\": %s", keyName, EncodeObject(ctx, obj.Data[first]))
			break
		}
		first++
	}
	for i, f := range fields[first+1:] {
		keyName := fieldToKey(f)
		if len(keyName) > 0 {
			r += fmt.Sprintf(", \"%s\": %s", keyName, EncodeObject(ctx, obj.Data[first+i+1]))
		}
	}
	return r+"}"
}

type EncodeFunc struct {}

func (f EncodeFunc) Describe(ctx *runtime.Context) string {
	return "<func encode>"
}

func (f EncodeFunc) Type() types.ID {
	return types.TypeFunction
}

func (f EncodeFunc) Call(ctx *runtime.Context, numArgs int) *runtime.Error {
	stack := ctx.Stack()
	if numArgs != 1 {
		stack.Push(model.None)
		return ctx.Error("invalid number of arguments")
	}
	obj := stack.Pop()
	stack.Pop() // FUNC
	stack.Push(model.StringObject(EncodeObject(ctx, obj)))
	return nil
}

func feedObject(ctx *runtime.Context, expect types.ID, reader tokens.TokenReader) (runtime.Object, error) {
	token, ok := reader.Next()
	if !ok {
		return nil, fmt.Errorf("unexpected end of input")
	}
	switch token.Kind {
	case '{':
		return feedStructure(ctx, expect, reader)
	case '[':
		return feedList(ctx, expect, reader)
	case tokens.TokenIdentifier:
		if token.Content != "null" {
			return nil, fmt.Errorf("%d:%d unexpected token", token.Line, token.Column)
		}
		return model.None, nil
	case tokens.TokenStringDQ:
		if expect != types.TypeString {
			return nil, fmt.Errorf("%d:%d expected string", token.Line, token.Column)
		}
		return model.StringObject(token.Content), nil
	case tokens.TokenDecimal, tokens.TokenFloat:
		switch expect {
		case types.TypeInt32:
			n, _ := strconv.Atoi(token.Content)
			return model.Int32Object(n), nil
		case types.TypeFloat32:
			n, _ := strconv.ParseFloat(token.Content, 32)
			return model.Float32Object(n), nil
		}
		return nil, fmt.Errorf("%d:%d expected a number type", token.Line, token.Column)
	}
	return nil, fmt.Errorf("%d:%d expected token", token.Line, token.Column)
}

type attrib struct {
	i int
	t types.ID
}

func attribsToMap(attribs []types.TypeField) map[string]attrib {
	r := map[string]attrib{}
	for i, a := range attribs {
		r[fieldToKey(a)] = attrib{i: i, t: a.TypeID}
	}
	return r
}

func feedStructure(ctx *runtime.Context, expect types.ID, reader tokens.TokenReader) (*model.UserDefinedObject, error) {
	fields := attribsToMap(ctx.Reflector().GetTypeAttribs(expect))
	r := &model.UserDefinedObject{
		Data:      make([]runtime.Object, len(fields)),
		ObjType:   expect,
	}
	for {
		var err error

		token, ok := reader.Next()
		if !ok {
			return nil, fmt.Errorf("unexpected end of input")
		}
		if token.Kind == '}' {
			break
		}
		if token.Kind != tokens.TokenStringDQ {
			return nil, fmt.Errorf("%d:%d expected string", token.Line, token.Column)
		}
		fieldName := token.Content

		token, ok = reader.Next()
		if !ok {
			return nil, fmt.Errorf("unexpected end of input")
		}
		if token.Kind != ':' {
			return nil, fmt.Errorf("%d:%d unexpected token", token.Line, token.Column)
		}

		if f, ok := fields[fieldName]; ok {
			r.Data[f.i], err = feedObject(ctx, f.t, reader)
			if err != nil {
				return nil, err
			}
		} else {
			reader.Next() // Skip value
		}

		token, ok = reader.Next()
		if !ok {
			return nil, fmt.Errorf("unexpected end of input")
		}

		if token.Kind == '}' {
			break
		}
		if token.Kind != ',' {
			return nil, fmt.Errorf("%d:%d unexpected token", token.Line, token.Column)
		}
	}
	for i := range r.Data {
		if r.Data[i] == nil {
			r.Data[i] = model.None
		}
	}
	return r, nil
}

func feedList(ctx *runtime.Context, expect types.ID, reader tokens.TokenReader) (*model.ListObject, error) {
	generic := ctx.Reflector().GetGeneric(expect)
	if generic.BaseType != types.TypeArray {
		return nil, fmt.Errorf("invalid type")
	}

	r := model.ListObject{}
	for {
		token, ok := reader.Next()
		if !ok {
			return nil, fmt.Errorf("unexpected end of input")
		}
		if token.Kind == ']' {
			return &r, nil
		}
		reader.SeekBack()

		obj, err := feedObject(ctx, generic.SubType, reader)
		if err != nil {
			return nil, err
		}
		r = append(r, obj)

		token, ok = reader.Next()
		if !ok {
			return nil, fmt.Errorf("unexpected end of input")
		}
		if token.Kind == ']' {
			return &r, nil
		}
		if token.Kind != ',' {
			return nil, fmt.Errorf("%d:%d unexpected token", token.Line, token.Column)
		}
	}
}

func DecodeJSON(ctx *runtime.Context, expectedType types.ID, buffer []byte) (runtime.Object, error) {
	reader, err := lexer.ReadTokens(bytes.ReplaceAll(buffer, []byte{'\n'}, []byte{}))
	if err != nil {
		return nil, err
	}
	object, err := feedObject(ctx, expectedType, reader)
	if err != nil {
		return nil, err
	}

	return object, nil
}

type DecodeFunc struct {}

func (f DecodeFunc) Describe(ctx *runtime.Context) string {
	return "<func decode>"
}

func (f DecodeFunc) Type() types.ID {
	return types.TypeFunction
}

func (f DecodeFunc) Call(ctx *runtime.Context, numArgs int) *runtime.Error {
	if numArgs != 2 {
		return ctx.Error("invalid number of arguments")
	}
	stack := ctx.Stack()
	obj, ok := stack.Pop().(model.StringObject)
	if !ok {
		stack.Push(model.None)
		return ctx.Error("expected a string")
	}
	expectedType, ok := stack.Pop().(model.Int32Object)
	if !ok {
		stack.Push(model.None)
		return ctx.Error("expected a type")
	}
	stack.Pop() // FUNC

	decoded, err := DecodeJSON(ctx, types.ID(expectedType), []byte(string(obj)))
	if err != nil {
		stack.Push(model.None)
		return ctx.Error(err.Error())
	}
	stack.Push(decoded)

	return nil
}

func AddSymbols(symbolTable symbols.Table) {
	encodeFuncType, err := symbolTable.RegisterType(types.NewFunction(types.TypeString, []types.ID{types.TypeUnknown}, false))
	if err != nil {
		panic(err)
	}
	if err := symbolTable.RegisterGlobal("jsonEncode", encodeFuncType); err != nil {
		panic(err)
	}

	decodeFuncType, err := symbolTable.RegisterType(types.NewFunction(types.TypeUnknown, []types.ID{types.TypeInt32, types.TypeString}, false))
	if err != nil {
		panic(err)
	}
	if err := symbolTable.RegisterGlobal("jsonDecode", decodeFuncType); err != nil {
		panic(err)
	}
}

func InjectObjects(ctx *runtime.Context, symbolTable symbols.Table) {
	ctx.SetGlobal(symbolTable.MustFind("jsonEncode", scope.Global).Reference, EncodeFunc{})
	ctx.SetGlobal(symbolTable.MustFind("jsonDecode", scope.Global).Reference, DecodeFunc{})
}
