package model

import (
	"gitlab.com/egosummiki/litequel/runtime"
	"gitlab.com/egosummiki/litequel/types"
)

type NoneExpr struct{}

var None = NoneExpr{}

func (n NoneExpr) Evaluate(ctx *runtime.Context) runtime.Object {
	return None
}

func (n NoneExpr) Describe(ctx *runtime.Context) string {
	return "<none>"
}


func (n NoneExpr) Convert(ctx *runtime.Context, typeID types.ID) (runtime.Object, *runtime.Error) {
	if typeID == types.TypeString {
		return StringObject("none"), nil
	}
	return None, nil
}

func (n NoneExpr) Type() types.ID {
	return types.TypeUnknown
}
