package model

import (
	"fmt"

	"gitlab.com/egosummiki/litequel/runtime"
	"gitlab.com/egosummiki/litequel/types"
)

type UserDefinedObject struct {
	Data      []runtime.Object
	ObjType   types.ID
	AddMethod int
}

func (ud *UserDefinedObject) Describe(ctx *runtime.Context) string {
	if len(ud.Data) == 0 {
		return "{}"
	}
	reflect := ctx.Reflector()
	fields := reflect.GetTypeAttribs(ud.ObjType)
	if len(fields) != len(ud.Data) {
		return "##INVALID_OBJ##"
	}

	result :=  reflect.GetTypeName(ud.ObjType) + "{" + fields[0].Name + ": " + ud.Data[0].Describe(ctx)
	for i, d := range ud.Data[1:] {
		result += ", " + fields[i+1].Name + ": " + d.Describe(ctx)
	}
	return result + "}"
}

func (ud *UserDefinedObject) Type() types.ID {
	return ud.ObjType
}

func (ud *UserDefinedObject) Convert(ctx *runtime.Context, typeID types.ID) (runtime.Object, *runtime.Error) {
	if typeID == types.TypeString {
		if len(ud.Data) == 0 {
			return StringObject("{}"), nil
		}
		reflect := ctx.Reflector()
		fields := reflect.GetTypeAttribs(ud.ObjType)
		if len(fields) != len(ud.Data) {
			return StringObject("##INVALID_OBJ##"), nil
		}

		result := "{" + convertField(ctx, fields, 0, ud.Data[0])
		for i, d := range ud.Data[1:] {
			result += ", " + convertField(ctx, fields, i+1, d)
		}
		return StringObject(result + "}"), nil
	}
	return None, nil
}

func convertField(ctx *runtime.Context, fields []types.TypeField, i int, obj runtime.Object) string {
	if obj.Type() == types.TypeString {
		return fmt.Sprintf("%s: \"%s\"", fields[i].Name, string(obj.(StringObject)))
	}
	conv, err := obj.(runtime.Convertible).Convert(ctx, types.TypeString)
	if err != nil {
		return "#"
	}
	return fmt.Sprintf("%s: %s", fields[i].Name, string(conv.(StringObject)))
}

func (ud *UserDefinedObject) GetAttribute(ctx *runtime.Context, index int) runtime.Object {
	if index < 0 || index >= len(ud.Data) {
		return None
	}

	return ud.Data[index]
}

func (ud *UserDefinedObject) SetProperty(ctx *runtime.Context, index int, value runtime.Object) {
	if index < 0 || index >= len(ud.Data) {
		return
	}
	ud.Data[index] = value
}

func (ud *UserDefinedObject) Add(ctx *runtime.Context, obj runtime.Object) runtime.Object {
	if ud.AddMethod == 0 {
		return None
	}
	stack := ctx.Stack()
	method := ctx.GetGlobal(ud.AddMethod)
	stack.Push(ud)
	stack.Push(method)
	stack.Push(obj)

	err := method.(runtime.Callable).Call(ctx, 1)
	if err != nil {
		stack.Pop()
		return nil
	}
	return stack.Pop()
}

func (ud *UserDefinedObject) Project(ctx *runtime.Context, projectionFunc runtime.Callable) (runtime.Object, *runtime.Error) {
	stack := ctx.Stack()
	stack.Push(projectionFunc.(runtime.Object))
	stack.Push(ud)
	for _, prop := range ud.Data {
		stack.Push(prop)
	}
	err := projectionFunc.Call(ctx, len(ud.Data)+1)
	if err != nil {
		return nil, err
	}
	return stack.Pop(), nil
}
