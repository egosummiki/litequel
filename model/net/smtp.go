package net

import (
	"gitlab.com/egosummiki/litequel/model"
	"gitlab.com/egosummiki/litequel/runtime"
	"gitlab.com/egosummiki/litequel/types"

	"net/smtp"
)

type SmtpAuthObject struct {
	Auth smtp.Auth
}

func (auth SmtpAuthObject) Describe(ctx *runtime.Context) string {
	return "<smtp.auth>"
}

func (auth SmtpAuthObject) Type() types.ID {
	return types.TypeUnknown
}

type SmtpClientObject struct {
	Client *smtp.Client
}

func (client SmtpClientObject) Describe(ctx *runtime.Context) string {
	return "<smtp.client>"
}

func (client SmtpClientObject) Type() types.ID {
	return types.TypeUnknown
}

type SmtpAuthFunc struct{}

func (f SmtpAuthFunc) Call(ctx *runtime.Context, numArgs int) *runtime.Error {
	if numArgs != 3 {
		panic("wrong number of arguments")
	}
	domain, ok3 := ctx.Stack().Pop().(model.StringObject)
	password, ok2 := ctx.Stack().Pop().(model.StringObject)
	login, ok1 := ctx.Stack().Pop().(model.StringObject)
	if !ok3 || !ok2 || !ok1 {
		panic("invalid arguments")
	}
	ctx.Stack().Pop()
	ctx.Stack().Push(SmtpAuthObject{Auth: smtp.PlainAuth("", string(login), string(password), string(domain))})
	return nil
}

func (f SmtpAuthFunc) Describe(ctx *runtime.Context) string {
	return "<func smtpAuth>"
}

func (f SmtpAuthFunc) Type() types.ID {
	return types.TypeFunction
}

type SendMailFunc struct{}

func (f SendMailFunc) Call(ctx *runtime.Context, numArgs int) *runtime.Error {
	if numArgs != 5 {
		panic("wrong number of arguments")
	}
	body, ok5 := ctx.Stack().Pop().(model.StringObject)
	to, ok4 := ctx.Stack().Pop().(model.StringObject)
	from, ok3 := ctx.Stack().Pop().(model.StringObject)
	auth, ok2 := ctx.Stack().Pop().(SmtpAuthObject)
	addr, ok1 := ctx.Stack().Pop().(model.StringObject)
	if !ok5 || !ok4 || !ok3 || !ok2 || !ok1 {
		panic("invalid arguments")
	}
	ctx.Stack().Pop()
	err := smtp.SendMail(string(addr), auth.Auth, string(from), []string{string(to)}, []byte(string(body)))
	if err != nil {
		ctx.Stack().Push(model.StringObject(err.Error()))
		return nil
	}
	ctx.Stack().Push(model.StringObject(""))
	return nil
}

func (f SendMailFunc) Describe(ctx *runtime.Context) string {
	return "<func sendMail>"
}

func (f SendMailFunc) Type() types.ID {
	return types.TypeFunction
}
