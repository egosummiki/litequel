package std

import (
	"fmt"
	"io/ioutil"
	"math/rand"
	"time"

	"gitlab.com/egosummiki/litequel/model"
	"gitlab.com/egosummiki/litequel/model/function"
	"gitlab.com/egosummiki/litequel/runtime"
	"gitlab.com/egosummiki/litequel/runtime/execution"
	"gitlab.com/egosummiki/litequel/types"
)

func stackToArgs(ctx *runtime.Context, stack *runtime.Stack, numArgs int) []string {
	args := make([]string, numArgs)
	for i := numArgs - 1; i >= 0; i-- {
		conv, err := stack.Pop().(runtime.Convertible).Convert(ctx, types.TypeString)
		if err != nil {
			args[i] = "#"
			continue
		}
		args[i] = string(conv.(model.StringObject))
	}
	return args
}

type InputFunc struct{}

func (f InputFunc) Call(ctx *runtime.Context, numArgs int) *runtime.Error {
	var value string
	_, _ = fmt.Scan(&value)
	ctx.Stack().Pop()
	ctx.Stack().Push(model.StringObject(value))
	return nil
}

func (f InputFunc) Describe(ctx *runtime.Context) string {
	return "<func input>"
}

func (f InputFunc) Type() types.ID {
	return types.TypeFunction
}

type ReadIntFunc struct{}

func (f ReadIntFunc) Call(ctx *runtime.Context, numArgs int) *runtime.Error {
	var value int
	fmt.Scan(&value)
	ctx.Stack().Pop()
	ctx.Stack().Push(model.Int32Object(value))
	return nil
}

func (f ReadIntFunc) Describe(ctx *runtime.Context) string {
	return "<func readInt>"
}

func (f ReadIntFunc) Type() types.ID {
	return types.TypeFunction
}

type DisasemFunc struct{}

func (f DisasemFunc) Call(ctx *runtime.Context, numArgs int) *runtime.Error {
	fn := ctx.Stack().Pop().(function.Function)

	for i, ins := range ctx.Runner().(*execution.Execution).GetBucket(fn.BucketID).Instructions {
		fmt.Printf("%04d  %s\n", i, ins.Mnemonic(ctx))
	}
	ctx.Stack().Pop()
	ctx.Stack().Push(model.None)
	return nil
}

func (f DisasemFunc) Describe(ctx *runtime.Context) string {
	return "<func disasem>"
}

func (f DisasemFunc) Type() types.ID {
	return types.TypeFunction
}

type LenFunc struct{}

func (f LenFunc) Call(ctx *runtime.Context, numArgs int) *runtime.Error {
	if numArgs != 1 {
		return nil
	}
	stack := ctx.Stack()
	arg := stack.Pop()
	stack.SlideBack(1)
	switch arg.(type) {
	case *model.ListObject:
		list := arg.(*model.ListObject)
		stack.Push(model.Int32Object(len(*list)))
	case *model.SliceObject:
		slice := arg.(*model.SliceObject)
		stack.Push(model.Int32Object(slice.Max - slice.Min))
	case *model.Range:
		r := arg.(*model.Range)
		stack.Push(model.Int32Object(r.Size()))
	default:
		stack.Push(model.None)
	}
	return nil
}

func (f LenFunc) Describe(ctx *runtime.Context) string {
	return "len"
}

func (f LenFunc) Type() types.ID {
	return types.TypeFunction
}

type RandFunc struct{}

func (f RandFunc) Call(ctx *runtime.Context, numArgs int) *runtime.Error {
	stack := ctx.Stack()
	switch numArgs {
	case 0:
		stack.SlideBack(1)
		stack.Push(model.Int32Object(rand.Int()))
	case 1:
		max := int(stack.Pop().(model.Int32Object))
		stack.SlideBack(1)
		stack.Push(model.Int32Object(rand.Intn(max)))
	case 2:
		min := int(stack.Pop().(model.Int32Object))
		max := int(stack.Pop().(model.Int32Object))
		stack.SlideBack(1)
		stack.Push(model.Int32Object(rand.Intn(max) + min))
	default:
		panic("Wrong number of arguments")
	}
	return nil
}

func (f RandFunc) Describe(ctx *runtime.Context) string {
	return "rand"
}

func (f RandFunc) Type() types.ID {
	return types.TypeFunction
}

type OutObj struct{}

func (o OutObj) Describe(ctx *runtime.Context) string {
	return "<stdout_buffer>"
}

func (o OutObj) Type() types.ID {
	return types.TypeBuffer
}

func (o OutObj) Append(ctx *runtime.Context, obj runtime.Object) *runtime.Error {
	conv, err := obj.(runtime.Convertible).Convert(ctx, types.TypeString)
	if err != nil {
		return err
	}
	fmt.Print(string(conv.(model.StringObject)))
	return nil
}

type OutPrint struct{}

func (o OutPrint) Describe(ctx *runtime.Context) string {
	return "<method print>"
}

func (o OutPrint) Type() types.ID {
	return types.TypeFunction
}

func (o OutPrint) Call(ctx *runtime.Context, numArgs int) *runtime.Error {
	stack := ctx.Stack()
	args := stackToArgs(ctx, stack, numArgs)
	for _, a := range args {
		fmt.Print(a)
	}
	fmt.Println()
	stack.Pop() // FUNC
	stack.Pop() // THIS
	stack.Push(model.None)
	return nil
}

type TimeUnix struct{}

func (t TimeUnix) Describe(ctx *runtime.Context) string {
	return "<timeunix>"
}

func (t TimeUnix) Type() types.ID {
	return types.TypeFunction
}

func (t TimeUnix) Call(ctx *runtime.Context, numArgs int) *runtime.Error {
	if numArgs != 0 {
		return ctx.Error("too many arguments")
	}
	stack := ctx.Stack()
	stack.Pop() // FUNC
	stack.Push(model.Uint64Object(time.Now().UnixNano()))
	return nil
}


type ReadFile struct{}

func (r ReadFile) Describe(ctx *runtime.Context) string {
	return "<func readAllFile>"
}

func (r ReadFile) Type() types.ID {
	return types.TypeFunction
}

func (r ReadFile) Call(ctx *runtime.Context, numArgs int) *runtime.Error {
	stack := ctx.Stack()
	fileName, ok := stack.Pop().(model.StringObject)
	if !ok {
		return ctx.Error("could not convert to string object")
	}

	stack.Pop() // FUNC

	contents, err := ioutil.ReadFile(string(fileName))
	if err != nil {
		return ctx.Error(err.Error())
	}

	stack.Push(model.StringObject(string(contents)))
	return nil
}
