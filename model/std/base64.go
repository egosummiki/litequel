package std

import (
	"encoding/base64"

	"gitlab.com/egosummiki/litequel/model"
	"gitlab.com/egosummiki/litequel/runtime"
	"gitlab.com/egosummiki/litequel/types"
)

type Base64Func struct{}

func (f Base64Func) Call(ctx *runtime.Context, numArgs int) *runtime.Error {
	val, ok := ctx.Stack().Pop().(model.StringObject)
	if !ok {
		panic("invalid object")
	}
	ctx.Stack().Pop()
	ctx.Stack().Push(model.StringObject(base64.StdEncoding.EncodeToString([]byte(string(val)))))
	return nil
}

func (f Base64Func) Describe(ctx *runtime.Context) string {
	return "<func base64>"
}

func (f Base64Func) Type() types.ID {
	return types.TypeFunction
}

type Base64DecFunc struct{}

func (f Base64DecFunc) Call(ctx *runtime.Context, numArgs int) *runtime.Error {
	val, ok := ctx.Stack().Pop().(model.StringObject)
	if !ok {
		panic("invalid object")
	}
	ctx.Stack().Pop()
	res, err := base64.StdEncoding.DecodeString(string(val))
	if err != nil {
		panic(err)
	}
	ctx.Stack().Push(model.StringObject(string(res)))
	return nil
}

func (f Base64DecFunc) Describe(ctx *runtime.Context) string {
	return "<func base64dec>"
}

func (f Base64DecFunc) Type() types.ID {
	return types.TypeFunction
}
