package std

import (
	"gitlab.com/egosummiki/litequel/model"
	"gitlab.com/egosummiki/litequel/runtime"
	"gitlab.com/egosummiki/litequel/types"
)

type OrdFunc struct{}

func (f OrdFunc) Call(ctx *runtime.Context, numArgs int) *runtime.Error {
	char, ok := ctx.Stack().Pop().(model.StringObject)
	ctx.Stack().Pop()
	if !ok || len(char) != 1 {
		ctx.Stack().Push(model.Int8Object(0))
		return nil
	}
	ctx.Stack().Push(model.Int8Object(char[0]))
	return nil
}

func (f OrdFunc) Describe(ctx *runtime.Context) string {
	return "<func ord>"
}

func (f OrdFunc) Type() types.ID {
	return types.TypeFunction
}

type ChrFunc struct{}

func (f ChrFunc) Call(ctx *runtime.Context, numArgs int) *runtime.Error {
	conv, err := ctx.Stack().Pop().(runtime.Convertible).Convert(ctx, types.TypeInt8)
	if err != nil {
		return err
	}
	chr := conv.(model.Int8Object)
	ctx.Stack().Pop()
	ctx.Stack().Push(model.StringObject{rune(chr)})
	return nil
}

func (f ChrFunc) Describe(ctx *runtime.Context) string {
	return "<func chr>"
}

func (f ChrFunc) Type() types.ID {
	return types.TypeFunction
}
