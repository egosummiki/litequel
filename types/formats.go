package types

import "fmt"

func FormatSystemType(baseType ID, subTypes ...ID) string {
	result := fmt.Sprintf("%04X", baseType)
	for _, t := range subTypes {
		result += fmt.Sprintf("-%04X", t)
	}
	return result
}

func FormatFunctionType(returnType ID, arguments []ID, recv ID, isVariadic bool) string {
	var result string

	if recv != TypeUnknown {
		result += fmt.Sprintf("%04X.", recv)
	}

	result += "func("

	if len(arguments) > 0 {
		result += fmt.Sprintf("%04X", arguments[0])
		for _, a := range arguments[1:] {
			result += fmt.Sprintf(",%04X", a)
		}
	}

	if isVariadic {
		result += ",..."
	}

	result += ")"

	if returnType != TypeUnknown {
		result += fmt.Sprintf("<-%04X", returnType)
	}

	return result
}

