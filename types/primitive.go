package types

import "unsafe"

const (
	TypeUnknown ID = iota
	TypeInt8
	TypeInt16
	TypeInt32
	TypeInt64
	TypeUint8
	TypeUint16
	TypeUint32
	TypeUint64
	TypeFloat32
	TypeFloat64
	TypeString
	TypeBool
	TypeRange
	TypeFunction
	TypeArray
	TypeBuffer
	TypeError
	TypeMap
	TypePointer
)

var PrimitiveDetails = []Details{
	NewLanguageSpecific("unknown"),
	NewPrimitive("int8", int(unsafe.Sizeof(int8(0)))),
	NewPrimitive("int16", int(unsafe.Sizeof(int16(0)))),
	NewPrimitive("int32", int(unsafe.Sizeof(int32(0)))),
	NewPrimitive("int64", int(unsafe.Sizeof(int64(0)))),
	NewPrimitive("uint8", int(unsafe.Sizeof(uint8(0)))),
	NewPrimitive("uint16", int(unsafe.Sizeof(uint16(0)))),
	NewPrimitive("uint32", int(unsafe.Sizeof(uint32(0)))),
	NewPrimitive("uint64", int(unsafe.Sizeof(uint64(0)))),
	NewPrimitive("float32", int(unsafe.Sizeof(float32(0)))),
	NewPrimitive("float64", int(unsafe.Sizeof(float64(0)))),
	NewLanguageSpecific("string"),
	NewPrimitive("bool", 1),
	NewLanguageSpecific("range"),
	NewFunction(TypeUnknown, nil, true),
	NewLanguageSpecific("list"),
	NewLanguageSpecific("buffer"),
	NewLanguageSpecific("error"),
	NewLanguageSpecific("map"),
	NewLanguageSpecific("ptr"),
}

func PrimitiveNames() map[string]ID {
	result := map[string]ID{}
	for id, d := range PrimitiveDetails {
		result[d.TypeName()] = ID(id)
	}
	// aliases
	result["int"] = TypeInt32
	result["float"] = TypeFloat32

	return result
}
