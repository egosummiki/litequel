package types

import (
	"reflect"
	"strconv"
)

type ID int32

var typeNames = []string{
	0:  "unknown",
	1:  "int8",
	2:  "int16",
	3:  "int32",
	4:  "int64",
	5:  "uint8",
	6:  "uint16",
	7:  "uint32",
	8:  "uint64",
	9:  "float32",
	10: "float64",
	11: "string",
	12: "bool",
	13: "range",
	14: "func",
	15: "list",
	16: "buffer",
	17: "error",
	18: "map",
	19: "ptr",
}

func (t ID) String() string {
	tint := int(t)
	if tint >= len(typeNames) {
		return strconv.Itoa(tint)
	}
	return typeNames[tint]
}

type TypeProperties int32

type TypeVariant int

const (
	VariantPrimitive TypeVariant = iota
	VariantList
	VariantMap
	VariantStruct
	VariantGeneric
	VariantFunction
	VariantInterface
	VariantLanguageSpecific
	VariantCustomCollection
)

type TypeField struct {
	TypeID ID
	Annots []Annotation
	Name   string
}

type Details interface {
	TypeName() string
	TypeVariant() TypeVariant
}

type Collection interface {
	ElementType() ID
}

func DetailsEqual(a Details, b Details) bool {
	if a.TypeVariant() == VariantFunction {
		if b.TypeVariant() != VariantFunction {
			return false
		}

		aFunc := a.(Function)
		bFunc := b.(Function)

		if aFunc.Name != bFunc.Name {
			return false
		}
		if aFunc.ReturnType != bFunc.ReturnType {
			return false
		}
		if aFunc.Receiver != bFunc.Receiver {
			return false
		}
		if aFunc.IsVariadic != bFunc.IsVariadic {
			return false
		}
		if len(aFunc.Arguments) != len(bFunc.Arguments) {
			return false
		}
		for i, arg := range aFunc.Arguments {
			if arg != bFunc.Arguments[i] {
				return false
			}
		}

		return true
	}

	return reflect.DeepEqual(a, b)
}

type BaseType struct {
	Name string
}

func (b BaseType) TypeName() string {
	return b.Name
}

type Primitive struct {
	BaseType
	Size int
}

func (p Primitive) TypeVariant() TypeVariant {
	return VariantPrimitive
}

func NewPrimitive(name string, size int) Primitive {
	return Primitive{
		BaseType: BaseType{
			Name: name,
		},
		Size: size,
	}
}

type TypeStruct struct {
	BaseType
	Fields  []TypeField
	Methods []Method
}

func NewTypeStruct(name string, fields []TypeField) TypeStruct {
	return TypeStruct{BaseType: BaseType{Name: name}, Fields: fields}
}

func (t TypeStruct) TypeVariant() TypeVariant {
	return VariantStruct
}

type TypeGeneric struct {
	BaseType
	Type     ID
	Subtypes []ID
}

func NewTypeGeneric(name string, baseType ID, subtypes []ID) *TypeGeneric {
	return &TypeGeneric{BaseType: BaseType{Name: name}, Type: baseType, Subtypes: subtypes}
}

func GenericOf(baseType ID, subtypes ...ID) *TypeGeneric {
	name := FormatSystemType(baseType, subtypes...)
	return NewTypeGeneric(name, baseType, subtypes)
}

func (t TypeGeneric) TypeVariant() TypeVariant {
	return VariantGeneric
}

type CustomCollection struct {
	BaseType
	ElType ID
}

func NewCustomCollection(name string, elementType ID) *CustomCollection {
	return &CustomCollection{
		BaseType: BaseType{
			Name: name,
		},
		ElType: elementType,
	}
}

func (c CustomCollection) ElementType() ID {
	return c.ElType
}

func (c CustomCollection) TypeVariant() TypeVariant {
	return VariantCustomCollection
}

type PrimitiveType struct {
	BaseType
	Size uint32
}

func (p PrimitiveType) TypeVariant() TypeVariant {
	return VariantPrimitive
}

type List struct {
	BaseType
	ElType ID
}

func NewTypeList(name string, elementType ID) *List {
	return &List{
		BaseType: BaseType{
			Name: name,
		},
		ElType: elementType,
	}
}

func ListOf(elementType ID) *List {
	name := FormatSystemType(TypeArray, elementType)
	return NewTypeList(name, elementType)
}

func (t List) TypeVariant() TypeVariant {
	return VariantList
}

func (t List) ElementType() ID {
	return t.ElType
}

type Map struct {
	BaseType
	KeyType   ID
	ValueType ID
}

func NewTypeMap(name string, keyType ID, valueType ID) *Map {
	return &Map{BaseType: BaseType{Name: name}, KeyType: keyType, ValueType: valueType}
}

func MapOf(keyType ID, valueType ID) *Map {
	name := FormatSystemType(TypeMap, keyType, valueType)
	return NewTypeMap(name, keyType, valueType)
}

func (t Map) TypeVariant() TypeVariant {
	return VariantMap
}

func (t Map) ElementType() ID {
	return t.ValueType
}

type TypeSpecific struct {
	BaseType
}

func NewLanguageSpecific(name string) *TypeSpecific {
	return &TypeSpecific{BaseType: BaseType{
		Name: name,
	}}
}

func (t TypeSpecific) TypeVariant() TypeVariant {
	return VariantLanguageSpecific
}

type TypeEnum struct {
	BaseType
}

func NewTypeEnum(name string) *TypeEnum {
	return &TypeEnum{BaseType: BaseType{
		Name: name,
	}}
}

func (t TypeEnum) TypeVariant() TypeVariant {
	return VariantPrimitive
}

type Function struct {
	BaseType
	ReturnType ID
	Arguments  []ID
	IsVariadic bool
	Receiver   ID
}

func (t Function) TypeVariant() TypeVariant {
	return VariantFunction
}

func NewFunction(returnType ID, arguments []ID, isVariadic bool) Function {
	return Function{
		BaseType: BaseType{
			Name: FormatFunctionType(returnType, arguments, TypeUnknown, isVariadic),
		},
		ReturnType: returnType,
		Arguments:  arguments,
		IsVariadic: isVariadic,
		Receiver:   TypeUnknown,
	}
}

func NewMethod(returnType ID, arguments []ID, receiver ID, isVariadic bool) Function {
	return Function{
		BaseType: BaseType{
			Name: FormatFunctionType(returnType, arguments, receiver, isVariadic),
		},
		ReturnType: returnType,
		Arguments:  arguments,
		IsVariadic: isVariadic,
		Receiver:   receiver,
	}
}

type Annotation struct {
	Key   string
	Value string
}

type Method struct {
	Name       string
	Args       []ID
	ReturnType ID
	Variadic   bool
}

func MethodsEqual(left Method, right Method) bool {
	if left.Name != right.Name {
		return false
	}

	if len(left.Args) != len(right.Args) {
		return false
	}

	for i := range left.Args {
		if left.Args[i] != right.Args[i] {
			return false
		}
	}

	if left.ReturnType != right.ReturnType {
		return false
	}

	if left.Variadic != right.Variadic {
		return false
	}

	return true
}

type Interface struct {
	BaseType
	Contract []Method
}

func NewInterfaceType(name string, contract []Method) Interface {
	return Interface{
		BaseType: BaseType{
			Name: name,
		},
		Contract: contract,
	}
}

func (i Interface) TypeVariant() TypeVariant {
	return VariantInterface
}
