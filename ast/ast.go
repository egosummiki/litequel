package ast

type Expression struct {
	Elements  []interface{}
	Boolean   bool
	Line, Col int
}

type OrStmt struct {
	Line    int
	Col     int
	ErrName string
}

type FunctionCall struct {
	Line, Col int
	Arguments []Expression
	Or        *OrStmt
}

type ArrayLiteral struct {
	Elements  []Expression
	Line, Col int
}

type IndexOperator struct {
	Index     Expression
	Line, Col int
}

type IfStmt struct {
	Condition Expression
	Then      []interface{}
	Else      []interface{}
	Line, Col int
}

type ForStmt struct {
	What      string
	Over      Expression
	Body      []interface{}
	Line, Col int
}

type WhileStmt struct {
	Condition Expression
	Body      []interface{}
	Line, Col int
}

type AssigmentKind int8

const (
	AssignEq         AssigmentKind = 0
	AssignPlus       AssigmentKind = 1
	AssignMinus      AssigmentKind = 2
	AssignMult       AssigmentKind = 3
	AssignDiv        AssigmentKind = 4
	AssignPlusPlus   AssigmentKind = 5
	AssignMinusMinus AssigmentKind = 6
	AssignAppend     AssigmentKind = 7
)

type AssignStmt struct {
	Target    Expression
	Value     Expression
	Kind      AssigmentKind
	Line, Col int
}

type VariableDecl struct {
	Name      string
	TypeSpec  Type
	Value     Expression
	Line, Col int
}

type ConstDecl struct {
	Name      string
	TypeSpec  Type
	Value     Expression
	Line, Col int
	Exported  bool
}

type ReturnStmt struct {
	Value     Expression
	Line, Col int
}

type FuncDeclParam struct {
	Name     string
	TypeSpec Type
}

type FunctionDefinition struct {
	Name           string
	Receiver       Type
	Return         Type
	Variadic       bool
	Params         []FuncDeclParam
	Body           []interface{}
	TemplateParams []TemplateParam
	Line, Col      int
	Exported       bool
}

func (f FunctionDefinition) TemName() string {
	return f.Name
}

func (f FunctionDefinition) TemParams() []TemplateParam {
	return f.TemplateParams
}

type BreakStmt struct {
	Line, Col int
}
type ContinueStmt struct {
	Line, Col int
}

type BooleanOp struct {
	Level       int
	JumpOnFalse bool
	JumpToElse  bool
	Line, Col   int
}

type LiteralKind int

const (
	LiteralNil LiteralKind = iota
	LiteralInt
	LiteralFloat
	LiteralString
)

type Literal struct {
	Kind  LiteralKind
	Value string
	Line  int
	Col   int
}

func (Literal) TypeOrLiteral() {}

type Identifier struct {
	Name string
	Line int
	Col  int
}

type Operator struct {
	Kind string
	Line int
	Col  int
}

type ObjectProperty struct {
	Name string
	Line int
	Col  int
}

type Annotation struct {
	Key, Value string
}

type TemplateParamKind int

const (
	TemplateParamType TemplateParamKind = iota
	TemplateParamValue
)

type TemplateParam struct {
	Value string
	Kind  TemplateParamKind
}

type StructProperty struct {
	Name        string
	PropType    Type
	Annotations []Annotation
}

type StructDefinition struct {
	Name            string
	Properties      []StructProperty
	TemplateParams  []TemplateParam
	MethodTemplates []FunctionDefinition
	Exported        bool
	Line, Col       int
}

func (s StructDefinition) TemParams() []TemplateParam {
	return s.TemplateParams
}

func (s StructDefinition) TemName() string {
	return s.Name
}

type Constructor struct {
	TypeSpec Type
	Values   []struct {
		Arg   string
		Value Expression
	}
	Line, Col int
}

type ProjectionProperty struct {
	Arg   string
	Value Expression
}

type FormatString struct {
	Content []rune
	Format  []struct {
		Position int
		Value    Expression
	}
	Line int
	Col  int
}

type Conversion struct {
	Line       int
	Col        int
	TargetType Type
	Value      Expression
}

type FailStmt struct {
	Line    int
	Col     int
	Message Expression
}

type TypeVariant int

const (
	OrdinaryType TypeVariant = iota
	ListType
	MapType
	FuncType
)

type TypeOrLiteral interface {
	TypeOrLiteral()
}

type Type struct {
	Line         int
	Col          int
	Name         string
	Variant      TypeVariant
	Function     *FunctionType
	TemplateArgs []TypeOrLiteral
}

func (Type) TypeOrLiteral() {}

type SortStmt struct {
	Descending bool
	Field      string
	Line, Col  int
}

type MapConstruct struct {
	TypeSpec Type
	Values   []struct {
		Key   Expression
		Value Expression
	}
	Line, Col int
}

type Reference struct {
	Line int
	Col  int
}

type Enum struct {
	Values   []string
	TypeName string
	Line     int
	Col      int
}

type FunctionParam struct {
	Name     string
	TypeSpec Type
	Line     int
	Col      int
}

type FunctionType struct {
	Params     []FunctionParam
	ReturnType Type
	Line       int
	Col        int
}

type FunctionDecl struct {
	Name string
	FunctionType
}

type InterfaceDecl struct {
	Name     string
	Contract []FunctionDecl
	Line     int
	Col      int
}

type TemplateApplication struct {
	Args       []TypeOrLiteral
	Identifier Identifier
	Line       int
	Col        int
}

type Lambda struct {
	Return   Type
	Variadic bool
	Params   []FuncDeclParam
	Body     []interface{}
	Line     int
	Col      int
}
