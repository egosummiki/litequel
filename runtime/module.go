package runtime

type Module interface {
	OnInit(ctx *Context, symbolTable interface{}) *Error
	Name() string
}

// AntiPattern here - interface{} should be symbol table
// module needs it to load values into appropriate symbols
