package runtime

type IterationStack struct {
	stack []Iterator
}

func (is *IterationStack) Push(obj Iterator) {
	is.stack = append(is.stack, obj)
}

func (is *IterationStack) Top() Iterator {
	return is.stack[len(is.stack)-1]
}

func (is *IterationStack) Pop() {
	is.stack = is.stack[:len(is.stack)-1]
}
