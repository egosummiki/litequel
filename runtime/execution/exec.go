package execution

import (
	"fmt"
	"gitlab.com/egosummiki/litequel/runtime"
	"gitlab.com/egosummiki/litequel/runtime/instruction"
	"gitlab.com/egosummiki/litequel/symbols/scope"
)

type Location struct {
	Line int
	Col  int
}

type FileID int

type Execution struct {
	buckets       []Bucket
	currentBucket int
}

func NewExecution() *Execution {
	return &Execution{
		currentBucket: -1,
		buckets:       []Bucket{Bucket{}},
	}
}

func (e *Execution) GetBucket(bucketID int) *Bucket {
	return &e.buckets[bucketID]
}

func (e *Execution) NumBuckets() int {
	return len(e.buckets)
}

func (e *Execution) CurrentBucket() int {
	return e.currentBucket
}

func (e *Execution) LocaliseInstruction(i int) (int, int) {
	if i >= len(e.buckets[e.currentBucket].Locations) {
		panic(fmt.Sprintf("trying to localise non-existing instruction %d in bucket %d out of %d", i, e.currentBucket, len(e.buckets[e.currentBucket].Locations)))
	}
	loc := e.buckets[e.currentBucket].Locations[i]
	return loc.Line, loc.Col
}

func (e *Execution) SetMainBucket(name string, file FileID, instructions []instruction.Instruction, locations []Location) {
	e.buckets[0] = NewBucket(name, file, instructions, locations)
}

func (e *Execution) AddBucket(name scope.Scope, file FileID, instructions []instruction.Instruction, locations []Location) int {
	e.buckets = append(e.buckets, NewBucket(name.String(), file, instructions, locations))
	return len(e.buckets) - 1
}

func (e *Execution) RunBucket(ctx *runtime.Context, bucketID int) *runtime.Error {
	if bucketID >= len(e.buckets) || bucketID < 0 {
		return runtime.NewErrorBare("Invalid bucket ID")
	}
	prevBucket := e.currentBucket
	e.currentBucket = bucketID
	err := e.buckets[e.currentBucket].Execute(ctx)
	e.currentBucket = prevBucket
	return err
}

type Bucket struct {
	Name         string
	Instructions []instruction.Instruction
	Locations    []Location
	File         FileID
}

func NewBucket(name string, file FileID, instructions []instruction.Instruction, locations []Location) Bucket {
	return Bucket{
		Name:         name,
		File:         file,
		Instructions: instructions,
		Locations:    locations,
	}
}

func (b Bucket) Execute(ctx *runtime.Context) *runtime.Error {
	tmp := ctx.CurrentInstr
	for ctx.CurrentInstr = 0; ctx.CurrentInstr < len(b.Instructions); ctx.CurrentInstr++ {
		instr := b.Instructions[ctx.CurrentInstr]
		switch instr.(type) {
		case instruction.ReturnInst:
			ctx.CurrentInstr = tmp
			return nil
		}
		err := instr.Perform(ctx)
		if err != nil {
			ctx.CurrentInstr = tmp
			return err
		}
	}
	ctx.CurrentInstr = tmp
	return nil
}
