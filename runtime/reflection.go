package runtime

import "gitlab.com/egosummiki/litequel/types"

type Reflector interface {
	GetTypeName(typeID types.ID) string
	GetTypeAttribs(typeID types.ID) []types.TypeField
	GetGeneric(typeID types.ID) GenericType
	GetConstant(name string) int
	GetDetails(typeID types.ID) types.Details
}

