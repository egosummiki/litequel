package runtime

import "gitlab.com/egosummiki/litequel/types"

type Object interface {
	Describe(ctx *Context) string
	Type() types.ID
}

type Convertible interface {
	Convert(ctx *Context, typeID types.ID) (Object, *Error)
}

type Callable interface {
	Call(ctx *Context, numArgs int) *Error
}

type Addable interface {
	Add(ctx *Context, obj Object) Object
}

type Subtractable interface {
	Subtract(ctx *Context, obj Object) Object
}

type Multiplicable interface {
	Multiply(ctx *Context, obj Object) Object
}

type Divisible interface {
	Divide(ctx *Context, obj Object) Object
}

type Modulable interface {
	Modulo(ctx *Context, obj Object) Object
}

type Appendable interface {
	Append(ctx *Context, obj Object) *Error
}

type Iterable interface {
	Iter(ctx *Context) Iterator
}

type Iterator interface {
	Next(ctx *Context) Object
}

type Indexable interface {
	Index(ctx *Context, index Object) (Object, *Error)
}

type IndexMutable interface {
	SetIndex(ctx *Context, index Object, value Object) *Error
}

type AttributeOwner interface {
	GetAttribute(ctx *Context, index int) Object
}

type PropertyMutable interface {
	SetProperty(ctx *Context, index int, value Object)
}

type Filterable interface {
	Filter(ctx *Context, condition Callable) (Object, *Error)
}

type Projectable interface {
	Project(ctx *Context, projectionFunc Callable) (Object, *Error)
}

type Sortable interface {
	Sort(ctx *Context, prop int, desc bool) (Object, *Error)
}

type Comparable interface {
	Compare(ctx *Context, other Object) (int, *Error)
}

type Number interface {
	Addable
	Subtractable
	Multiplicable
	Divisible
	ToInt() int
}
