package runtime

const DefaultStackSize = 10000

type Stack struct {
	stack      []Object
	frameStack []int
	position   int
	frame      int
}

func NewStack(size int, frameStackSize int) *Stack {
	return &Stack{
		stack:      make([]Object, size),
		frameStack: make([]int, frameStackSize),
		position:   0,
		frame:      0,
	}
}

func (s *Stack) Frame() int {
	return s.frameStack[s.frame]
}

func (s *Stack) PopFrame() {
	s.frameStack[s.frame] = 0
	s.frame--
}

func (s *Stack) PushFrame(numArgs int) {
	s.frame++
	s.frameStack[s.frame] = s.position - numArgs
}

func (s *Stack) ClearFrame() {
	s.position = s.Frame()
}

func (s *Stack) Push(object Object) {
	if s.position >= len(s.stack) {
		panic("Stack overflow occured. Please increase the stack size.")
	}
	s.stack[s.position] = object
	s.position++
}

func (s *Stack) Pop() Object {
	if s.position <= 0 {
		panic("stack is empty")
	}
	s.position--
	return s.stack[s.position]
}

func (s *Stack) GetEnclosed(frame int, address int) Object {
	return s.stack[s.frameStack[s.frame - frame] + address]
}

func (s *Stack) StoreEnclosed(frame int, address int, obj Object) {
	s.stack[s.frameStack[s.frame - frame] + address] = obj
}

func (s *Stack) GetValue(address int) Object {
	af := address + s.Frame()
	if af >= s.position {
		panic("access violation")
	}
	return s.stack[af]
}

func (s *Stack) SetValue(address int, obj Object) {
	af := address + s.Frame()
	if af >= s.position {
		panic("access violation")
	}
	s.stack[af] = obj
}

func (s *Stack) Slide(amount int) {
	if s.position+amount >= len(s.stack) {
		panic("stack overflow occurred. please increase the stack size")
	}
	s.position += amount
}

func (s *Stack) SlideBack(amount int) {
	if s.position-amount < 0 {
		panic("Stack is empty.")
	}
	s.position -= amount
}

func (s *Stack) FromTop(index int) Object {
	return s.stack[s.position-index]
}
