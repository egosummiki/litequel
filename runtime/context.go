package runtime

import (
	"context"
	"fmt"

	"gitlab.com/egosummiki/litequel/types"
)

type Context struct {
	ctx       context.Context
	stack     *Stack
	globals   []Object
	iters     *IterationStack
	frame     int
	runner    BucketRunner
	reflector Reflector
	modules   map[string]Module

	CurrentInstr int
}

type BucketRunner interface {
	RunBucket(ctx *Context, bucketID int) *Error
	CurrentBucket() int
	LocaliseInstruction(i int) (int, int)
}

func NewRuntimeContext(bucketRunner BucketRunner, reflector Reflector, globalsSize int) *Context {
	ctx := &Context{
		ctx:          context.Background(),
		stack:        NewStack(DefaultStackSize, 24),
		frame:        0,
		CurrentInstr: 0,
		globals:      make([]Object, globalsSize),
		iters:        &IterationStack{},
		runner:       bucketRunner,
		reflector:    reflector,
		modules:      map[string]Module{},
	}
	return ctx
}

func (rc *Context) error(message string, class string) *Error {
	line, col := rc.runner.LocaliseInstruction(rc.CurrentInstr)
	return &Error{
		instruction: rc.CurrentInstr,
		line:        line,
		col:         col,
		bucket:      rc.runner.CurrentBucket(),
		message:     message,
		class:       class,
	}
}

func (rc *Context) Error(message string) *Error {
	return rc.error(message, "native")
}

func (rc *Context) Errorf(format string, attribs ...interface{}) *Error {
	return rc.error(fmt.Sprintf(format, attribs...), "native")
}

func (rc *Context) UserError(message string) *Error {
	return rc.error(message, "invoked")
}

func (rc *Context) Runner() BucketRunner {
	return rc.runner
}

func (rc *Context) Run(bucketID int) *Error {
	return rc.runner.RunBucket(rc, bucketID)
}

func (rc *Context) GetModule(name string) Module {
	return rc.modules[name]
}

func (rc *Context) AddModule(module Module) {
	rc.modules[module.Name()] = module
}

func (rc *Context) Stack() *Stack {
	return rc.stack
}

func (rc *Context) Reflector() Reflector {
	return rc.reflector
}

func (rc *Context) IterStack() *IterationStack {
	return rc.iters
}

func (rc *Context) GetEnclosed(frame int, address int) Object {
	return rc.Stack().GetEnclosed(frame, address)
}

func (rc *Context) StoreEnclosed(frame int, address int, obj Object) {
	rc.Stack().StoreEnclosed(frame, address, obj)
}

func (rc *Context) StoreAbsolute(address int, obj Object) {
	rc.Stack().SetValue(address, obj)
}

func (rc *Context) GetLocal(address int) Object {
	return rc.Stack().GetValue(rc.frame + address)
}

func (rc *Context) SetLocal(address int, obj Object) {
	rc.Stack().SetValue(rc.frame+address, obj)
}

func (rc *Context) GetGlobal(index int) Object {
	if index >= len(rc.globals) {
		panic("Not a valid global ID.")
	}
	return rc.globals[index]
}

func (rc *Context) Update(runner BucketRunner, size int) {
	rc.runner = runner
	if size <= len(rc.globals) {
		return
	}
	newGlobals := make([]Object, size)
	copy(newGlobals[:len(rc.globals)], rc.globals)
	rc.globals = newGlobals
}

func (rc Context) SetGlobal(index int, obj Object) {
	rc.globals[index] = obj
}

type GenericType struct {
	BaseType types.ID
	SubType  types.ID
}
