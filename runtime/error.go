package runtime

import (
	"fmt"
)

type Error struct {
	line, col   int
	instruction int
	bucket      int
	message     string
	class       string
}

func NewErrorBare(message string) *Error {
	return &Error{
		message: message,
	}
}

func (e *Error) Error() string {
	return fmt.Sprintf("[%04x:%04x] runtime error at line %d col %d: %s ", e.bucket, e.instruction, e.line, e.col, e.message)
}

func (e *Error) Msg() string {
	return e.message
}

func (e *Error) Line() int {
	return e.line
}

func (e *Error) Col() int {
	return e.col
}

func (e *Error) Class() string {
	return e.class
}

