package instruction

import (
	"fmt"
	"gitlab.com/egosummiki/litequel/runtime"
)

type StoreAdd struct {
	Address int
}

func (inst StoreAdd) Perform(ctx *runtime.Context) *runtime.Error {
	value := ctx.Stack().Pop()
	addto, ok := ctx.GetLocal(inst.Address).(runtime.Addable)
	if !ok {
		return ctx.Error("cannot assign to or add this object")
	}
	ctx.SetLocal(inst.Address, addto.Add(ctx, value))
	return nil
}

func (inst StoreAdd) Mnemonic(ctx *runtime.Context) string {
	return fmt.Sprintf("store_add %d", inst.Address)
}

type StoreSubtract struct {
	Address int
}

func (inst StoreSubtract) Perform(ctx *runtime.Context) *runtime.Error {
	value := ctx.Stack().Pop()
	to, ok := ctx.GetLocal(inst.Address).(runtime.Subtractable)
	if !ok {
		return ctx.Error("cannot assign to or subtract this object")
	}
	ctx.SetLocal(inst.Address, to.Subtract(ctx, value))
	return nil
}

func (inst StoreSubtract) Mnemonic(ctx *runtime.Context) string {
	return fmt.Sprintf("store_sub %d", inst.Address)
}

type StoreMult struct {
	Address int
}

func (inst StoreMult) Perform(ctx *runtime.Context) *runtime.Error {
	value := ctx.Stack().Pop()
	to, ok := ctx.GetLocal(inst.Address).(runtime.Multiplicable)
	if !ok {
		return ctx.Error("cannot assign to or multiply this object.")
	}
	ctx.SetLocal(inst.Address, to.Multiply(ctx, value))
	return nil
}

func (inst StoreMult) Mnemonic(ctx *runtime.Context) string {
	return fmt.Sprintf("store_mult %d", inst.Address)
}

type StoreDiv struct {
	Address int
}

func (inst StoreDiv) Perform(ctx *runtime.Context) *runtime.Error {
	value := ctx.Stack().Pop()
	to, ok := ctx.GetLocal(inst.Address).(runtime.Divisible)
	if !ok {
		return ctx.Error("cannot assign to or divide this number")
	}
	ctx.SetLocal(inst.Address, to.Divide(ctx, value))
	return nil
}

func (inst StoreDiv) Mnemonic(ctx *runtime.Context) string {
	return fmt.Sprintf("store_div %d", inst.Address)
}

type Append struct {
	Address int
}

func (inst Append) Perform(ctx *runtime.Context) *runtime.Error {
	value := ctx.Stack().Pop()
	to, ok := ctx.GetLocal(inst.Address).(runtime.Appendable)
	if !ok {
		return ctx.Error("object is not appendable")
	}
	return to.Append(ctx, value)
}

func (inst Append) Mnemonic(ctx *runtime.Context) string {
	return fmt.Sprintf("append %d", inst.Address)
}

type AppendGlobal struct {
	Address int
}

func (inst AppendGlobal) Perform(ctx *runtime.Context) *runtime.Error {
	value := ctx.Stack().Pop()
	to, ok := ctx.GetGlobal(inst.Address).(runtime.Appendable)
	if !ok {
		return ctx.Error("object is not appendable")
	}
	return to.Append(ctx, value)
}

func (inst AppendGlobal) Mnemonic(ctx *runtime.Context) string {
	return fmt.Sprintf("append_glob %d", inst.Address)
}

type SetIndexInst struct {
}

func (inst SetIndexInst) Perform(ctx *runtime.Context) *runtime.Error {
	stack := ctx.Stack()
	index := stack.Pop()
	target, ok := stack.Pop().(runtime.IndexMutable)
	if !ok {
		return ctx.Error("indexing is not supported on this object or object is not indexable")
	}
	value := stack.Pop()
	return target.SetIndex(ctx, index, value)
}

func (inst SetIndexInst) Mnemonic(ctx *runtime.Context) string {
	return "set_index"
}

type Store struct {
	Address int
}

func (inst Store) Perform(ctx *runtime.Context) *runtime.Error {
	value := ctx.Stack().Pop()
	ctx.SetLocal(inst.Address, value)
	return nil
}

func (inst Store) Mnemonic(ctx *runtime.Context) string {
	return fmt.Sprintf("store %d", inst.Address)
}

// Only functions
type StoreGlobal struct {
	Address int
}

func (inst StoreGlobal) Perform(ctx *runtime.Context) *runtime.Error {
	value := ctx.Stack().Pop()
	ctx.SetGlobal(inst.Address, value)
	return nil
}

func (inst StoreGlobal) Mnemonic(ctx *runtime.Context) string {
	return fmt.Sprintf("store_glob %d", inst.Address)
}

type StoreEnclosed struct {
	Frame   int
	Address int
}

func (s StoreEnclosed) Perform(ctx *runtime.Context) *runtime.Error {
	ctx.StoreEnclosed(s.Frame, s.Address, ctx.Stack().Pop())
	return nil
}

func (s StoreEnclosed) Mnemonic(ctx *runtime.Context) string {
	return fmt.Sprintf("store_e %d %d", s.Frame, s.Address)
}

