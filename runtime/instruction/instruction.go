package instruction

import (
	"fmt"

	"gitlab.com/egosummiki/litequel/model"
	"gitlab.com/egosummiki/litequel/model/unsafe"
	"gitlab.com/egosummiki/litequel/runtime"
	"gitlab.com/egosummiki/litequel/types"
)

type Instruction interface {
	Perform(ctx *runtime.Context) *runtime.Error
	Mnemonic(ctx *runtime.Context) string
}

type NopInst struct{}

func (n NopInst) Perform(ctx *runtime.Context) *runtime.Error {
	return nil
}

func (n NopInst) Mnemonic(ctx *runtime.Context) string {
	return "nop"
}

type PushInst struct {
	Arg runtime.Object
}

func (pi PushInst) Perform(ctx *runtime.Context) *runtime.Error {
	ctx.Stack().Push(pi.Arg)
	return nil
}

func (pi PushInst) Mnemonic(ctx *runtime.Context) string {
	return fmt.Sprintf("push %s", pi.Arg.Describe(ctx))
}

type PopInst struct {
	Amount int
}

func (pi PopInst) Perform(ctx *runtime.Context) *runtime.Error {
	ctx.Stack().SlideBack(pi.Amount)
	return nil
}

func (pi PopInst) Mnemonic(ctx *runtime.Context) string {
	return fmt.Sprintf("pop %d", pi.Amount)
}

type DescribeInst struct {
}

func (inst DescribeInst) Perform(ctx *runtime.Context) *runtime.Error {
	obj := ctx.Stack().Pop()
	switch obj.(type) {
	case model.StringObject:
		fmt.Printf("\"%s\"\n", string(obj.(model.StringObject)))
	case runtime.Convertible:
		conv, err := obj.(runtime.Convertible).Convert(ctx, types.TypeString)
		if err != nil {
			return err
		}
		fmt.Println(string(conv.(model.StringObject)))
	default:
		fmt.Println(obj.Describe(ctx))
	}
	return nil
}

func (inst DescribeInst) Mnemonic(ctx *runtime.Context) string {
	return "desc"
}

type CallInst struct {
	NumArgs int
}

func (ci CallInst) Perform(ctx *runtime.Context) *runtime.Error {
	stack := ctx.Stack()
	object := stack.FromTop(ci.NumArgs + 1)
	callable, ok := object.(runtime.Callable)
	if !ok {
		return ctx.Error("object is not callable")
	}
	return callable.Call(ctx, ci.NumArgs)
}

func (ci CallInst) Mnemonic(ctx *runtime.Context) string {
	return fmt.Sprintf("call %d", ci.NumArgs)
}

type CallCatchInst struct {
	Address int
	NumArgs int
}

func (inst CallCatchInst) Perform(ctx *runtime.Context) *runtime.Error {
	stack := ctx.Stack()
	object := stack.FromTop(inst.NumArgs + 1)
	callable, ok := object.(runtime.Callable)
	if !ok {
		return ctx.Error("object is not callable")
	}
	err := callable.Call(ctx, inst.NumArgs)
	if err != nil {
		ctx.SetLocal(inst.Address, &model.Error{Error: err})
		return nil
	}
	ctx.SetLocal(inst.Address, model.None)
	return nil
}

func (inst CallCatchInst) Mnemonic(ctx *runtime.Context) string {
	return fmt.Sprintf("callc %d %d", inst.NumArgs, inst.Address)
}

type FailInst struct {
}

func (fi FailInst) Perform(ctx *runtime.Context) *runtime.Error {
	msg := ctx.Stack().Pop()
	switch msg.(type) {
	case model.StringObject:
		return ctx.UserError(string(msg.(model.StringObject)))
	case *model.Error:
		return msg.(*model.Error).Error
	}

	return ctx.Error("expected a string")
}

func (fi FailInst) Mnemonic(ctx *runtime.Context) string {
	return "fail"
}

type AddInst struct{}

func (ai AddInst) Perform(ctx *runtime.Context) *runtime.Error {
	stack := ctx.Stack()
	num2 := stack.Pop()
	num1, ok := stack.Pop().(runtime.Addable)
	if !ok {
		return ctx.Error("object is not addable")
	}
	stack.Push(num1.Add(ctx, num2))
	return nil
}

func (ai AddInst) Mnemonic(ctx *runtime.Context) string {
	return "add"
}

type SubInst struct{}

func (ai SubInst) Perform(ctx *runtime.Context) *runtime.Error {
	stack := ctx.Stack()
	num2 := stack.Pop()
	num1, ok := stack.Pop().(runtime.Subtractable)
	if !ok {
		return ctx.Error("object is not subtractable")
	}
	stack.Push(num1.Subtract(ctx, num2))
	return nil
}

func (ai SubInst) Mnemonic(ctx *runtime.Context) string {
	return "sub"
}

type MultInst struct{}

func (ai MultInst) Perform(ctx *runtime.Context) *runtime.Error {
	stack := ctx.Stack()
	num2 := stack.Pop()
	num1, ok := stack.Pop().(runtime.Multiplicable)
	if !ok {
		return ctx.Error("object is not multiplicable")
	}
	stack.Push(num1.Multiply(ctx, num2))
	return nil
}

func (ai MultInst) Mnemonic(ctx *runtime.Context) string {
	return "mult"
}

type DivInst struct{}

func (ai DivInst) Perform(ctx *runtime.Context) *runtime.Error {
	stack := ctx.Stack()
	num2 := stack.Pop()
	num1, ok := stack.Pop().(runtime.Divisible)
	if !ok {
		return ctx.Error("object is not divisible")
	}
	stack.Push(num1.Divide(ctx, num2))
	return nil
}

func (ai DivInst) Mnemonic(ctx *runtime.Context) string {
	return "div"
}

type ModInst struct{}

func (ai ModInst) Perform(ctx *runtime.Context) *runtime.Error {
	stack := ctx.Stack()
	num2 := stack.Pop()
	num1, ok := stack.Pop().(runtime.Modulable)
	if !ok {
		return ctx.Error("operation % is not defined on this object")
	}
	stack.Push(num1.(runtime.Modulable).Modulo(ctx, num2))
	return nil
}

func (ai ModInst) Mnemonic(ctx *runtime.Context) string {
	return "mod"
}

type EqInst struct{}

func (in EqInst) Perform(ctx *runtime.Context) *runtime.Error {
	stack := ctx.Stack()
	result := model.Int32Object(0)
	num2 := stack.Pop()
	num1 := stack.Pop()

	if num2 == model.None {
		if num1 == model.None {
			stack.Push(model.Int32Object(1))
		} else {
			stack.Push(model.Int32Object(0))
		}
		return nil
	}

	switch num1.(type) {
	case model.StringObject:
		cmp, ok := num2.(model.StringObject)
		if !ok {
			return ctx.Error("comparing objects of different types")
		}
		if string(num1.(model.StringObject)) == string(cmp) {
			result = model.Int32Object(1)
		}
	case model.Int32Object:
		cmp, ok := num2.(model.Int32Object)
		if !ok {
			return ctx.Error("comparing objects of different types")
		}
		if num1.(model.Int32Object) == cmp {
			result = model.Int32Object(1)
		}
	case model.Float32Object:
		cmp, ok := num2.(model.Float32Object)
		if !ok {
			return ctx.Error("comparing objects of different types")
		}
		if num1.(model.Float32Object) == cmp {
			result = model.Int32Object(1)
		}
	default:
		return ctx.Error("object is not comparable.")
	}

	stack.Push(result)
	return nil
}

func (in EqInst) Mnemonic(ctx *runtime.Context) string {
	return "eq"
}

type NeqInst struct{}

func (in NeqInst) Perform(ctx *runtime.Context) *runtime.Error {
	stack := ctx.Stack()
	result := model.Int32Object(1)
	num2 := stack.Pop()
	num1 := stack.Pop()

	if num2 == model.None {
		if num1 != model.None {
			stack.Push(model.Int32Object(1))
		} else {
			stack.Push(model.Int32Object(0))
		}
		return nil
	}

	switch num1.(type) {
	case model.StringObject:
		cmp, ok := num2.(model.StringObject)
		if !ok {
			return ctx.Error("comparing objects of different types")
		}
		if string(num1.(model.StringObject)) == string(cmp) {
			result = model.Int32Object(0)
		}
	case model.Int32Object:
		cmp, ok := num2.(model.Int32Object)
		if !ok {
			return ctx.Error("comparing objects of different types")
		}
		if num1.(model.Int32Object) == cmp {
			result = model.Int32Object(0)
		}
	case model.Float32Object:
		cmp, ok := num2.(model.Float32Object)
		if !ok {
			return ctx.Error("Comparing objects of different types")
		}
		if num1.(model.Float32Object) == cmp {
			result = model.Int32Object(0)
		}
	default:
		return ctx.Error("object is not comparable.")
	}

	stack.Push(result)
	return nil
}

func (in NeqInst) Mnemonic(ctx *runtime.Context) string {
	return "neq"
}

type GtEqInst struct{}

func (in GtEqInst) Perform(ctx *runtime.Context) *runtime.Error {
	stack := ctx.Stack()
	conv, err := stack.Pop().(runtime.Convertible).Convert(ctx, types.TypeInt64)
	if err != nil {
		return err
	}
	num2, ok := conv.(model.Int64Object)
	if !ok {
		return ctx.Error("comparing object, that isn't a number")
	}
	conv, err = stack.Pop().(runtime.Convertible).Convert(ctx, types.TypeInt64)
	if err != nil {
		return err
	}
	num1, ok := conv.(model.Int64Object)
	if !ok {
		return ctx.Error("comparing object, that isn't a number")
	}
	if int64(num1) >= int64(num2) {
		stack.Push(model.Int32Object(1))
	} else {
		stack.Push(model.Int32Object(0))
	}
	return nil
}

func (in GtEqInst) Mnemonic(ctx *runtime.Context) string {
	return "gt_eq"
}

type GtInst struct{}

func (in GtInst) Perform(ctx *runtime.Context) *runtime.Error {
	stack := ctx.Stack()
	conv, err := stack.Pop().(runtime.Convertible).Convert(ctx, types.TypeInt64)
	if err != nil {
		return err
	}
	num2, ok := conv.(model.Int64Object)
	if !ok {
		return ctx.Error("comparing object, that isn't a number")
	}
	conv, err = stack.Pop().(runtime.Convertible).Convert(ctx, types.TypeInt64)
	if err != nil {
		return err
	}
	num1, ok := conv.(model.Int64Object)
	if !ok {
		return ctx.Error("comparing object, that isn't a number")
	}
	if int64(num1) > int64(num2) {
		stack.Push(model.Int32Object(1))
	} else {
		stack.Push(model.Int32Object(0))
	}
	return nil
}

func (in GtInst) Mnemonic(ctx *runtime.Context) string {
	return "gt"
}

type LtEqInst struct{}

func (in LtEqInst) Perform(ctx *runtime.Context) *runtime.Error {
	stack := ctx.Stack()
	conv, err := stack.Pop().(runtime.Convertible).Convert(ctx, types.TypeInt64)
	if err != nil {
		return err
	}
	num2, ok := conv.(model.Int64Object)
	if !ok {
		return ctx.Error("comparing object, that isn't a number")
	}
	conv, err = stack.Pop().(runtime.Convertible).Convert(ctx, types.TypeInt64)
	if err != nil {
		return err
	}
	num1, ok := conv.(model.Int64Object)
	if !ok {
		return ctx.Error("comparing object, that isn't a number")
	}
	if int64(num1) <= int64(num2) {
		stack.Push(model.Int32Object(1))
	} else {
		stack.Push(model.Int32Object(0))
	}
	return nil
}

func (in LtEqInst) Mnemonic(ctx *runtime.Context) string {
	return "lt_eq"
}

type LtInst struct{}

func (in LtInst) Perform(ctx *runtime.Context) *runtime.Error {
	stack := ctx.Stack()
	conv, err := stack.Pop().(runtime.Convertible).Convert(ctx, types.TypeInt64)
	if err != nil {
		return err
	}
	num2, ok := conv.(model.Int64Object)
	if !ok {
		return ctx.Error("comparing object, that isn't a number")
	}
	conv, err = stack.Pop().(runtime.Convertible).Convert(ctx, types.TypeInt64)
	if err != nil {
		return err
	}
	num1, ok := conv.(model.Int64Object)
	if !ok {
		return ctx.Error("comparing object, that isn't a number")
	}
	if int64(num1) < int64(num2) {
		stack.Push(model.Int32Object(1))
	} else {
		stack.Push(model.Int32Object(0))
	}
	return nil
}

func (in LtInst) Mnemonic(ctx *runtime.Context) string {
	return "lt"
}

type LoadGlobal struct {
	Arg int
}

func (in LoadGlobal) Perform(ctx *runtime.Context) *runtime.Error {
	value := ctx.GetGlobal(in.Arg)
	ctx.Stack().Push(value)
	return nil
}

func (in LoadGlobal) Mnemonic(ctx *runtime.Context) string {
	return fmt.Sprintf("load_glob %d", in.Arg)
}

type Load struct {
	Arg int
}

func (ai Load) Perform(ctx *runtime.Context) *runtime.Error {
	ctx.Stack().Push(ctx.GetLocal(ai.Arg))
	return nil
}

func (ai Load) Mnemonic(ctx *runtime.Context) string {
	return fmt.Sprintf("load %d", ai.Arg)
}

type LoadEnclosed struct {
	Frame   int
	Address int
}

func (l LoadEnclosed) Perform(ctx *runtime.Context) *runtime.Error {
	ctx.Stack().Push(ctx.GetEnclosed(l.Frame, l.Address))
	return nil
}

func (l LoadEnclosed) Mnemonic(ctx *runtime.Context) string {
	return fmt.Sprintf("load_e %d %d", l.Frame, l.Address)
}

type Slide struct {
	Amount int
}

func (ai Slide) Perform(ctx *runtime.Context) *runtime.Error {
	ctx.Stack().Slide(ai.Amount)
	return nil
}

func (ai Slide) Mnemonic(ctx *runtime.Context) string {
	return fmt.Sprintf("reserve %d", ai.Amount)
}

// Takes the last object on stack and check it its zero
type JumpZeroInst struct {
	Address int
}

func (inst JumpZeroInst) Perform(ctx *runtime.Context) *runtime.Error {
	obj, ok := ctx.Stack().Pop().(runtime.Number)
	if !ok {
		return ctx.Error("Object cannot be converted to boolean")
	}
	if obj.ToInt() == 0 {
		ctx.CurrentInstr += inst.Address
	}
	return nil
}

func (inst JumpZeroInst) Mnemonic(ctx *runtime.Context) string {
	return fmt.Sprintf("skip_if_zero %d", inst.Address)
}

// Takes the last object on stack and check it its zero
type JumpNotZeroInst struct {
	Address int
}

func (inst JumpNotZeroInst) Perform(ctx *runtime.Context) *runtime.Error {
	obj, ok := ctx.Stack().Pop().(runtime.Number)
	if !ok {
		return ctx.Error("object cannot be converted to boolean")
	}
	if obj.ToInt() != 0 {
		ctx.CurrentInstr += inst.Address
	}
	return nil
}

func (inst JumpNotZeroInst) Mnemonic(ctx *runtime.Context) string {
	return fmt.Sprintf("skip_ifnot_zero %d", inst.Address)
}

// Takes the last object on stack and check it its zero
type JumpForwardInst struct {
	Address int
}

func (inst JumpForwardInst) Perform(ctx *runtime.Context) *runtime.Error {
	ctx.CurrentInstr += inst.Address
	return nil
}

func (inst JumpForwardInst) Mnemonic(ctx *runtime.Context) string {
	return fmt.Sprintf("skip %d", inst.Address)
}

// Jumps forward and pops the last loop
type BreakInst struct {
	Address int
}

func (inst BreakInst) Perform(ctx *runtime.Context) *runtime.Error {
	ctx.CurrentInstr += inst.Address
	ctx.IterStack().Pop()
	return nil
}

func (inst BreakInst) Mnemonic(ctx *runtime.Context) string {
	return fmt.Sprintf("break %d", inst.Address)
}

/*
ctx runs instruction
instructions use model
model uses ctx
*/
type RangeInst struct{}

func (inst RangeInst) Perform(ctx *runtime.Context) *runtime.Error {
	stack := ctx.Stack()
	num2, ok1 := stack.Pop().(runtime.Number)
	num1, ok2 := stack.Pop().(runtime.Number)
	if !ok1 || !ok2 {
		return ctx.Error("Range can be constructed only from 2 numbers")
	}
	stack.Push(model.NewRange(num1.ToInt(), num2.ToInt()))
	return nil
}

func (inst RangeInst) Mnemonic(ctx *runtime.Context) string {
	return "range"
}

type IterInst struct{}

func (inst IterInst) Perform(ctx *runtime.Context) *runtime.Error {
	iterable := ctx.Stack().Pop().(runtime.Iterable)
	ctx.IterStack().Push(iterable.Iter(ctx))
	return nil
}

func (inst IterInst) Mnemonic(ctx *runtime.Context) string {
	return "iter"
}

type LoopInstr struct {
	Skip int
}

func (inst LoopInstr) Perform(ctx *runtime.Context) *runtime.Error {
	iterStack := ctx.IterStack()
	nextValue := iterStack.Top().Next(ctx)
	if nextValue == model.None {
		ctx.CurrentInstr += inst.Skip
		iterStack.Pop()
		return nil
	}
	ctx.Stack().Push(nextValue)
	return nil
}

func (inst LoopInstr) Mnemonic(ctx *runtime.Context) string {
	return fmt.Sprintf("loop %d", inst.Skip)
}

type ReturnInst struct {
}

func (inst ReturnInst) Perform(ctx *runtime.Context) *runtime.Error {
	// This literally does nothing
	return nil
}

func (inst ReturnInst) Mnemonic(ctx *runtime.Context) string {
	return "ret"
}

type MakeListInst struct {
	Num int
}

func (inst MakeListInst) Perform(ctx *runtime.Context) *runtime.Error {
	stack := ctx.Stack()
	list := model.ListObject(make([]runtime.Object, inst.Num))
	for i := len(list) - 1; i >= 0; i-- {
		list[i] = stack.Pop()
	}
	stack.Push(&list)
	return nil
}

func (inst MakeListInst) Mnemonic(ctx *runtime.Context) string {
	return fmt.Sprintf("make_list %d", inst.Num)
}

type IndexInst struct {
}

func (inst IndexInst) Perform(ctx *runtime.Context) *runtime.Error {
	stack := ctx.Stack()
	index := stack.Pop()
	indexable, ok := stack.Pop().(runtime.Indexable)
	if !ok {
		return ctx.Error("object is not indexable")
	}
	result, err := indexable.Index(ctx, index)
	if err != nil {
		return err
	}
	stack.Push(result)
	return nil
}

func (inst IndexInst) Mnemonic(ctx *runtime.Context) string {
	return "index"
}

type GetAttribInst struct {
	Index int
}

func (inst GetAttribInst) Perform(ctx *runtime.Context) *runtime.Error {
	stack := ctx.Stack()
	owner, ok := stack.Pop().(runtime.AttributeOwner)
	if !ok {
		return ctx.Error("object doesn't own attributes")
	}
	stack.Push(owner.GetAttribute(ctx, inst.Index))
	return nil
}

func (inst GetAttribInst) Mnemonic(ctx *runtime.Context) string {
	return fmt.Sprintf("attrib %d", inst.Index)
}

type SetPropertyInst struct {
	Index int
}

func (inst SetPropertyInst) Perform(ctx *runtime.Context) *runtime.Error {
	stack := ctx.Stack()
	object, ok := stack.Pop().(runtime.PropertyMutable)
	if !ok {
		return ctx.Error("object's properties are not mutable")
	}
	object.SetProperty(ctx, inst.Index, stack.Pop())
	return nil
}

func (inst SetPropertyInst) Mnemonic(ctx *runtime.Context) string {
	return fmt.Sprintf("setprop %d", inst.Index)
}

type ConstructInst struct {
	Count      int
	Type       types.ID
	TotalCount int
	Overload   *struct {
		AddMethod int
	}
}

func (inst ConstructInst) Perform(ctx *runtime.Context) *runtime.Error {
	stack := ctx.Stack()
	data := make([]runtime.Object, inst.TotalCount)
	for i := 0; i < inst.Count; i++ {
		ref, ok := stack.Pop().(model.Int32Object)
		if !ok || ref < 0 || int(ref) >= inst.TotalCount {
			return ctx.Error("invalid constructor property reference")
		}
		obj := stack.Pop()
		data[int(ref)] = obj
	}
	for i, val := range data {
		if val == nil {
			data[i] = model.None
		}
	}
	object := model.UserDefinedObject{Data: data, ObjType: inst.Type}
	if inst.Overload != nil {
		object.AddMethod = inst.Overload.AddMethod
	}
	stack.Push(&object)
	return nil
}

func (inst ConstructInst) Mnemonic(ctx *runtime.Context) string {
	return fmt.Sprintf("constr %d %d %d", inst.Count, inst.Type, inst.TotalCount)
}

type FilterInst struct {
}

func (inst FilterInst) Perform(ctx *runtime.Context) *runtime.Error {
	stack := ctx.Stack()

	cond, ok := stack.Pop().(runtime.Callable)
	if !ok {
		return ctx.Error("condition is not filterable")
	}
	filterable, ok := stack.Pop().(runtime.Filterable)
	if !ok {
		return ctx.Error("object is not filterable")
	}

	filtered, err := filterable.Filter(ctx, cond)
	if err != nil {
		return err
	}
	stack.Push(filtered)
	return nil
}

func (inst FilterInst) Mnemonic(ctx *runtime.Context) string {
	return "filter"
}

type ProjectInst struct {
}

func (inst ProjectInst) Perform(ctx *runtime.Context) *runtime.Error {
	stack := ctx.Stack()

	projFunc, ok := stack.Pop().(runtime.Callable)
	if !ok {
		return ctx.Error("condition is not filterable")
	}
	projectable, ok := stack.Pop().(runtime.Projectable)
	if !ok {
		return ctx.Error("object is not projectable")
	}
	projected, err := projectable.Project(ctx, projFunc)
	if err != nil {
		return err
	}
	stack.Push(projected)
	return nil
}

func (inst ProjectInst) Mnemonic(ctx *runtime.Context) string {
	return "project"
}

type FormatStrInst struct {
	Value model.StringFormatObject
}

func (inst FormatStrInst) Perform(ctx *runtime.Context) *runtime.Error {
	ctx.Stack().Push(model.StringObject(inst.Value.String(ctx)))
	return nil
}

func (inst FormatStrInst) Mnemonic(ctx *runtime.Context) string {
	return fmt.Sprintf("format_str %s", inst.Value.Describe(ctx))
}

type ConvertInst struct {
	TargetType types.ID
}

func (inst ConvertInst) Perform(ctx *runtime.Context) *runtime.Error {
	stack := ctx.Stack()
	obj := stack.Pop()
	if obj.Type() == inst.TargetType {
		stack.Push(obj)
		return nil
	}
	convertible, ok := obj.(runtime.Convertible)
	if !ok {
		return ctx.Error("you can't convert to this object")
	}
	conv, err := convertible.Convert(ctx, inst.TargetType)
	if err != nil {
		return err
	}
	stack.Push(conv)
	return nil
}

func (inst ConvertInst) Mnemonic(ctx *runtime.Context) string {
	return fmt.Sprintf("convert %d", inst.TargetType)
}

type SortInst struct {
	Prop int
	Desc bool
}

func (s SortInst) Perform(ctx *runtime.Context) *runtime.Error {
	stack := ctx.Stack()
	obj := stack.Pop()
	sortable, ok := obj.(runtime.Sortable)
	if !ok {
		return ctx.Error("object is not sortable")
	}

	result, err := sortable.Sort(ctx, s.Prop, s.Desc)
	if err != nil {
		return err
	}

	stack.Push(result)
	return nil
}

func (s SortInst) Mnemonic(ctx *runtime.Context) string {
	return fmt.Sprintf("sort %d %v", s.Prop, s.Desc)
}

type MakeMapInst struct {
	Size int
}

func (m MakeMapInst) Perform(ctx *runtime.Context) *runtime.Error {
	stack := ctx.Stack()
	mapObj := model.MapObject{}

	for i := 0; i < m.Size; i++ {
		value := stack.Pop()
		key := stack.Pop()

		str, ok := key.(model.StringObject)
		if !ok {
			return ctx.Errorf("map key is expected to be a string")
		}

		mapObj[string(str)] = value
	}

	stack.Push(mapObj)
	return nil
}

func (m MakeMapInst) Mnemonic(ctx *runtime.Context) string {
	return "map"
}

type RefInst struct {
}

func (r RefInst) Perform(ctx *runtime.Context) *runtime.Error {
	stack := ctx.Stack()
	ptr, err := unsafe.Ref(ctx, stack.Pop())
	if err != nil {
		return err
	}
	ctx.Stack().Push(ptr)
	return nil
}

func (r RefInst) Mnemonic(ctx *runtime.Context) string {
	return "ref"
}

type Wrap struct {
	IFaceType      types.ID
	UnderlyingType types.ID
	Mapping        map[int]int
}

func (w Wrap) Perform(ctx *runtime.Context) *runtime.Error {
	stack := ctx.Stack()
	stack.Push(model.InterfaceObject{
		UnderlyingObj: stack.Pop(),
		ObjType:       w.UnderlyingType,
		InterfaceType: w.IFaceType,
		CallMapping:   w.Mapping,
	})
	return nil
}

func (w Wrap) Mnemonic(ctx *runtime.Context) string {
	return fmt.Sprintf("wrap %d %d %v", w.IFaceType, w.UnderlyingType, w.Mapping)
}

const (
	TmpContinue = 0
	TmpBreak    = 1
)

// Temporary instruction should never be invoked
// That's why it simply panics
type TmpInst struct {
	Kind  int
	Level int
}

func (inst TmpInst) Perform(ctx *runtime.Context) *runtime.Error {
	return ctx.Error("invoked temporary instruction")
}

func (inst TmpInst) Mnemonic(ctx *runtime.Context) string {
	return "tmp"
}
